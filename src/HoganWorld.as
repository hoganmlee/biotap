package {
	import de.nulldesign.nd2d.display.World2D;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	/**
	 * Dispatched when the World2D is initialized and the context3D is available. The flag 'isHardwareAccelerated' is available then
	 * @eventType flash.events.Event.INIT
	 */
	[Event(name="init", type="flash.events.Event")]
	
	public class HoganWorld extends World2D {
		 
		 /**
		  * Constructor of class world
		  * @param renderMode Context3DRenderMode (auto, software)
		  * @param frameRate timer and the swf will be set to this framerate
		  * @param bounds the worlds boundaries
		  * @param stageID
		  */
		 public function HoganWorld(renderMode:String, frameRate:uint = 60, bounds:Rectangle = null, stageID:uint = 0) {
			 super(renderMode, frameRate, bounds, stageID);
		 }
		 
		 public function hide():void{
			 
			 sleep();
			 
			 stage.stage3Ds[stageID].removeEventListener(Event.CONTEXT3D_CREATE, context3DCreated);
			 stage.stage3Ds[stageID].removeEventListener(ErrorEvent.ERROR, context3DError);
			 
			 if(context3D) {
				 trace("disposing context3D");
				 context3D.dispose();
			 }
			 
			 if(scene) {
				 trace("disposing scene");
				 scene.dispose();
			 }
		 }
		 
		 public function show():void{
			 
			 stage.stage3Ds[stageID].addEventListener(Event.CONTEXT3D_CREATE, context3DCreated);
			 stage.stage3Ds[stageID].addEventListener(ErrorEvent.ERROR, context3DError);
			 stage.stage3Ds[stageID].requestContext3D(renderMode);
			 
			 
			 trace("calling request for context3D");
			
			 
			 //start();
			 
		 }
		 
	 }
 }