package com.sajakfarki.biopets.graphics.text
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	public class GemCounterView extends Node2D
	{
		public static var TEXT_PADDING:int = 3;
		public static var NUM_CHARS:int = 3;
		
//		[Embed( "../media/text/whiteText.png" )]
//		protected var textureAtlasBitmap:Class;
//		[Embed( "../media/text/whiteText.plist", mimeType = "application/octet-stream" )]
//		protected var textureAtlasXML:Class;
		
//		public var spriteBatch:Sprite2DBatch;
		
		private var _atlas:TextureAtlas;
		private var _chars:Vector.<Sprite2D>;
		private var _count:String;
		private var _type:String;
		
		public function GemCounterView(sb:Sprite2DBatch, px:int, py:int, type:String)
		{
			super();
			
			this.x = px;
			this.y = py;
			
			sb.addChild(this);
			
			_type = type;
			
//			var atlasTex:Texture2D = Texture2D.textureFromBitmapData(new textureAtlasBitmap().bitmapData);
//			_atlas = new TextureAtlas(atlasTex.bitmapWidth, atlasTex.bitmapHeight, new XML(new textureAtlasXML()), TextureAtlas.XML_FORMAT_COCOS2D, 0, false);
//			
//			spriteBatch = new Sprite2DBatch(atlasTex);
//			spriteBatch.setSpriteSheet(_atlas);
//			
//			addChild(spriteBatch);
			
			_chars = new Vector.<Sprite2D>;
			
			var img:Sprite2D;
			for (var i:int = 0; i < NUM_CHARS; i++) {
				img = new Sprite2D();
				img.y = this.y;
				sb.addChild(img);
				//img.setFrameByName("7");
				//img.visible = false;
				_chars.push(img);
			}
			
			setCount(0);
		}
		
//		public function show():void
//		{
//			for (var i:int = NUM_CHARS-_count.length; i < NUM_CHARS; i++) 
//			{
//				TweenMax.to(_chars[i], 0.5, {alpha:0.6});
//			}
//			
//		}
//		
//		public function hide():void
//		{
//			for (var i:int = 0; i < NUM_CHARS; i++) 
//			{
//				TweenMax.to(_chars[i], 0.5, {alpha:0});
//			}
//		}
		
		public function setCount(value:int, init:Boolean = true):void
		{
			//trace(value.toString().length);
			
			_count = value.toString();
			var remainder:int = NUM_CHARS - _count.length;
			
			if(remainder >= 0){
				
				for (var i:int = 0; i < remainder; i++) 
				{
					_chars[i].visible = false;
				}
				
			}else{
				
				throw("ERROR: Increase your NUM_CHARS");
			}
			
			var k:int = _count.length-1
			
			for (var j:int = NUM_CHARS-1; j >= remainder; j--) 
			{
				if(init){
					_chars[j].visible = true;
					_chars[j].alpha = 0.6;
				}
				
				_chars[j].spriteSheet.setFrameByName(_type+_count.charAt(k)+".png");
				
				if(j != NUM_CHARS-1){
					
					_chars[j].x = _chars[j+1].x - _chars[j+1].spriteSheet.spriteWidth/2 - _chars[j].spriteSheet.spriteWidth/2 - TEXT_PADDING;
					
				}else{
					
					_chars[j].x = this.x -_chars[j].spriteSheet.spriteWidth/2;
					
				}
				
				k--;
				
			}

		}
		
	}
}