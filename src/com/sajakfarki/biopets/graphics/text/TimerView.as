package com.sajakfarki.biopets.graphics.text
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.osflash.signals.Signal;
	
	public class TimerView extends Node2D
	{
		public static var NUM_CHARS:int = 4;
		public static var TEXT_PADDING:int = 3;
		
		public var timesup:Signal = new Signal();
		
//		[Embed( "../media/text/whiteText.png" )]
//		private var textureAtlasBitmap:Class;
//		[Embed( "../media/text/whiteText.plist", mimeType = "application/octet-stream" )]
//		private var textureAtlasXML:Class;
//		
//		[Embed(source="../media/text/time_remaining.png" )]
//		private var _textTexture:Class;
		private var _text:Sprite2D; 
		
		
	//	private var spriteBatch:Sprite2DBatch;
		
		
		private var _timer:Timer;
		private var _time:int;
		
		private var _characters:String;
		
		private var _atlas:TextureAtlas;
		
		private var _chars:Vector.<Sprite2D>;
		
		private var _type:String;
		
		public function TimerView(sb:Sprite2DBatch, px:int, py:int, type:String)
		{
			super();
			
			sb.addChild(this);
			
			this.x = px;
			this.y = py;
			
			_type = type;
			
			/** Level */
			//var heatLevelTexture:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _textTexture()).bitmapData);
			
			//_text = new Sprite2D(heatLevelTexture);
			_text = new Sprite2D();
			_text.x = -58;
			_text.y = 0;
			
			this.addChild(_text);
			
			_text.spriteSheet.setFrameByName("time_remaining.png");
			
//			var atlasTex:Texture2D = Texture2D.textureFromBitmapData(new textureAtlasBitmap().bitmapData);
//			_atlas = new TextureAtlas(atlasTex.bitmapWidth, atlasTex.bitmapHeight, new XML(new textureAtlasXML()), TextureAtlas.XML_FORMAT_COCOS2D, 0, false);
//			
//			spriteBatch = new Sprite2DBatch(atlasTex);
//			spriteBatch.setSpriteSheet(_atlas);
//			
//			addChild(spriteBatch);
			
			_chars = new Vector.<Sprite2D>;
			
			
			/**********************************************************
			 *  Setup characters
			 **********************************************************/
			var img:Sprite2D;
			for (var i:int = 0; i < NUM_CHARS; i++) {
				img = new Sprite2D();
				this.addChild(img);
				//img.x = x + i * (img.spriteSheet.spriteWidth + 2);
				//img.setFrameByName("0");
				//img.x = x + i * (img.width + TEXT_PADDING);
				//img.y = y;
				//img.visible = false;
				img.alpha = 0.6;
				_chars.push(img);
			}
				
			//setTime();
		
			_timer = new Timer(1000, 0);
			_timer.addEventListener(TimerEvent.TIMER, onTickTock, false, 0, true);
			
		}
		
		public function set time(value:int):void
		{
			_time = value;
			setTime();
		}
		
		public function start():void
		{
			_timer.start();
		}

		public function stop():void
		{
			_timer.stop();
		}
		
		private function onTickTock(e:TimerEvent):void
		{
			_time--;
			
			setTime();
		
			if(_time <=0 ){
				
				_timer.removeEventListener(TimerEvent.TIMER, onTickTock, false);
				timesup.dispatch();
			}
				
		}
		
		private function setTime():void
		{
			var minutes:int = _time / 60;
			var seconds:int = _time % 60;
			
			if(seconds < 10){
				_characters = minutes + ":0" + seconds;
			}else{
				_characters = minutes + ":" + seconds;
			}
			
//			if(minutes < 10){
//				_characters = "0" + _characters;
//			}
			
			for (var i:int = 0; i < _chars.length; i++) 
			{
				
				if(_characters.charAt(i) == ":"){
					_chars[i].spriteSheet.setFrameByName(_type + "colon.png");	
				}else{
					_chars[i].spriteSheet.setFrameByName(_type + _characters.charAt(i) + ".png");
				}
				
				if(i != 0){
					_chars[i].x = _chars[i-1].x + _chars[i-1].spriteSheet.spriteWidth/2 + _chars[i].spriteSheet.spriteWidth/2 + TEXT_PADDING;
				}
				
			}
			
		}
		
	}
}