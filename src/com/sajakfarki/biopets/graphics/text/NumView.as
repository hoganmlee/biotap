package com.sajakfarki.biopets.graphics.text
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.osflash.signals.Signal;
	
	public class NumView extends Node2D
	{
		public static var TEXT_PADDING:int = 3;
		
		public var timesup:Signal = new Signal();
		
		
		[Embed( "../media/text/whiteText.png" )]
		private var textureAtlasBitmap:Class;
		[Embed( "../media/text/whiteText.plist", mimeType = "application/octet-stream" )]
		private var textureAtlasXML:Class;
		
		private var spriteBatch:Sprite2DBatch;
		
		private var _characters:String;
		
		private var _chars:Vector.<Sprite2D>;
		
		private var _type:String = "white";
		
		public function NumView(chars:int)
		{
			super();
			
			var atlasTex:Texture2D = Texture2D.textureFromBitmapData(new textureAtlasBitmap().bitmapData);
			var atlas:TextureAtlas = new TextureAtlas(atlasTex.bitmapWidth, atlasTex.bitmapHeight, new XML(new textureAtlasXML()), TextureAtlas.XML_FORMAT_COCOS2D, 0, false);
						
			spriteBatch = new Sprite2DBatch(atlasTex);
			spriteBatch.setSpriteSheet(atlas);
			
			addChild(spriteBatch);
			
			_chars = new Vector.<Sprite2D>;
			
			for (var i:int = 0; i < chars; i++) 
			{
				var img:Sprite2D;
				img = new Sprite2D();
				img.visible = false;
				
				spriteBatch.addChild(img);
				
				_chars.push(img);
				
			}
			
		}
		
		public function setNum(chars:int):void
		{
			//regex add comma
			_characters = chars.toString().replace( /\d{1,3}(?=(\d{3})+(?!\d))/g , "$&,");
			
			//trace("_characters = " + _characters); 
			
			/**********************************************************
			 *  Setup characters
			 **********************************************************/
			for (var i:int = 0; i < _characters.length; i++) {
				
				//trace(_type+_characters.charAt(i))
				
				if(_characters.charAt(i) == ","){
					
					_chars[i].spriteSheet.setFrameByName(_type+"comma.png");
					
				}else{
					
					_chars[i].spriteSheet.setFrameByName(_type+_characters.charAt(i)+".png");
					
				}
				
				if(i != 0){
					
					_chars[i].x = _chars[i-1].x + _chars[i-1].spriteSheet.spriteWidth/2 + _chars[i].spriteSheet.spriteWidth/2 + TEXT_PADDING;
					
				}
				
				_chars[i].visible = true;
				
			}
			
			for (var j:int = _characters.length; j < _chars.length; j++) 
			{
				_chars[j].visible = false;
			}
			
			
		}
		
	}
}