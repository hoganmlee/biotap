package com.sajakfarki.biopets.graphics.text
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.osflash.signals.Signal;
	
	public class NumberView extends Node2D
	{
		public static var TEXT_PADDING:int = 3;
		
//		public var timesup:Signal = new Signal();
		
//		[Embed( "../media/text/brownText.png" )]
//		private var textureAtlasBitmap:Class;
//		[Embed( "../media/text/brownText.plist", mimeType = "application/octet-stream" )]
//		private var textureAtlasXML:Class;
		
		
		
//		private var spriteBatch:Sprite2DBatch;
		
		
		
		private var _characters:String;
		
//		private var _atlas:TextureAtlas;
		
		private var _chars:Vector.<Sprite2D>;
		
		private var _type:String;
//		private var _num:int
		
		private var _px:int;
		private var _py:int;
		
		public function NumberView(sb:Sprite2DBatch, px:int, py:int, textType:String, numLength:int)
		{
			super();
			
			_type = textType;
			
			_px = px;
			_py = py;
						
			_chars = new Vector.<Sprite2D>();
			
			/**********************************************************
			 *  Setup characters
			 **********************************************************/
			var img:Sprite2D;
			for (var i:int = 0; i < numLength; i++) {
				img = new Sprite2D();
				img.visible = false;
				img.x = px;
				img.y = py;
				sb.addChild(img);
				_chars.push(img);
			}
						
		}
		
		public function show():void{
			
			for (var i:int = 0; i < _characters.length; i++) 
			{
				_chars[i].visible = true;
			}
			
		}
		
		public function setNumber(value:int):void
		{
			_characters = value.toString().replace( /\d{1,3}(?=(\d{3})+(?!\d))/g , "$&,");
			
			//trace(_characters);
			
			for (var i:int = 0; i < _characters.length; i++) {
				
				if(_characters.charAt(i) == ","){
					
					_chars[i].spriteSheet.setFrameByName(_type+"comma.png");
					
				}else{
					
					_chars[i].spriteSheet.setFrameByName(_type+_characters.charAt(i)+".png");
					
				}
				
				if(i != 0){
					
					_chars[i].x = _chars[i-1].x + _chars[i-1].spriteSheet.spriteWidth/2 + _chars[i].spriteSheet.spriteWidth/2 + TEXT_PADDING;
					
				}

			}
			
		}
		
		
		
		
	}
}