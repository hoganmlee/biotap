package com.sajakfarki.biopets.graphics.text
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.osflash.signals.Signal;
	
	public class PercentView extends Node2D
	{
		public static var NUM_CHARS:int = 4;
		public static var TEXT_PADDING:int = 3;
		
//		public var timesup:Signal = new Signal();
		
//		[Embed( "../media/text/brownText.png" )]
//		private var textureAtlasBitmap:Class;
//		[Embed( "../media/text/brownText.plist", mimeType = "application/octet-stream" )]
//		private var textureAtlasXML:Class;
		
		
		//private var spriteBatch:Sprite2DBatch;
		
		
		private var _characters:String;
		
		private var _atlas:TextureAtlas;
		
		private var _chars:Vector.<Sprite2D>;
		
		private var _type:String;
		private var _px:int;
		private var _py:int;
		
		public function PercentView(sb:Sprite2DBatch, px:int, py:int,  textType:String)
		{
			super();
			
			_type = textType;
			
			_px = px;
			_py = py;
			
//			var atlasTex:Texture2D = Texture2D.textureFromBitmapData(new textureAtlasBitmap().bitmapData);
//			_atlas = new TextureAtlas(atlasTex.bitmapWidth, atlasTex.bitmapHeight, new XML(new textureAtlasXML()), TextureAtlas.XML_FORMAT_COCOS2D, 0, false);
//			
//			spriteBatch = new Sprite2DBatch(atlasTex);
//			spriteBatch.setSpriteSheet(_atlas);
			
//			addChild(spriteBatch);
			
			_chars = new Vector.<Sprite2D>();
			
			/**********************************************************
			 *  Setup characters
			 **********************************************************/
			var img:Sprite2D;
			for (var i:int = 0; i < NUM_CHARS; i++) {
				img = new Sprite2D();
				img.visible = false;
				img.x = px;
				img.y = py;
				sb.addChild(img);
				_chars.push(img);
			}
			
		}
		
		public function show():void{
			
			for (var i:int = 0; i < _characters.length; i++) 
			{
				_chars[i].visible = true;
			}
			
		}
		
		public function setPercent(value:int):void
		{
			_characters = value.toString() + "%";
			
			for (var i:int = 0; i < _characters.length; i++) 
			{
				
//				if(i < _characters.length){
//					
//					_chars[i].visible = true;
					
					if(_characters.charAt(i) == "%"){
						
						_chars[i].spriteSheet.setFrameByName(_type+"percent.png");
						
					}else{
						
						_chars[i].spriteSheet.setFrameByName(_type+_characters.charAt(i)+".png");
						
					}
					
					if(i != 0){
						
						_chars[i].x = _chars[i-1].x + _chars[i-1].spriteSheet.spriteWidth/2 + _chars[i].spriteSheet.spriteWidth/2 + TEXT_PADDING;
						
					}
//					else{
//						
//					}
//					
//				}else{
//					
//					_chars[i].visible = false;
//					
//				}
			
			}
			
		}	
		
		
	}
}