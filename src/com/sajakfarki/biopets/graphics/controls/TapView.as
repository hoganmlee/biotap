package com.sajakfarki.biopets.graphics.controls
{
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	
	public class TapView extends Sprite2D
	{
//		[Embed(source = "../media/game_interface/tap_ring.png")]
//		private var _tapTexture:Class;
		
		private var _radius:Number;
		
		public function TapView(sb:Sprite2DBatch, radius : Number)
		{
			//var tap:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _tapTexture()).bitmapData);
			//super(tap);
			//var tap:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _tapTexture()).bitmapData);
			super();
			
			sb.addChild(this);
			
			this.spriteSheet.setFrameByName("tap_ring.png");
			
			this.width = radius;
			this.height = radius;
			
			//TODO: scale tap based on radius
			_radius = radius;
		}
	}
}