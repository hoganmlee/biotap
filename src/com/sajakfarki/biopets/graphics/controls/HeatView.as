package com.sajakfarki.biopets.graphics.controls
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Quad2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.geom.Point;
	
	public class HeatView extends Node2D
	{
		
		//distance from button to min degrees
		private static const ZERO : int = -30;
		
		
		//png height
		private static const HEAT_GAUGE_HEIGHT : Number = 400;
		
		
		//distance from the bottom of the png to the "+" on the heat button
		private static const DISTANCE_TO_BUTTON : Number = 48;
		
		
		//height of thermometer
		private static const LEVEL_HEIGHT :Number = 280;
		
		
		private var _heatLevel:Quad2D
		private var _heatTLHash:Quad2D;
		private var _heatTRHash:Quad2D;
		private var _heatBLHash:Quad2D;
		private var _heatBRHash:Quad2D;
		private var _heatLRange:Quad2D;
		private var _heatRRange:Quad2D;
		
		
		
		[Embed(source="../media/game_interface/heat_button.png" )]
		private var _heatButtonTexture:Class;
		private var _heatButton:Sprite2D;
		
		
		private var _max:Number;
		private var _min:Number;
		private var _optimalMax:Number;
		private var _optimalMin:Number;
		
		public function HeatView(radius:Number, max:Number, min:Number, optimalMax:Number, optimalMin:Number)
		{			
			super();
			
			_max = max;
			_min = min;
			_optimalMax = optimalMax;
			_optimalMin = optimalMin;
			
			var maxOptimalPercent : Number = (_optimalMax - _min) / (_max - _min )
			var minOptimalPercent : Number = (_optimalMin - _min) / (_max - _min);
			
			
			/** Level */
			_heatLevel = new Quad2D(28, 10);
			//_timeBar.pivot = new Point(-_timeWidth*0.5, 0);
			_heatLevel.pivot = new Point(0, 5);
			_heatLevel.topLeftColor = _heatLevel.topRightColor = _heatLevel.bottomRightColor = _heatLevel.bottomLeftColor = 0xCCff4e00;
			_heatLevel.x = 1;
			_heatLevel.y = ZERO;
			
			this.addChild(_heatLevel);
			
			
			/** Heat Button */
			var heatButtonTexture:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _heatButtonTexture()).bitmapData);
			_heatButton = new Sprite2D(heatButtonTexture);
			_heatButton.y = - ( HEAT_GAUGE_HEIGHT / 2 ) + DISTANCE_TO_BUTTON;
			this.addChild(_heatButton);
			
			//trace(_optimalMax)
			//trace(maxOptimalPercent);
			
			var rangeHeight:int = LEVEL_HEIGHT * maxOptimalPercent - LEVEL_HEIGHT * minOptimalPercent;
			
			_heatTLHash = new Quad2D(9, 3);
			_heatTRHash = new Quad2D(9, 3);
			_heatBLHash = new Quad2D(9, 3);
			_heatBRHash = new Quad2D(9, 3);
			
			_heatLRange = new Quad2D(2, rangeHeight);
			_heatRRange = new Quad2D(2, rangeHeight);
			
			_heatTLHash.topLeftColor = _heatTLHash.topRightColor = _heatTLHash.bottomRightColor = _heatTLHash.bottomLeftColor = 
			_heatTRHash.topLeftColor = _heatTRHash.topRightColor = _heatTRHash.bottomRightColor = _heatTRHash.bottomLeftColor = 
			_heatBLHash.topLeftColor = _heatBLHash.topRightColor = _heatBLHash.bottomRightColor = _heatBLHash.bottomLeftColor =
			_heatBRHash.topLeftColor = _heatBRHash.topRightColor = _heatBRHash.bottomRightColor = _heatBRHash.bottomLeftColor =
			_heatLRange.topLeftColor = _heatLRange.topRightColor = _heatLRange.bottomRightColor = _heatLRange.bottomLeftColor =
			_heatRRange.topLeftColor = _heatRRange.topRightColor = _heatRRange.bottomRightColor = _heatRRange.bottomLeftColor = 0xCCFFFFFF;
			
			
			_heatTLHash.x = _heatBLHash.x =  -8;
			_heatTRHash.x = _heatBRHash.x = 11;
			_heatTLHash.y = _heatTRHash.y = ZERO - LEVEL_HEIGHT * maxOptimalPercent;
			_heatBLHash.y = _heatBRHash.y = ZERO - LEVEL_HEIGHT * minOptimalPercent;
			
			_heatLRange.x = -12;
			_heatRRange.x = 15;
			_heatLRange.y = _heatTLHash.y + rangeHeight/2;
			_heatRRange.y = _heatTLHash.y + rangeHeight/2;
						
			
			addChild(_heatTLHash);
			addChild(_heatTRHash);
			addChild(_heatBLHash);
			addChild(_heatBRHash);
			addChild(_heatLRange);
			addChild(_heatRRange);
						
		}
		
		public function heat(value:Number):void
		{
			var percent : Number = value / (_max - _min);
			//trace("percent = " + percent);
			
			_heatLevel.height = LEVEL_HEIGHT * percent + 0.1;
		}
		
		public function kill():void
		{
			TweenMax.to(this, 0.5, {alpha:0});
			
		}
		
		private function removeHeat():void
		{
			this.parent.removeChild(this);
		}
		
	}
}