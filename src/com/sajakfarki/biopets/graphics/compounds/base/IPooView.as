package com.sajakfarki.biopets.graphics.compounds.base
{
	public interface IPooView
	{
		function rotate(value:Number):void
		function ring(value:Number):void;
		
		function stopSound():void;
			
		function get isShowing():Boolean;
		function show():void
		function hide():void
		function kill():void
		function shake(b:Boolean):void
		function bacteriaCollected(value:int, show:Boolean):void
		function progress(gemCount:int, percent:Number):void
			
	}
}