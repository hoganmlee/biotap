package com.sajakfarki.biopets.graphics.compounds.base
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	
	import flash.display.Sprite;
	
	public class PooReactionView extends Node2D
	{
		public static var SPACING:int = 1;
		
		private var _reactionVector:Vector.<PooReactionSegment>;
		private var _progressSegmentAngle:Number;
		private var _numGems:int;
		
		//private var _startAngle : Number = 0;
		
		public function PooReactionView(sb:Sprite2DBatch, numGems:Number, px:Number, py:Number, show:Boolean=false)
		{
			super();
			
			_reactionVector = new Vector.<PooReactionSegment>();
			
			_progressSegmentAngle = (180 - (numGems-1)*SPACING ) / numGems;
			
			_numGems = numGems;
			
			for (var i:int = 0; i < _numGems; i++) 
			{
				//trace("initial angle of segment = " + _progressSegmentAngle * i);
				var newSeg : PooReactionSegment = new PooReactionSegment(sb, _progressSegmentAngle, _progressSegmentAngle * i + SPACING * i, px, py);
				
				newSeg.pooRotation = 0;
				
				if(show){
					newSeg.progress = 1;
				}else{
					newSeg.progress = 0;
				}
				
				_reactionVector.push(newSeg);
				
				//this.addChild(newSeg);
				
			}
			
		}
		
		public function kill():void
		{
			for (var i:int = 0; i < _numGems; i++) 
			{
				TweenMax.to(_reactionVector[i], 0.6, {delay:0.05*i, scaleX:2, scaleY:2, ease:Back.easeInOut, onComplete:removeChild, onCompleteParams:[_reactionVector[i]]});
			}
			
		}
		
		public function updateRotation(angle:Number):void
		{
			for (var i:int = 0; i < _numGems; i++) 
			{
				//trace("update rotation = " + angle);
				TweenMax.delayedCall(0.3, rotateTo, [_reactionVector[i], angle]);
			}
			
		}
		
		private function rotateTo(poo:PooReactionSegment, angle:Number):void
		{
			TweenMax.to(poo, 0.5, {pooRotation:angle, ease:Back.easeInOut});
		}
		
		public function progress(gemCount:int, percent:Number):void
		{
			
			//trace("_progressSegmentAngle = " +  percent);
			_reactionVector[_numGems-gemCount].progress = percent;
			
		}
	}
}