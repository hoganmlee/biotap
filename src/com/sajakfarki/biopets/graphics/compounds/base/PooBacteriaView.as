package com.sajakfarki.biopets.graphics.compounds.base
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.BitmapData;
	
	public class PooBacteriaView extends Node2D
	{
		private var _bacteriaVector:Vector.<PooBacteriaCount>;
		private var _numBacteria:int;
		private var _bacteriaAngle:int;
		private var _maxRadius:Number;
		
//		public function PooBacteriaView(numBacteria:int, maxRadius:Number, bitmapdata:BitmapData)
		public function PooBacteriaView(sb:Sprite2DBatch, px:int, py:int, numBacteria:int, maxRadius:Number, counterTexture:String)
		{
			super();
			
			_bacteriaVector = new Vector.<PooBacteriaCount>();		
			
			_maxRadius = maxRadius;
			_numBacteria = numBacteria;
			_bacteriaAngle = 360 / _numBacteria;
			
			for (var i:int = 1; i < numBacteria + 1; i++) 
			{
				
				var angle:Number = i  * _bacteriaAngle - 90;
				
//				var bacTexture:Texture2D = Texture2D.textureFromBitmapData(bitmapdata);
//				var bacSprite:Sprite2D = new Sprite2D(bacTexture);
				var bacSprite:Sprite2D = new Sprite2D();
				
				sb.addChild(bacSprite);
				
				bacSprite.spriteSheet.setFrameByName(counterTexture);
				
				bacSprite.x = px + Math.cos( ( ( i  * _bacteriaAngle ) / 180) * Math.PI ) * _maxRadius;
				bacSprite.y = py + Math.sin( ( ( i * _bacteriaAngle ) / 180) * Math.PI ) * _maxRadius;
				bacSprite.alpha = 0.25;
				
				var radians:Number = Math.atan2(bacSprite.y, bacSprite.x);
				var degrees:Number = (radians/Math.PI)*180;
				bacSprite.rotation = degrees + 90;
				
				//this.addChild(bacSprite);
				
				var pooBacteria:PooBacteriaCount = new PooBacteriaCount(bacSprite, px, py, _maxRadius, angle); 
				
				_bacteriaVector.push(pooBacteria);
				
			}
			
		}
				
		public function bacteriaCollected(value:int):void
		{
			//trace(this, _numBacteria);
			for (var i:int = 0; i < value; i++) 
			{
				_bacteriaVector[i].show();
			}
			
			for (var j:int = value; j < _numBacteria; j++) 
			{
				_bacteriaVector[j].hide();
			}
			
		}
		
		public function kill():void
		{
			for (var i:int = 0; i < _bacteriaVector.length; i++) 
			{
				TweenMax.delayedCall(i*0.07, _bacteriaVector[i].kill);
			}
		}
		
	}
}