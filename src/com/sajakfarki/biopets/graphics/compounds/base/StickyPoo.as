package com.sajakfarki.biopets.graphics.compounds.base
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	
	import flash.media.Sound;
	import flash.media.SoundChannel;

	public class StickyPoo extends Sprite2D implements IPooView
	{
		private var _pScale:Number;
		
		private var _init:Boolean = false;
		private var _isShaking:Boolean = false;
		private var _isStopped:Boolean = true;
		
		private var _dontSuck:Boolean = false;
		
		/** Sound */
		[Embed(source = "../sounds/SulphateSound.mp3")]
		private var _suckMP3:Class;
		private var _suck:Sound = new _suckMP3() as Sound;
		
		private var _channel:SoundChannel;
		
		public function StickyPoo(sb:Sprite2DBatch)
		{
			super();
			
			sb.addChild(this);
			
			this.spriteSheet.setFrameByName(BadGuyTypes.DOUCHEBAG);
			
		}
		
		public function rotate(value:Number):void
		{
		}
		
		public function ring(value:Number):void
		{
		}
		
		public function get isShowing():Boolean
		{
			return false;
		}
		
		public function show():void
		{
		}
		
		public function hide():void
		{
		}
		
		public function kill():void
		{
		}
		
		public function stopSound():void
		{
			_dontSuck = true;
			
			if(_channel){
				_channel.stop();
			}
		}
		
		public function shake(b:Boolean):void
		{
			if(!_init){
				_init = true;
				_pScale = this.scaleX;
			}
			
			if(b && !_isShaking){
				
//				trace("shake it out!");
				
				if(!_dontSuck){
					_channel = _suck.play(0, 99);
				}
				
				_isShaking = true;
				_isStopped = false;
				
				TweenMax.to(this, 0.25, {scaleX:_pScale+0.02, scaleY:_pScale+0.02, ease:Back.easeInOut, repeat:-1, yoyo:true});
				
			}
			
			if(!b && !_isStopped){
				
				if(_channel){
					_channel.stop();
				}
				
				_isShaking = false;
				_isStopped = true;
				
				TweenMax.killTweensOf(this);
				TweenMax.to(this, 0.3, {scaleX:_pScale, scaleY:_pScale});
				
			}
		}
		
		public function bacteriaCollected(value:int, show:Boolean):void
		{
		}
		
		public function progress(gemCount:int, percent:Number):void
		{
		}
	}
}