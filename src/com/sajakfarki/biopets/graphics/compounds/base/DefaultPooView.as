package com.sajakfarki.biopets.graphics.compounds.base
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	import com.sajakfarki.biopets.utils.VariableParticleSystem2D;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.ParticleSystem2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.geom.ParticleVertex;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	import de.nulldesign.nd2d.materials.ParticleSystemMaterial;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.utils.ParticleSystemPreset;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	public class DefaultPooView extends Node2D implements IPooView
	{
		[Embed(source = "../media/game/gem_aminoAcid.png")]
		private var _aminoAcid:Class;
		[Embed(source = "../media/game/gem_sugar.png")]
		private var _sugar:Class;
		[Embed(source = "../media/game/gem_fattyAcid.png")]
		private var _fattyAcid:Class;
		[Embed(source = "../media/game/gem_volitileFattyAcid.png")]
		private var _volitile:Class;
		[Embed(source = "../media/game/gem_carbonDioxide.png")]
		private var _carbon:Class;
		[Embed(source = "../media/game/gem_hydrogen.png")]
		private var _hydrogen:Class;
		[Embed(source = "../media/game/gem_aceticAcid.png")]
		private var _aceticAcid:Class;
		[Embed(source = "../media/game/gem_methane.png")]
		private var _methane:Class;
		
		/** Sound */
		[Embed(source = "../sounds/PooSplat.mp3")]
		private var _pooSplatMP3:Class;
		private var _pooSplat:Sound = new _pooSplatMP3() as Sound;
		
		/** Sound */
		[Embed(source = "../sounds/BTUSound.mp3")]
		private var _btuMP3:Class;
		private var _btu:Sound = new _btuMP3() as Sound;
		
		private var _channel:SoundChannel;
		
//		[Embed(source = "../media/poo/optimalRing.png")]
//		private var _optimalTexture:Class;
		private var _ring:Sprite2D;
		
		private var _startRadius:Number;
		private var _maxRadius:Number;
		
		//private var _numBacteria:int;
		private var _lastNum:int = 0;
		private var _bacteriaAngle:Number;
		
		
		//private var _startAngle:Number = 270;
		
		
		private var _pooReactionView:PooReactionView;
//		private var _pooBacteriaView:PooBacteriaView;
		private var _reactionBG:PooReactionView;
		
		
//		public function DefaultPooView(sb:Sprite2DBatch, 
//									   				   minRadius : Number, 
//													   maxRadius : Number, 
//													   numBacteria:int, 
//													   numGems:Number, 
//													   px:Number, 
//													   py:Number, 
//													   counterTexture:String,
//														optimalType:String)
		private var _poo:Node2D;
		private var _isShowing:Boolean;
		
		private var _numGems:int;
		private var _psp:ParticleSystemPreset;
		private var _particles1:VariableParticleSystem2D;
		private var _particles2:VariableParticleSystem2D;
		private var _particles3:VariableParticleSystem2D;
		
		private var _stageType:String;
		
		public function DefaultPooView(sb:Sprite2DBatch, 
									   				   minRadius : Number, 
													   maxRadius : Number, 
													   numBacteria:int, 
													   numGems:Number, 
													   px:Number, 
													   py:Number,
													   pooType:String,
													   stageType:String)
		{
			super();
			
			_startRadius = minRadius;
			_maxRadius = maxRadius;
//			_bacteriaAngle = 360 / numBacteria
			
			_numGems = numGems;
				
			_stageType = stageType;
			
				
			_isShowing = false;
				
			//var opt:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _optimalTexture()).bitmapData);
				
//			_pooBacteriaView = new PooBacteriaView(sb, px, py, numBacteria, maxRadius, counterTexture);
//			_pooBacteriaView = new PooBacteriaView(sb, px, py, numBacteria, maxRadius);
			
			_poo = new Node2D();
			_poo.x = px;
			_poo.y = py;
			sb.addChild(_poo);
			
			var a1:Sprite2D = new Sprite2D();
			var a2:Sprite2D = new Sprite2D();
			var a3:Sprite2D = new Sprite2D();
							
			switch(stageType){
				
				case StageTypes.HYDROLYSIS:
					
					_poo.addChild(a1);
					a1.spriteSheet.setFrameByName("gem_aminoAcid.png");
					a1.x = -34;
					a1.rotation = -30;
					
					_poo.addChild(a2);
					a2.spriteSheet.setFrameByName("gem_sugar.png");
					
					_poo.addChild(a3);
					a3.x = 34;
					a3.rotation = -90;
					a3.spriteSheet.setFrameByName("gem_fattyAcid.png");
					
					//gem_aminoAcid
					//gem_fattyAcid
					//gem_sugar
					
					break;
				
				case StageTypes.ACIDOGENESIS:
					
					_poo.addChild(a1);
					a1.x = -20;
					a1.rotation = 45
					a1.spriteSheet.setFrameByName("gem_volitileFattyAcid.png");
					
					_poo.addChild(a2);
					a2.x = 20;
					a2.spriteSheet.setFrameByName("gem_carbonDioxide.png");
					
					//gem_volitileFattyAcid
					//gem_carbonDioxide					
					
					break;
				
				case StageTypes.ACETOGENESIS:
					
					_poo.addChild(a1);
					a1.x = -34;
					a1.spriteSheet.setFrameByName("gem_carbonDioxide.png");
					
					_poo.addChild(a2);
					a2.x = 0;
//					a2.rotation = -30
					a2.spriteSheet.setFrameByName("gem_hydrogen.png");
					
					_poo.addChild(a3);
					a3.x = 34;
//					a3.rotation = -90;
					a3.spriteSheet.setFrameByName("gem_aceticAcid.png");
					
					//gem_carbonDioxide
					//gem_hydrogen
					//gem_aceticAcid
					
					break;
				
				case StageTypes.METHANOGENESIS:
					_poo.addChild(a1);
					a1.x = -20;
					a1.spriteSheet.setFrameByName("gem_methane.png");
					
					_poo.addChild(a2);
					a2.x = 20;
//					a2.rotation = -30
					a2.spriteSheet.setFrameByName("gem_carbonDioxide.png");
					
					//gem_methane
					//gem_carbonDioxide
					
					break;
				
			}
		
			/** Poo texture */
			var p:Sprite2D = new Sprite2D();
//			p.x = px;
//			p.y = py;
			p.alpha = 0.5;
			
			_poo.addChild(p);
			p.spriteSheet.setFrameByName(pooType);
			
			
			//_pooBacteriaView.x = px;
			//_pooBacteriaView.y = py;
			//_pooBacteriaView.rotation = _startAngle;
			//this.addChild(_pooBacteriaView);
			
			
			/** feed zone ring */
			_ring = new Sprite2D();
			sb.addChild(_ring);
			_ring.spriteSheet.setFrameByName("feedzone.png");
						
			_ring.alpha = 0;
			//			_ring.width = minRadius * 2;
			//			_ring.height = minRadius * 2;
			//trace("maxRadius = " +maxRadius);
			_ring.width = maxRadius * 2;
			_ring.height = maxRadius * 2;
			_ring.x = px;
			_ring.y = py;
			
			
			//this.addChild(_ring);
			
			
			
			
			
			// TODO: reimplement reaction view
			
			
			
			//_reactionBG = new PooReactionView(sb, numGems, px, py, true);
			//_reactionBG.alpha = 0;
			//this.addChild(_reactionBG);
			
			//_pooReactionView = new PooReactionView(sb, numGems, px, py);
			//_pooReactionView.rotation = _startAngle;
			//this.addChild(_pooReactionView);
			
			//_pooReactionView.updateRotation(_startAngle);
			_psp = new ParticleSystemPreset();
			_psp.minStartPosition.x = -20.0;
			_psp.maxStartPosition.x = 20.0;
			_psp.startColor = 0x00FF00;
			_psp.startColorVariance = 0x0000FF;
			_psp.endColor = 0xAAFF33;
			_psp.endColorVariance = 0xFF9966;
			_psp.minStartSize = 0.0;
			_psp.maxStartSize = 1.0;
			_psp.minEndSize = 3.0;
			_psp.maxEndSize = 4.0;
			_psp.spawnDelay = 0.05;
			
			var part1:Texture2D;
			var part2:Texture2D;
			var part3:Texture2D;
			
			switch(stageType){
				
				case StageTypes.HYDROLYSIS:
					//gem_aminoAcid
					//gem_fattyAcid
					//gem_sugar
					
					part1 = Texture2D.textureFromBitmapData(Bitmap(new _aminoAcid()).bitmapData);
					part2 = Texture2D.textureFromBitmapData(Bitmap(new _fattyAcid()).bitmapData);
					part3 = Texture2D.textureFromBitmapData(Bitmap(new _sugar()).bitmapData);
					
					break;
				
				case StageTypes.ACIDOGENESIS:
					
					part1 = Texture2D.textureFromBitmapData(Bitmap(new _volitile()).bitmapData);
					part2 = Texture2D.textureFromBitmapData(Bitmap(new _carbon()).bitmapData);
					//gem_volitileFattyAcid
					//gem_carbonDioxide					
					
					break;
				
				case StageTypes.ACETOGENESIS:
					
					part1 = Texture2D.textureFromBitmapData(Bitmap(new _carbon()).bitmapData);
					part2 = Texture2D.textureFromBitmapData(Bitmap(new _hydrogen()).bitmapData);
					part3 = Texture2D.textureFromBitmapData(Bitmap(new _aceticAcid()).bitmapData);
					//gem_carbonDioxide
					//gem_hydrogen
					//gem_aceticAcid
					
					break;
				
				case StageTypes.METHANOGENESIS:
					
					part1 = Texture2D.textureFromBitmapData(Bitmap(new _methane()).bitmapData);
					part2 = Texture2D.textureFromBitmapData(Bitmap(new _carbon()).bitmapData);
					//gem_methane
					//gem_carbonDioxide
					
					break;
				
			}
			
			_particles1 = new VariableParticleSystem2D(part1, 10, _psp);
			_particles1.gravity = new Point(0.0, -200.0);
			//particles.scaleX = particles.scaleY = 4.0;
			_particles1.blendMode = BlendModePresets.ADD_PREMULTIPLIED_ALPHA;
					
			
			_particles2 = new VariableParticleSystem2D(part2, 10, _psp );
			_particles2.gravity = new Point(0.0, -400.0);
			//particles.scaleX = particles.scaleY = 4.0;
			_particles2.blendMode = BlendModePresets.ADD_PREMULTIPLIED_ALPHA;
			
			if(part3 != null){
				_particles3 = new VariableParticleSystem2D(part3, 10, _psp );
				_particles3.gravity = new Point(0.0, -300.0);
				//particles.scaleX = particles.scaleY = 4.0;
				_particles3.blendMode = BlendModePresets.ADD_PREMULTIPLIED_ALPHA;
			}
			
		}
		
		public function bacteriaCollected(value:int, show:Boolean):void
		{

			if(value != _lastNum){
				
				if(value > 0 && _lastNum == 0){
					
//					trace("collect bacteria here");
					
					if(_channel){
						_channel.stop();
					}
					
					_channel = _btu.play(0,99);
					
					switch(_stageType){
						
						case StageTypes.HYDROLYSIS:
							_particles1.reset();
							_particles2.reset();
							_particles3.reset();
							addChild(_particles1);
							addChild(_particles2);
							addChild(_particles3);
							break;
						
						case StageTypes.ACIDOGENESIS:
							_particles1.reset();
							_particles2.reset();
							addChild(_particles1);
							addChild(_particles2);
							break;
						
						case StageTypes.ACETOGENESIS:
							_particles1.reset();
							_particles2.reset();
							_particles3.reset();
							addChild(_particles1);
							addChild(_particles2);
							addChild(_particles3);	
							break;
						
						case StageTypes.METHANOGENESIS:
							_particles1.reset();
							_particles2.reset();
							addChild(_particles1);
							addChild(_particles2);
							break;
						
					}
					
				}else if(value == 0){
					
					if(_channel){
						_channel.stop();
					}
										
					removeParticles();
					
				}
				
//				_pooBacteriaView.bacteriaCollected(value);
				
				if(show){
					
					//_pooReactionView.alpha = 1;
					//TweenMax.to(_pooReactionView, 0.4, {alpha:1});
					//	TweenMax.to(_reactionBG, 0.4, {alpha:1});
//					_pooReactionView.updateRotation(_bacteriaAngle * value);
					//_reactionBG.updateRotation(_bacteriaAngle * value);
					//_pooReactionView.rotation =  + _bacteriaAngle * value;
					
				}else{
					
					//TweenMax.to(_reactionBG, 0.6, {alpha:0});
					//	TweenMax.to(_pooReactionView, 0.6, {alpha:0});
					//_pooReactionView.alpha = 0;
					
				}

				_lastNum = value;
				
			}
			
		}
		
		private function removeParticles():void
		{
			switch(_stageType){
				
				case StageTypes.HYDROLYSIS:
					
					trace("add event complete listener");
					
//					_particles1.addEventListener(Event.COMPLETE, particle1Complete);
//					_particles2.addEventListener(Event.COMPLETE, particle2Complete);
//					_particles3.addEventListener(Event.COMPLETE, particle3Complete);
					
					_particles1.isDead = true;
					_particles2.isDead = true;
					_particles3.isDead = true;
					
					
					//removeChild(_particles1);
					//removeChild(_particles2);
					//removeChild(_particles3);
					
					break;
				
				case StageTypes.ACIDOGENESIS:
					removeChild(_particles1);
					removeChild(_particles2);
					break;
				
				case StageTypes.ACETOGENESIS:
					removeChild(_particles1);
					removeChild(_particles2);
					removeChild(_particles3);	
					break;
				
				case StageTypes.METHANOGENESIS:
					removeChild(_particles1);
					removeChild(_particles2);
					break;
				
			}
		}
		
		private function particle1Complete(e:Event):void
		{
			trace("particle 1 remove!");
			removeChild(_particles1);
		}
		private function particle2Complete(e:Event):void
		{
			trace("particle 2 remove!");
			removeChild(_particles2);
		}
		private function particle3Complete(e:Event):void
		{
			trace("particle 3 remove!");
			removeChild(_particles3);
		}
		
		public function get isShowing():Boolean
		{
			return _isShowing;
		}
		
		public function show():void
		{
			//_isShowing = true;
			if(_ring.alpha < 0.5){
				_ring.width = _maxRadius*2;
				_ring.height = _maxRadius*2;
				
				_ring.alpha += 0.01;
				//TweenMax.to(_ring, 0.5, {alpha:0.5, ease:Back.easeIn});
			}
		}
		
		public function hide():void
		{
			if(_ring.alpha > 0){
				_ring.alpha -= 0.01;
				//_isShowing = false;
				//TweenMax.to(_ring, 0.5, {alpha:0, ease:Back.easeOut});
			}
		}
				
		
		public function ring(value:Number):void
		{
			
//			_ring.width = value * 2 + 19;
//			_ring.height = value * 2 + 19;
//									
//			if(value == _startRadius && _ring.alpha != 0){
//				
//				_ring.alpha -=0.05;
//				
//				if(_ring.alpha < 0){
//					_ring.alpha = 0;
//				}
//				
//			}else if(value > _startRadius && _ring.alpha != 1){
//				
//				_ring.alpha += 0.05;
//				
//				if(_ring.alpha > 1){
//					_ring.alpha = 1;
//				}
//				
//			}
						
			if(value < _maxRadius){
				
				//if(_isShowing && this.stage){
					hide();
				//}
				
			}else{
				
				//if(!_isShowing && this.stage){
					show();
				//}
				
			}
			
			
		}
		
		public function stopSound():void
		{
			if(_channel){
				_channel.stop();
			}
		}
		
		/**********************************************************
		 * 
		 * 	Set Progress of Reaction
		 * 
		 **********************************************************/
		public function progress(gemCount:int, percent:Number):void
		{
//			trace("===== poo progress = " + percent + " gem count = " + gemCount);
			/** Progress of gem collection */
			//_pooReactionView.progress(gemCount, percent);
			
			if(percent > 0){
				
				var scalePercent:Number = ( ( gemCount / _numGems) - ( ( gemCount - 1 ) / _numGems ) ) * percent + ( ( gemCount - 1) / _numGems );
								
				_poo.scaleX = scalePercent;
				_poo.scaleY = scalePercent;
				
				//_particles.alpha = 1 - scalePercent;
				//trace(_particles.alpha);
				
			}
			
//			TweenMax.to(_poo, 0.4, {scaleX:scalePercent, scaleY:scalePercent, ease:Back.easeIn});
						
		}
		
		public function rotate(value:Number):void
		{
			_poo.rotation = value;
		}
		
		public function kill():void
		{
			if(_channel){
				_channel.stop();
			}
			_channel = _pooSplat.play(0, 1);
			
			//_particles.reset();
			removeParticles();
			
			//_particles.removeAllChildren();
			
			TweenMax.to(_poo, 0.5, {alpha:0});
			TweenMax.to(_ring, 0.5, {alpha:0, width:_ring.width + 50, height:_ring.height + 50, ease:Back.easeIn, onComplete:removeRing});
		}
		
//		public function killRing():void
//		{
//			
//			_pooBacteriaView.kill();
//			
//			TweenMax.to(_ring, 0.5, {alpha:0, width:_ring.width + 50, height:_ring.height + 50, ease:Back.easeIn, onComplete:removeRing});
//			
//			//_pooReactionView.kill();
//			//TweenMax.delayedCall(0.1, _reactionBG.kill)
//		}
		
		public function shake(b:Boolean):void{}
		
		private function removeRing():void
		{
			this.removeChild(_ring);
		}
		
	}
}