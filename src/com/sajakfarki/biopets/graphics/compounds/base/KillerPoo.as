package com.sajakfarki.biopets.graphics.compounds.base
{
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;

	public class KillerPoo extends Sprite2D implements IPooView
	{		
		public function KillerPoo(sb:Sprite2DBatch)
		{
			super();
			
			sb.addChild(this);
			
			this.spriteSheet.setFrameByName(BadGuyTypes.JERK);
			
		}
		
		public function rotate(value:Number):void
		{
		}
		
		public function ring(value:Number):void
		{
		}
		
		public function get isShowing():Boolean
		{
			return false;
		}
		
		public function stopSound():void
		{
			
		}
		
		public function show():void
		{
		}
		
		public function hide():void
		{
		}
		
		public function kill():void
		{
		}
		
		public function shake(b:Boolean):void
		{
		}
		
		public function bacteriaCollected(value:int, show:Boolean):void
		{
		}
		
		public function progress(gemCount:int, percent:Number):void
		{
		}
	}
}