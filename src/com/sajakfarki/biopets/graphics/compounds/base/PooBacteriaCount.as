package com.sajakfarki.biopets.graphics.compounds.base
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quint;
	
	import de.nulldesign.nd2d.display.Sprite2D;

	public class PooBacteriaCount
	{
		
		private static var DISTANCE:Number = 15;
		
		private var _view:Sprite2D;
		private var _radius:Number;
		private var _angle:Number;
		
		private var _x:Number;
		private var _y:Number;
		private var _cx:Number;
		private var _cy:Number;
		private var _kx:Number;
		private var _ky:Number;
						
		public function PooBacteriaCount(sprite2D:Sprite2D, px:int, py:int, radius:Number, angle:Number)
		{
			_view = sprite2D;
			_view.alpha = 0;
			_radius = radius;
			_angle = angle;
			
			_view.x = px + Math.cos( ( angle / 180 ) * Math.PI ) * ( radius - DISTANCE );
			_view.y = py + Math.sin( ( angle / 180 ) * Math.PI ) * ( radius - DISTANCE );
			_view.rotation = ( ( Math.atan2(_view.y, _view.x) / Math.PI ) * 180 ) + 90;
			_view.alpha = 0;
			
			_x = _view.x;
			_y = _view.y;
			
			_cx = px + Math.cos( ( angle / 180 ) * Math.PI ) * radius; 
			_cy = py + Math.sin( ( angle / 180 ) * Math.PI ) * radius;
			
			_kx = px + Math.cos( ( angle / 180 ) * Math.PI ) * ( radius + DISTANCE );
			_ky = py +Math.sin( ( angle / 180 ) * Math.PI ) * ( radius + DISTANCE );
		}
		
		public function show():void
		{
			TweenMax.killTweensOf(_view);
			TweenMax.to(_view, 0.3, {x:_cx, y:_cy, alpha:1, ease:Back.easeOut});
		}
		
		public function hide():void
		{
			TweenMax.killTweensOf(_view);
			TweenMax.to(_view, 0.3, {x:_x, y:_y, alpha:0, ease:Back.easeIn});
		}
		
		public function kill():void
		{
			TweenMax.to(_view, 0.3, {x:_kx, y:_ky, alpha:0, ease:Quint.easeOut}); 
		}
		
	}
}