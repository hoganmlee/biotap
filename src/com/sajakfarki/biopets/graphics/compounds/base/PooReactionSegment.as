package com.sajakfarki.biopets.graphics.compounds.base
{
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	
	public class PooReactionSegment extends Sprite2D
	{
//		[Embed(source = "../media/poo/reactionImage.png")]
//		private var _reactionT:Class;
//		
//		[Embed(source = "../media/poo/reactionMask.png")]
//		private var _maskT:Class;
		private var _mask:Sprite2D;
		
		private var _angle:Number;
		private var _rotation:Number;
		private var _percent:Number;
		private var _newRotation:Number;
		
		public function PooReactionSegment(sb:Sprite2DBatch, angle:Number, rotation:Number, px:Number, py:Number)
		{
			_angle = angle;
			_rotation = rotation;
			_newRotation = rotation;
			
			
			this.rotation = rotation;
			//trace("initial rotation = " + rotation);
			
			
			//var reactionT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _reactionT()).bitmapData);
			//super(reactionT);
			super();
			
			sb.addChild(this);
			
			this.spriteSheet.setFrameByName("reactionImage.png");
			
			blendMode = BlendModePresets.ADD_NO_PREMULTIPLIED_ALPHA;
			//blendMode = BlendModePresets.NORMAL_PREMULTIPLIED_ALPHA;
			
			//var maskT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _maskT()).bitmapData);
			
			//_mask = new Sprite2D(maskT);
			_mask = new Sprite2D();
			sb.addChild(_mask);
			_mask.spriteSheet.setFrameByName("reactionMask.png");
			
			_mask.x = px;
			_mask.y = py;
			
			setMask(_mask);
			
		}
		public function get pooRotation():Number
		{
			//if(this.rotation != _rotation){
			//	return this.rotation;
			//}else{
				//return _rotation;
			//}
			return _newRotation;
		}
		
		public function set pooRotation(value:Number):void
		{	
			//_rotation = value;
			_newRotation = value;
			
			this.rotation = _rotation + _newRotation;
			_mask.rotation = this.rotation + _percent * _angle;
		}
		
		public function get progress():Number
		{
			return _percent;
		}
		
		public function set progress(percent:Number):void
		{
			_percent = percent;
			_mask.rotation = this.rotation + _percent * _angle;
		}
	}
}