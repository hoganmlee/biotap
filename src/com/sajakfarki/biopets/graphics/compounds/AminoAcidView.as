package com.sajakfarki.biopets.graphics.compounds
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.sajakfarki.biopets.graphics.compounds.base.DefaultPooView;
	import com.sajakfarki.biopets.graphics.compounds.base.IPooView;
	
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	
	public class AminoAcidView extends DefaultPooView implements IPooView
	{
		[Embed(source="../media/aminoAcid.png" )]
		private var _aminoTexture:Class;
		
		[Embed(source="../media/pooBacteriaCounter.png" )]
		private var _bacTexture:Class;
		
		
		private var _amino:Sprite2D;
		
		
		public function AminoAcidView(minRadius:Number, maxRadius:Number, numBacteria:int)
		{
			super(minRadius, maxRadius);
			
			var poo:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _aminoTexture()).bitmapData);
			_amino = new Sprite2D(poo);
			
			this.addChild(_amino);
			
			createBacteriaRing((new _bacTexture()).bitmapData, numBacteria);
			
		}
		
		public function rotate(value:Number):void
		{
			_amino.rotation = value;
		}
		
		public function kill():void
		{
			killRing();
			
			TweenMax.to(_amino, 0.5, {alpha:0, width:0.1, height:0.1, ease:Back.easeIn, onComplete:removePoo});
		}
		
		private function removePoo():void
		{
			this.removeChild(_amino);
		}
		
	}
}