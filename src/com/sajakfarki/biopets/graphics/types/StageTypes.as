package com.sajakfarki.biopets.graphics.types
{
	public class StageTypes
	{
		public static var HYDROLYSIS:String = "hydro";
		public static var ACIDOGENESIS:String = "acido";
		public static var ACETOGENESIS:String = "aceto";
		public static var METHANOGENESIS:String = "meth";
	}
}