package com.sajakfarki.biopets.graphics.types
{
	public class BacteriaTypes
	{
		public static var ARCHIE:String = "bact_archie.png";
		public static var BAXTER:String = "bact_baxter.png";
		public static var SABBY:String = "bact_sabby.png";
		public static var CLOSTER:String = "bact_closter.png";
		
	}
}