package com.sajakfarki.biopets.graphics.bacteria
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.sajakfarki.biopets.graphics.types.BacteriaTypes;
	import com.sajakfarki.biopets.utils.FastRandom;
	
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	
	public class ArchieBacteriaView extends Sprite2D implements IBacteriaView
	{
		//public static const FRAMES:int = 100;
		
		//		[Embed(source = "../media/bacteria/archie.png")]
		//		private var _bacteriaTexture:Class;
		
		//		[Embed( "../media/bacteria/archie_spritesheet.png" )]
		//		protected var textureAtlasBitmap:Class;
		//		[Embed( "../media/bacteria/archie_spritesheet.plist", mimeType = "application/octet-stream" )]
		//		protected var textureAtlasXML:Class;
		
		//private var _sprite:Sprite2D;
		
		//private var _radius:Number;
		
		public function ArchieBacteriaView(sb:Sprite2DBatch)
		{
			super();
			
//			/this.alpha = 0;
			
			sb.addChild(this);
			
			//_radius = radius;
			
			//			var atlasTex:Texture2D = Texture2D.textureFromBitmapData(new textureAtlasBitmap().bitmapData);
			//			var atlas:TextureAtlas = new TextureAtlas(atlasTex.bitmapWidth, atlasTex.bitmapHeight, new XML(new textureAtlasXML()), TextureAtlas.XML_FORMAT_COCOS2D, 60, false);
			
			//			this.setSpriteSheet(atlas);
			
			this.spriteSheet.setFrameByName(BacteriaTypes.ARCHIE);
			
			//this.spriteSheet.playAnimation( "archie", FastRandom.randomRange(FRAMES) );
			
		}
		
		public function kill():void
		{
			TweenMax.to(this, 0.8, {scaleX: 0.1, scaleY:0.1, rotation:this.rotation+300, ease:Back.easeOut, onComplete:killComplete});
		}
		
		private function killComplete():void{
			
			this.visible = false;
			
		}
		
	}
}
