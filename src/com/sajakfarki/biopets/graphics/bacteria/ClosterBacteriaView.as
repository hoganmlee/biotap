package com.sajakfarki.biopets.graphics.bacteria
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.sajakfarki.biopets.graphics.types.BacteriaTypes;
	
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	
	public class ClosterBacteriaView extends Sprite2D implements IBacteriaView
	{
//		[Embed(source = "../media/bacteria/closter.png")]
//		private var _bacteriaTexture:Class;
//		
//		private var _radius:Number;
		
		public function ClosterBacteriaView(sb : Sprite2DBatch)
		{
//			var poo:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bacteriaTexture()).bitmapData);
//			super(poo);
//			
//			_radius = radius;
			
			super();
			
			sb.addChild(this);
			
			this.spriteSheet.setFrameByName(BacteriaTypes.CLOSTER);
			
		}
		
		public function kill():void
		{
			TweenMax.to(this, 0.8, {scaleX: 0.1, scaleY:0.1, rotation:this.rotation+300, ease:Back.easeOut, onComplete:killComplete});
		}
		
		private function killComplete():void{
			
			this.visible = false;
			
		}
		
	}
}