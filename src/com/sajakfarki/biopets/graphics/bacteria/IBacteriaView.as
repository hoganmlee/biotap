package com.sajakfarki.biopets.graphics.bacteria
{
	public interface IBacteriaView
	{
		function kill():void;
	}
}