package com.sajakfarki.biopets.signals
{
	import flash.geom.Point;
	
	import org.osflash.signals.Signal;
	
	public class HUDPosition extends Signal
	{
		public function HUDPosition()
		{
			super(String, Point, int, Number, Number);
		}
	}
}