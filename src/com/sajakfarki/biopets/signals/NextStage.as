package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;
	
	public class NextStage extends Signal
	{
		public function NextStage(...parameters)
		{
			super(parameters);
		}
	}
}