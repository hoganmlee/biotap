package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;
	
	public class NumberOfBacteria extends Signal
	{
		public function NumberOfBacteria()
		{
			super(String, int);
		}
	}
}