package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;
	
	public class ReactionProgress extends Signal
	{
		public function ReactionProgress()
		{
			super(String, int, Number);
		}
	}
}