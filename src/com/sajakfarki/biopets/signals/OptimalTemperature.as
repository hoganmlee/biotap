package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;
	
	public class OptimalTemperature extends Signal
	{
		public function OptimalTemperature()
		{
			super(Boolean);
		}
	}
}