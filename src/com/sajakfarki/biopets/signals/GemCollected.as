package com.sajakfarki.biopets.signals
{
	import flash.geom.Point;
	
	import org.osflash.signals.Signal;
	
	public class GemCollected extends Signal
	{
		public function GemCollected()
		{
			super(Point);
		}
	}
}