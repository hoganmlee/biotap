package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;
	
	public class LevelInitialized extends Signal
	{
		public function LevelInitialized()
		{
			super();
		}
	}
}