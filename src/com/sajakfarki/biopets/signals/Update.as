package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;

	public class Update extends Signal
	{
		public function Update()
		{
			super( Number );
		}
	}
}
