package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;
	
	public class StageFinished extends Signal
	{
		public function StageFinished()
		{
			super(Boolean);
		}
	}
}