package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;
	
	public class Retry extends Signal
	{
		public function Retry(...parameters)
		{
			super(parameters);
		}
	}
}