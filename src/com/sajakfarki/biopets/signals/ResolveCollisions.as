package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;

	public class ResolveCollisions extends Signal
	{
		public function ResolveCollisions()
		{
			super( Number );
		}
	}
}
