package com.sajakfarki.biopets.signals
{
	import org.osflash.signals.Signal;

	public class Move extends Signal
	{
		public function Move()
		{
			super( Number );
		}
	}
}
