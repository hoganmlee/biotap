package com.sajakfarki.biopets.utils
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quint;
	import com.greensock.plugins.ShortRotationPlugin;
	import com.greensock.plugins.TweenPlugin;

	public class PooHUD extends pooHUD
	{
		private var _id : String;
		
		private var _gems : int;
		
		private var _progress : Number;
		
		private var _progressSegmentAngle : Number;
		private var _bacteriaSegementAngle : Number;
		
		private var _maxBacteria : Number;
		private var _minBacteria : Number;
		
		private var _isOptimal:Boolean = false;
		private var _isOpen:Boolean = false;
		private var _isShowing:Boolean = false;
		private var _isDead:Boolean = false;
		
		private var _angle:Number;
		private var _numBacteria:int;
		
		public function PooHUD()
		{
			super();
			
			TweenPlugin.activate([ShortRotationPlugin]);
		}
		
		/**********************************************************
		 * Getter/Setters
		 **********************************************************/
		public function set ID(value:String):void
		{
			_id = value;
		}
		
		public function set isOptimal(value:Boolean):void
		{
			_isOptimal = value;
			
			if(!_isDead){
				
				if(_isOptimal){
					
					if(!_isOpen){
						
						openHUD();
					}
					
					if(_numBacteria > _minBacteria){
						showProgress();
					}
					
				}else{
					
					hideProgress();
					
				}
			
			}
			
		}
		
		/**********************************************************
		 * Show/Hide Progress Bar
		 **********************************************************/
		private function showProgress():void {
			_isShowing = true;
			TweenMax.to(this.gemCounter, 0.4, {autoAlpha:1});
		}
		
		private function hideProgress():void{
			_isShowing = false;
			TweenMax.to(this.gemCounter, 0.4, {autoAlpha:0});
		}
		
		/**********************************************************
		 * Build HUD
		 **********************************************************/
		public function buildHUD(gems:int, minBacteria:Number, maxBacteria:Number):void
		{
			_gems = gems;
			_minBacteria = minBacteria;
			_maxBacteria = maxBacteria;
			
			_progressSegmentAngle = 180 / gems;
			_bacteriaSegementAngle = 360 / maxBacteria;
			_angle = _minBacteria * _bacteriaSegementAngle;
			
			this.gemCounter.scaleX = 0;
			this.gemCounter.scaleY = 0;
			this.gemCounter.rotation = _angle - 90;
			this.gemCounter.progressBar.rotation = 0;
			
		}
		
		/**********************************************************
		 * Open/Close HUD
		 **********************************************************/
		public function openHUD():void
		{
			_isOpen = true; 
			
			TweenMax.to(this.gemCounter, 0.5, {rotation:_angle, scaleX:1, scaleY:1, ease:Back.easeOut});
		}
		
		public function closeHUD():void
		{
			_isDead = true;
			_isOpen = false; 
			
			TweenMax.killDelayedCallsTo(moveCounter);
			TweenMax.killTweensOf(this.gemCounter);
			
			TweenMax.to(this.gemCounter, 0.9, {rotation:this.gemCounter.rotation + 240, scaleX:1.5, scaleY:1.5, alpha:0, ease:Quint.easeInOut, onComplete:cleanup});
		}
		
		private function cleanup():void
		{
			this.parent.removeChild(this);
		}
		
		/**********************************************************
		 * 
		 * 	Set Number of Bacteria
		 * 
		 **********************************************************/
		public function numBacteria(value:int):void
		{
			if(!_isDead){
				_numBacteria = value;
							
				/** Angle of Progress Bar position */
				if(value <= _minBacteria){
					_angle = _minBacteria * _bacteriaSegementAngle
					
					if(_isShowing) hideProgress();
						
				}else{
					_angle = value * _bacteriaSegementAngle
						
					if(!_isShowing) showProgress();
					
				}
				
				/** Delay to counter rapid signals */
				TweenMax.killDelayedCallsTo(moveCounter);
				TweenMax.delayedCall(0.2, moveCounter);
			}
			
		}
		
		private function moveCounter():void
		{
			TweenMax.to(this.gemCounter, 0.4, {shortRotation:{rotation:_angle}, ease:Back.easeInOut});
		}
		
		/**********************************************************
		 * 
		 * 	Set Progress of Reaction
		 * 
		 **********************************************************/
		public function progress(gemCount:int, percent:Number):void
		{
			/** Progress of gem collection */
			this.gemCounter.progressBar.rotation = ((_gems - gemCount) * _progressSegmentAngle) + _progressSegmentAngle * percent;
			
		}
		
	}
}