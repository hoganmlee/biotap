package com.sajakfarki.biopets.utils
{
	import flash.events.TimerEvent;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextFieldAutoSize;
	import flash.utils.Timer;
	
	import org.osflash.signals.Signal;

	public class TimeCounter extends timeCounter
	{
		
		public var timesup:Signal = new Signal();
		
		private var _timer:Timer;
		private var _time:int;
		
		
		public function TimeCounter(time:int)
		{
			super();
			
			_time = time;
			
			this.timeCount.autoSize = TextFieldAutoSize.CENTER;
			this.timeCount.multiline = false;
			this.timeCount.antiAliasType=AntiAliasType.NORMAL;
			this.timeCount.gridFitType = GridFitType.NONE;
						
			_timer = new Timer(1000);
			_timer.addEventListener(TimerEvent.TIMER, updateTime);
			
			setTime();
		}
		
		public function get time():int
		{
			return _time;
		}
		
		protected function updateTime(event:TimerEvent):void
		{
			_time--;
			
			setTime();
			
			if(_time == 0){
				
				_timer.removeEventListener(TimerEvent.TIMER, updateTime);
				
				timesup.dispatch();
				
			}
			
		}
		
		private function setTime():void
		{
			var minutes:int = _time / 60;
			var seconds:int = _time % 60;
			
			if(seconds < 10){
				this.timeCount.text = minutes + ":0" + seconds;
			}else{
				this.timeCount.text = minutes + ":" + seconds;
			}
			
		}
		
		public function pause():void
		{
			_timer.stop();
		}
		
		public function start():void
		{
			_timer.start();
		}
		
			
			
			
	}
}