package com.sajakfarki.biopets.utils
{
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextFieldAutoSize;

	public class GemCounter extends gemCounter
	{
		public function GemCounter()
		{
			super();
			
			this.gemCountMC.gemCount.autoSize = TextFieldAutoSize.CENTER;
			this.gemCountMC.gemCount.multiline = false;
			this.gemCountMC.gemCount.antiAliasType=AntiAliasType.NORMAL;
			this.gemCountMC.gemCount.gridFitType = GridFitType.NONE;
			
		}
		
		public function setCount(value:int):void
		{
			
			this.gemCountMC.gemCount.text = value.toString();
			
			this.producedMC.x = this.gemCountMC.width/2;
			
			
		}
		
	}
}