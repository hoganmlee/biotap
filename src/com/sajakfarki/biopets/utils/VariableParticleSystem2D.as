package com.sajakfarki.biopets.utils
{
	import de.nulldesign.nd2d.display.ParticleSystem2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.utils.ParticleSystemPreset;
	
	import flash.events.Event;
	import flash.utils.getTimer;
	
	public class VariableParticleSystem2D extends ParticleSystem2D
	{
		private var _isDead:Boolean = false;
		
		public function VariableParticleSystem2D(textureObject:Texture2D, maxCapacity:uint, preset:ParticleSystemPreset, burst:Boolean=false)
		{
			super(textureObject, maxCapacity, preset, burst);
		}
		
		public function set isDead(value:Boolean):void
		{
			_isDead = value;
		}
				
		override protected function step(elapsed:Number):void {
			
			currentTime = getTimer() - startTime;
			
			if(activeParticles < maxCapacity && !_isDead) {
				
				activeParticles = Math.min(Math.ceil(currentTime / preset.spawnDelay), maxCapacity);
				
			}
			
			if(_isDead){
				
				activeParticles = 0;
				
			}
			
			if(burst && !burstDone && (currentTime > lastParticleDeathTime)) {
				burstDone = true;
				dispatchEvent(new Event(Event.COMPLETE));
			}
			
		}
		
		override public function reset():void
		{
			super.reset();
			
			_isDead = false;
		}
		
	}
}