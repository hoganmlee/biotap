package com.sajakfarki.biopets.utils
{
	public class FastRandom
	{
		private static const FASTRANDOMTOFLOAT:Number = 1 / uint.MAX_VALUE;
		private static var fastrandomseed:uint = Math.random() * uint.MAX_VALUE;
		
		public static function getRandom():Number
		{
			fastrandomseed ^= (fastrandomseed << 21);
			fastrandomseed ^= (fastrandomseed >>> 35);
			fastrandomseed ^= (fastrandomseed << 4);
			return (fastrandomseed * FASTRANDOMTOFLOAT);
		}
		
		/** Random from Min to Max */
		public static function randomRange( min : Number, max : Number = NaN ) : Number
		{
			if ( isNaN(max) )
			{
				max = min;
				min = 0;
			}
			
			return getRandom() * ( max - min ) + min;
		}
	}
}