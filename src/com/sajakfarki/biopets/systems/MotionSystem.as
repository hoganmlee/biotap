package com.sajakfarki.biopets.systems
{
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.nodes.*;
	import com.tomseysdavies.ember.core.IEntityManager;
	import com.tomseysdavies.ember.core.IFamily;
	import com.tomseysdavies.ember.core.ISystem;
	
	import flash.geom.Vector3D;

	public class MotionSystem implements ISystem
	{
		[Inject]
		public var entityManager : IEntityManager;
		
		[Inject]
		public var stageState : StageState;
		
		[Inject]
		public var tick : Move;
		
		private var family : IFamily;
		
		//private var _pos : Vector3D = new Vector3D();
		private var _newPos : Vector3D = new Vector3D();
		
		private var _node : MotionNode;
		private var _position : Position;
		private var _motion : Motion;
		
		private var _distance : Number;
		
		public function onRegister() : void
		{
			family = entityManager.getEntityFamily( MotionNode );
			tick.add( update );
		}
		
		private function update( time : Number ) : void
		{			
			for(family.start(); family.hasNext; family.next() ){
				
				_node = family.currentNode;
				_position = _node.position;
				_motion = _node.motion;
				
				//_pos.x = _position.point.x;
				//_pos.y = _position.point.y;
				//_pos.z = 0;
				
				_newPos.x = _position.point.x;
				_newPos.y = _position.point.y;
				//_newPos.z = 0;
				
				
				/** scale by Time */
				_motion.acceleration.scaleBy(time*25);
				
													
				_motion.velocity.incrementBy(_motion.acceleration);
																
//				if ( _motion.velocity.lengthSquared > _motion.maxSpeedSQ )
//				{
//					_motion.velocity.normalize();
//					_motion.velocity.scaleBy(_motion.maxSpeed);
//				}
							
				_newPos.incrementBy(_motion.velocity);
								
				_motion.acceleration.x = 0;
				_motion.acceleration.y = 0;
				//_motion.acceleration.z = 0;
				
				//if( !_newPos.equals(_pos) )
				//{
					//set new point
					_position.point.x = _newPos.x;
					_position.point.y = _newPos.y;
					
					var radians:Number = Math.atan2(- _motion.velocity.y,  - _motion.velocity.x);
					var degrees:Number = (radians/Math.PI)*180;
					_position.rotation = degrees + 270;
					
				//}				
			}
		}
		
		public function dispose() : void
		{
			tick.remove( update );
		}
	}
}
