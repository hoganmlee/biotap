package com.sajakfarki.biopets.systems
{
	import com.sajakfarki.biopets.components.Orbit;
	import com.sajakfarki.biopets.components.Position;
	import com.sajakfarki.biopets.components.StageState;
	import com.sajakfarki.biopets.signals.Move;
	import com.sajakfarki.biopets.systems.nodes.OrbitNode;
	import com.tomseysdavies.ember.core.IEntityManager;
	import com.tomseysdavies.ember.core.IFamily;
	import com.tomseysdavies.ember.core.ISystem;
	
	public class OrbitSystem implements ISystem
	{		
		[Inject]
		public var entityManager : IEntityManager;
		
		[Inject]
		public var stageState : StageState;
		
		[Inject]
		public var tick : Move;
		
		private var family : IFamily;
		
		public function onRegister() : void
		{
			family = entityManager.getEntityFamily( OrbitNode );
			tick.add( update );
		}
		
		private function update( time : Number ) : void
		{
			//var node : OrbitNode;
						
			for ( family.start(); family.hasNext; family.next() )
			{				
				//family.currentNode.position.point.x += family.currentNode.orbit.velocity.x * time;
				//family.currentNode.position.point.y += family.currentNode.orbit.velocity.y * time;
				
				if ( family.currentNode.position.point.x < 0 )
				{
					family.currentNode.position.point.x += stageState.width;
				}
				if ( family.currentNode.position.point.x > stageState.width )
				{
					family.currentNode.position.point.x -= stageState.width;
				}
				if ( family.currentNode.position.point.y < 0 )
				{
					family.currentNode.position.point.y += stageState.height;
				}
				if ( family.currentNode.position.point.y > stageState.height )
				{
					family.currentNode.position.point.y -= stageState.height;
				}
				
				family.currentNode.position.rotation += family.currentNode.orbit.angularVelocity * time * 180 / Math.PI;
				
			}
		}
		
		public function dispose() : void
		{
			tick.remove( update );
		}
	}
}