package com.sajakfarki.biopets.systems
{
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.*;
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.nodes.*;
	import com.sajakfarki.biopets.utils.FastRandom;
	import com.tomseysdavies.ember.base.EntityManager;
	import com.tomseysdavies.ember.core.IEntityManager;
	import com.tomseysdavies.ember.core.IFamily;
	import com.tomseysdavies.ember.core.ISystem;
	
	import de.nulldesign.nd2d.display.Scene2D;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class GameManager implements ISystem
	{
		[Inject]
		public var gameBatch :GameBatches;
		
		[Inject]
		public var entityManager  : IEntityManager;
		
		[Inject]
		public var levelCreator : LevelCreator;
		
		[Inject]
		public var stageState : StageState;
				
		[Inject]
		public var tick : PreUpdate;
				
		public function onRegister():void
		{
			tick.add( update );
		}
				
		private function update() : void
		{
			
			if( !stageState.init )
			{
				trace(">>> GameManager Init");
												
				/** Create Bacteria */
				for( var j:int = 0; j < stageState.bacteriaPostion.length; j++ )
				{
					stageState.numBacteria = stageState.bacteriaPostion.length;
					
					levelCreator.createBacteria(stageState.bacteriaType,
														      stageState.bacteriaRadius,
															  stageState.bacteriaPostion[j].x,
															  stageState.bacteriaPostion[j].y,
															  stageState.maxBacteriaSpeed, 
															  stageState.maxBacteriaForce,
															  false);
				}
				
				/** Create Bad Bacteria */
				for( var l:int = 0; l < stageState.badBacteriaPosition.length; l++ )
				{
					levelCreator.createBacteria(stageState.badBacteriaType,
															  stageState.badBacteriaRadius,
															  FastRandom.getRandom()*stageState.width, 
															  FastRandom.getRandom()*stageState.height,
															  stageState.maxBadBacteriaSpeed,
															  stageState.maxBadBacteriaForce,
															  true);
															
				}
				
				for (var i2:int = 0; i2 < stageState.stickyPosition.length; i2++) 
				{					
					levelCreator.createBadPoo(BadGuyTypes.DOUCHEBAG,
						stageState.stickyPosition[i2].scale,
						stageState.stickyPosition[i2].pos.x,
						stageState.stickyPosition[i2].pos.y);
				}
				
				
				for (var i3:int = 0; i3 < stageState.killerPosition.length; i3++) 
				{
					levelCreator.createBadPoo(BadGuyTypes.JERK,
						stageState.killerPosition[i3].scale,
						stageState.killerPosition[i3].pos.x,
						stageState.killerPosition[i3].pos.y);
				}
				
				/** Create Poos */
				for( var i:int = 0; i < stageState.pooPostion.length; i++ )
				{
//					var xPos:int = pooPosition[i].x;
//					var yPos:int = pooPosition[i].y;
					
					levelCreator.createPoo(stageState.compoundType,
														stageState.stageType,
													   "poop"+i,
													   Math.ceil(GameState.gemsCollected / stageState.pooPostion.length),
													   stageState.pooPostion[i].x, 
													   stageState.pooPostion[i].y, 
													   stageState.minPooRadius, 
													   stageState.maxPooRadius,
													   stageState.rateOfRadiusExpand,
													   stageState.rateOfRadiusCollapse,
													   stageState.rateOfPop,
													   stageState.popTime,
													   stageState.rateOfReaction,
													   stageState.reactionStartTime,
													   stageState.minBacteria,
													   stageState.maxBacteria);
					
					//stageState.hudPosition.dispatch("poop"+i, new Point(stageState.pooPostion[i].x, stageState.pooPostion[i].y), stageState.numGems, stageState.minBacteria, stageState.maxBacteria);
					
				}
					
				/** Create Taps*/
				//if(stageState.showHeat){
					for (var k:int = 0; k < Tap.MAX_TAPS; k++) 
					{
						levelCreator.createTap(stageState.tapRadius, -200, -200);
					}				
				//}
					
				
				/** Heat Control  */
				
				if(stageState.showHeat){
					levelCreator.createHeat(60,
														 stageState.width-70, 
														 stageState.height-100, 
														 stageState.maxHeatTemp, 
														 stageState.minHeatTemp, 
														 stageState.currentHeat,
														 stageState.maxHeatOptimal, 
														 stageState.minHeatOptimal,
														 stageState.heatStep,
														 stageState.coolRate,
														 stageState.heatRate);
				}
				
				
				stageState.init = true;
				
				
				//TODO: stage start mechanism, show screen
				//stageState.paused = true;
								
				
			}else{
			
				//TODO: system variables
				
				
				
				
			
			}
			
		}
		
		public function dispose():void
		{
			tick.remove( update );
		}
	}
}
