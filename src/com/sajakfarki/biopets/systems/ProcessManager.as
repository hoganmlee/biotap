package com.sajakfarki.biopets.systems
{
	
	import com.sajakfarki.biopets.components.StageState;
	import com.sajakfarki.biopets.signals.*;
	import com.tomseysdavies.ember.core.ISystem;
	
	import de.nulldesign.nd2d.display.Scene2D;
	
	import flash.events.Event;
	import flash.utils.getTimer;
	
	public class ProcessManager implements ISystem
	{
		private static const MAX_FRAME_TIME : Number = 1.05;
		
		[Inject]
		public var preUpdate : PreUpdate;
		
		[Inject]
		public var update : Update;
		
		[Inject]
		public var move : Move;
		
		[Inject]
		public var resolveCollisions : ResolveCollisions;
		
		[Inject]
		public var render : Render;
		
		[Inject]
		public var viewContext : Scene2D;
		
		[Inject]
		public var stageState : StageState;
		
		
		private var firstStart:Boolean = true;
		private var time : uint;		
		
		public function onRegister() : void
		{
			start();
		}
		
		private function frameUpdate( event : Event ) : void
		{
			var oldTime : uint = time;
			time = getTimer();
			
			var frameTime : Number = ( time - oldTime ) / 1000;
			
			//trace(frameTime);
			
			if( frameTime > MAX_FRAME_TIME )
			{
				frameTime = MAX_FRAME_TIME;
			}
			
			if(firstStart){
				
				preUpdate.dispatch();
				update.dispatch( frameTime );
				move.dispatch( frameTime );
				render.dispatch();
				
				trace(">> ProcessManager Init");
				firstStart = false;
				
				if(stageState.paused){
					dispose();
				}
				
				stageState.levelInitialized.dispatch();
				
			}else{
								
				//preUpdate.dispatch();
				update.dispatch( frameTime );
				move.dispatch( frameTime );
				resolveCollisions.dispatch( frameTime );
				render.dispatch();
				
			}
			
		}
		
		public function start() : void
		{
			time = getTimer();
			viewContext.stage.addEventListener( Event.ENTER_FRAME, frameUpdate );
		}
		
		public function dispose() : void
		{
			viewContext.stage.removeEventListener( Event.ENTER_FRAME, frameUpdate );
		}
		
	}
}