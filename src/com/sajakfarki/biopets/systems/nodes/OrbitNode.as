package com.sajakfarki.biopets.systems.nodes
{
	import com.sajakfarki.biopets.components.Orbit;
	import com.sajakfarki.biopets.components.Position;
	import com.tomseysdavies.ember.base.Node;
	
	import flash.utils.Dictionary;
	
	public class OrbitNode extends Node
	{
		public static const componentClasses : Array = [ Position, Orbit ];
		
		public var position : Position;
		public var orbit : Orbit;
		
		public function OrbitNode( entityID : String, components : Dictionary )
		{
			super( entityID, components );
			position = components[ Position ];
			orbit = components[ Orbit ];
		}
	}
}