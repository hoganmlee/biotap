package com.sajakfarki.biopets.systems.nodes
{
	import com.sajakfarki.biopets.components.Position;
	import com.sajakfarki.biopets.components.Tap;
	import com.tomseysdavies.ember.base.Node;
	
	import flash.utils.Dictionary;
	
	public class TapControlNode extends Node
	{
		public static const componentClasses : Array = [ Tap, Position ];
		
		public var tap : Tap;
		public var position : Position;
		
		public function TapControlNode(entityID:String, components:Dictionary)
		{
			super(entityID, components);
			
			tap = components[ Tap ];
			position = components[ Position ]; 
			
		}
		
	}
}