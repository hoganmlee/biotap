package com.sajakfarki.biopets.systems.nodes
{
	import com.sajakfarki.biopets.components.Poo;
	import com.sajakfarki.biopets.components.Position;
	import com.tomseysdavies.ember.base.Node;
	
	import flash.utils.Dictionary;
	
	public class PooCollisionNode extends Node
	{
		public static const componentClasses : Array = [ Poo, Position ];
		
		public var poo : Poo;
		public var position : Position;
		
		public function PooCollisionNode(entityID:String, components:Dictionary)
		{
			super(entityID, components);
			
			poo = components[ Poo ];
			position = components[ Position ]; 
				
		}
	}
}