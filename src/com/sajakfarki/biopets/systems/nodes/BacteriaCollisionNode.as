package com.sajakfarki.biopets.systems.nodes
{
	import com.sajakfarki.biopets.components.Bacteria;
	import com.sajakfarki.biopets.components.Motion;
	import com.sajakfarki.biopets.components.Position;
	import com.tomseysdavies.ember.base.Node;
	
	import flash.utils.Dictionary;
	
	public class BacteriaCollisionNode extends Node
	{
		public static const componentClasses : Array = [ Bacteria, Position, Motion ];
		
		public var bacteria : Bacteria;
		public var position : Position;
		public var motion : Motion;
		
		
		public function BacteriaCollisionNode(entityID:String, components:Dictionary)
		{
			super(entityID, components);
			
			bacteria = components[ Bacteria ];
			position = components[ Position ];
			motion = components[ Motion ];
			
		}
	}
}