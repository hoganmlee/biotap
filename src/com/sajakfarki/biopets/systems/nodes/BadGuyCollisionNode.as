package com.sajakfarki.biopets.systems.nodes
{
	import com.sajakfarki.biopets.components.BadGuy;
	import com.sajakfarki.biopets.components.Poo;
	import com.sajakfarki.biopets.components.Position;
	import com.tomseysdavies.ember.base.Node;
	
	import flash.utils.Dictionary;
	
	public class BadGuyCollisionNode extends Node
	{
		public static const componentClasses : Array = [ BadGuy, Position ];
		
		public var badguy : BadGuy;
		public var position : Position;
		
		public function BadGuyCollisionNode(entityID:String, components:Dictionary)
		{
			super(entityID, components);
			
			badguy = components[ BadGuy ];
			position = components[ Position ];
				
		}
	}
}