package com.sajakfarki.biopets.systems.nodes
{
	import com.sajakfarki.biopets.components.*;
	import com.tomseysdavies.ember.base.Node;
	
	import flash.utils.Dictionary;
	
	public class MotionNode extends Node
	{
		public static const componentClasses : Array = [ Position, Motion ];
		
		public var position : Position;
		public var motion : Motion;
		
		public function MotionNode( entityID : String, components : Dictionary )
		{
			super( entityID, components );
			position = components[ Position ];
			motion = components[ Motion ];
		}
	}
}