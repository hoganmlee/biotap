package com.sajakfarki.biopets.systems.nodes
{
	import com.sajakfarki.biopets.components.HeatControl;
	import com.sajakfarki.biopets.components.Position;
	import com.tomseysdavies.ember.base.Node;
	
	import flash.utils.Dictionary;
	
	public class HeatControlNode extends Node
	{
		public static const componentClasses : Array = [ HeatControl , Position ];
		
		public var control: HeatControl;
		public var position : Position;
		
		public function HeatControlNode(entityID:String, components:Dictionary)
		{
			super(entityID, components);
			
			control = components[ HeatControl ];
			position = components[ Position ];
			
		}
	}
}