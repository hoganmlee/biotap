package com.sajakfarki.biopets.systems
{
	import com.sajakfarki.biopets.components.Cards;
	import com.sajakfarki.biopets.components.Position;
	import com.sajakfarki.biopets.components.Tap;
	import com.sajakfarki.biopets.game.LevelCreator;
	import com.sajakfarki.biopets.signals.Update;
	import com.sajakfarki.biopets.systems.nodes.TapControlNode;
	import com.sajakfarki.biopets.utils.FastRandom;
	import com.tomseysdavies.ember.core.IEntityManager;
	import com.tomseysdavies.ember.core.IFamily;
	import com.tomseysdavies.ember.core.ISystem;
	
	import de.nulldesign.nd2d.display.Scene2D;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	public class TapSystem implements ISystem
	{		
		[Inject]
		public var entityManager : IEntityManager;
		
		[Inject]
		public var tick : Update;
		
		[Inject]
		public var viewContext : Scene2D;
		
//		[Inject]
//		public var keyPoll : KeyPoll;
		
		
		/** Music */
		[Embed(source = "../sounds/ClickFX.mp3")]
		private var _clickMP3:Class;
		private var _tapSound:Sound = new _clickMP3() as Sound;
		
		public static var scratches:Vector.<Sound>;
		
		private var _channel:SoundChannel;
				
		private var _isDown:Boolean = false;
				
		private var family : IFamily;
		
		public function onRegister() : void
		{
			family = entityManager.getEntityFamily( TapControlNode );
			
			tick.add( update );
			
		}
		
		public function start():void
		{
			tick.add( update );
			viewContext.stage.addEventListener(MouseEvent.MOUSE_DOWN, tapped);
		}
		
		protected function tapped(event:MouseEvent):void
		{
			_isDown = true;
		}
		
		private function update( time : Number ) : void
		{
			
			/**********************************************
			 * 
			 *  If user is Tapping -> Check Tap Pool -> Start Tap
			 * 
			 **********************************************/
			if(_isDown){
				
				_isDown = false;
				
				for(family.start(); family.hasNext; family.next()) {					
					
					var tapNode : TapControlNode;
					tapNode = family.currentNode;
					
					/** Check for a free tap node */
					if(tapNode.tap.tapLifeRemaining <= 0){
						
						if(Cards.ISSCRATCH){
							_channel = scratches[int(FastRandom.randomRange(0, 5))].play(0, 1);
						}else{
							_channel = _tapSound.play(0, 1);
						}
						
						/** Set tap at mouseX, Y */
						tapNode.position.point = new Point(viewContext.mouseX, viewContext.mouseY);
						tapNode.tap.tapLifeRemaining = tapNode.tap.tapLifetime;
						tapNode.position.collisionRadius = tapNode.tap.collisionSize;
						
						return;
						
					}
					
				}
				
			}
			
			/**  */
			for(family.start(); family.hasNext; family.next()) {
				
				var tNode : TapControlNode;
				tNode = family.currentNode;
				
				if(tNode.tap.tapLifeRemaining > 0){
					
					//shrink display
					tNode.tap.tapLifeRemaining -= time;
					tNode.position.collisionRadius = tNode.tap.collisionSize * ( tNode.tap.tapLifeRemaining / tNode.tap.tapLifetime );
					
				}else{

					//TODO: visible = false;
					
					
				}
				
			}
			
		}
		
		public function dispose() : void
		{
			tick.remove( update );
			viewContext.stage.removeEventListener(MouseEvent.MOUSE_DOWN, tapped);
		}
	}
}