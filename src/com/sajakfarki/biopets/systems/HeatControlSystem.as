package com.sajakfarki.biopets.systems
{
	import com.sajakfarki.biopets.components.Cards;
	import com.sajakfarki.biopets.components.HeatControl;
	import com.sajakfarki.biopets.components.StageState;
	import com.sajakfarki.biopets.signals.Update;
	import com.sajakfarki.biopets.systems.nodes.*;
	import com.tomseysdavies.ember.core.IEntityManager;
	import com.tomseysdavies.ember.core.IFamily;
	import com.tomseysdavies.ember.core.ISystem;
	
	import de.nulldesign.nd2d.display.Scene2D;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	import flashx.textLayout.tlf_internal;
		
	public class HeatControlSystem implements ISystem
	{
		[Inject]
		public var entityManager : IEntityManager;
		
		[Inject]
		public var tick : Update;
		
		[Inject]
		public var stageState:StageState;
				
		[Inject]
		public var viewContext : Scene2D;
		
		private var _heatControls : IFamily;
		
		private var _isDown:Boolean;
		
		private var _tapPoint : Point 
		
		/** Torch */
		private var _torch:Sound = new Cards.TORCHMP3() as Sound;
		private var _heat:Sound = new Cards.HEATMP3() as Sound;
		
		/** Warning */
		[Embed(source = "../sounds/TemperatureWarning.mp3")]
		private var _warningMP3:Class;
		private var _warning:Sound = new _warningMP3() as Sound;
		
		
		private var _channel:SoundChannel;
		private var _channel2:SoundChannel;
		private var _isPlaying:Boolean;
		
		
		
		public function onRegister() : void
		{
			_heatControls = entityManager.getEntityFamily( HeatControlNode );
			tick.add( update );
			
			viewContext.stage.addEventListener(MouseEvent.MOUSE_DOWN, tapped);
			viewContext.stage.addEventListener(MouseEvent.MOUSE_UP, uptapped);
			
		}
		
		protected function uptapped(event:MouseEvent):void
		{
			_isDown = false;
			
			if(_channel){
				
				_isPlaying = false;
				_channel.stop();
				_channel = null;
			}
			
		}
		
		protected function tapped(event:MouseEvent):void
		{			
			_isDown = true;
			_tapPoint = new Point(viewContext.mouseX, viewContext.mouseY);  
		}
		
		private function update( time : Number ) : void
		{
			
			if(stageState.showHeat){
				
				_heatControls.start();
				
				var heatNode : HeatControlNode;
				heatNode = _heatControls.currentNode;
				
				/** If user is pressing */
				if(_isDown){
					
					if ( Point.distance( heatNode.position.point, _tapPoint) <= heatNode.position.collisionRadius )
					{
						
						if(!_isPlaying){
							
							_isPlaying = true;
							
							if(Cards.ISHEAT){
								_channel = _heat.play(0,99);
							}else{							
								_channel = _torch.play(0,99);
							}
						}
						
						heatNode.control.currentHeat += heatNode.control.heatRate * time;
					}
					
				}	
				
				/** cool down the system */
				if(heatNode.control.currentHeat != heatNode.control.min){
					heatNode.control.currentHeat -= heatNode.control.coolRate * time;
				}
				
				/** check min/max */
				if(heatNode.control.currentHeat < heatNode.control.min){
					heatNode.control.currentHeat = heatNode.control.min;
				}else if(heatNode.control.currentHeat > heatNode.control.max){
					heatNode.control.currentHeat = heatNode.control.max;
				}
				
				
				//set temperature
				heatNode.position.temperature = heatNode.control.currentHeat;
				
				
				/** detect optimal */
				if(heatNode.control.currentHeat > heatNode.control.optimalMin && heatNode.control.currentHeat < heatNode.control.optimalMax){
					
					if(!heatNode.control.isOptimal){
						heatNode.control.isOptimal = true;
						
						/** dispatch optimal */
						stageState.optimalTemperature.dispatch(true);
					}
					
				}else{
					
					if(heatNode.control.isOptimal){
					
						heatNode.control.isOptimal = false;
						
						/** dispatch optimal */
						stageState.optimalTemperature.dispatch(false);
						
						_channel2 = _warning.play();
						
						
					}
					
				}
			
			}

		}
		
		public function dispose() : void
		{
			tick.remove( update );
		}
	}
}