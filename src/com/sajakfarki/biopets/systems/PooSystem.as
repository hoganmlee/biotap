package com.sajakfarki.biopets.systems
{
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.nodes.*;
	import com.tomseysdavies.ember.base.Node;
	import com.tomseysdavies.ember.core.IEntityManager;
	import com.tomseysdavies.ember.core.IFamily;
	import com.tomseysdavies.ember.core.ISystem;
	
	import flash.geom.Vector3D;
	
	import flashx.textLayout.tlf_internal;
	
	public class PooSystem implements ISystem
	{
		private static const ACCELERATION_MULTIPLIER :  Number = 1.1;
		
		[Inject]
		public var entityManager : IEntityManager;
		
		[Inject]
		public var stageState : StageState;
		
		[Inject]
		public var tick : Update;
		
		private var poos : IFamily;
		//private var gems : IFamily;
		
		
		private var _poo : Poo;
		private var _position : Position;
				
		
		public function onRegister() : void
		{
			poos = entityManager.getEntityFamily( PooCollisionNode );
			
			//gems = entityManager.getEntityFamily( GemCollectNode )
			
			tick.add( update );
		}
		
		private function update( time : Number ) : void
		{					
			if(!stageState.finished){
				
				if(poos.size == 0){
					
					//poos finished
					stageState.stageFinished.dispatch(true);
					stageState.showHeat = false;
					
				}
				
				
				/** Poo Loop */
				for(poos.start(); poos.hasNext; poos.next() ){
					
					_poo = poos.currentNode.poo;
					_position = poos.currentNode.position;
					
					/** Number of Gems > 0 */
					if(_position.numberOfGems > 0){
						
						if(_poo.isOptimal && _poo.collectionCount > _poo.minBacteria){
							
							if(stageState.badBacteriaInfection != 1 && _poo.badCount > 0){
							
								_position.reactionTime -= ( _poo.rateOfReaction * Math.pow(stageState.badBacteriaInfection, _poo.badCount ) * time ) * ( ( _poo.collectionCount - _poo.minBacteria ) * ACCELERATION_MULTIPLIER );
									
							}else{
								
								_position.reactionTime -= ( _poo.rateOfReaction * time ) * ( ( _poo.collectionCount - _poo.minBacteria ) *  ACCELERATION_MULTIPLIER );
															
							}
															if(_position.reactionTime < 0){
								
								//trace("Pooooof!!");
								
								_position.numberOfGems--;
								
								if(_position.numberOfGems >= 0){
								
									_position.reactionTime = _position.startTime;
									
									/** dispatch reaction progress signal */
									stageState.gemCollected.dispatch(_position.point);
								
								}
								
							}
							
							/** dispatch reaction progress signal */
							//stageState.reactionProgress.dispatch(_poo.ID, _poo.numberOfGems, (_poo.startTime - _poo.reactionTime) / _poo.startTime);
							//_poo
						}
						
					}else{
												
						if(_poo.collectionCount > 0){
													
							for each (var i:* in _poo.bacteriaCollection) 
							{
								_poo.collectionCount--;
								i.isEating = false;
								i.target = null;
								delete _poo.bacteriaCollection[j];
							}
										
							_position.isDead = true;
							
							poos.remove(poos.currentNode.entityID);
							
						}
						
					} 
					
					/************************************************************
					 *  Poo is NOT optimal POP off bacteriaCollection
					 ************************************************************/
					if(!_poo.isOptimal){
						
						if(_poo.collectionCount> 0){
							
							_poo.popCount += _poo.rateOfPop * time;
													
							if(_poo.popCount >= _poo.popTime){
								
								for each (var j:* in _poo.bacteriaCollection) 
								{
									_poo.collectionCount--;
									j.isEating = false;
									j.target = null;
									delete _poo.bacteriaCollection[j];
									
									break;
								}
															
								/** dispatch bacteria collection signal */
								//stageState.numberOfBacteria.dispatch(_poo.ID, _poo.collectionCount);
								
								_poo.popCount = 0;
								
							}
													
						}
						
					} /** END IF IS OPTIMAL */
									
					
				} /** END POO LOOP */
			
			}else{
			
				/** Poo Loop */
				for(poos.start(); poos.hasNext; poos.next() ){
				
					poos.currentNode.position.isDead = true;
					
				}
				
			}
			
		}
		
		public function dispose() : void
		{
			tick.remove( update );
		}
	}
}
