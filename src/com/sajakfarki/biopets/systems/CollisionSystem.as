package com.sajakfarki.biopets.systems
{
	import com.sajakfarki.biopets.components.Bacteria;
	import com.sajakfarki.biopets.components.Poo;
	import com.sajakfarki.biopets.components.StageState;
	import com.sajakfarki.biopets.game.LevelCreator;
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	import com.sajakfarki.biopets.signals.ResolveCollisions;
	import com.sajakfarki.biopets.systems.nodes.*;
	import com.sajakfarki.biopets.utils.FastRandom;
	import com.tomseysdavies.ember.core.IEntityManager;
	import com.tomseysdavies.ember.core.IFamily;
	import com.tomseysdavies.ember.core.ISystem;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	
	public class CollisionSystem implements ISystem
	{
		[Inject]
		public var entityManager : IEntityManager;
		
		[Inject]
		public var levelCreator : LevelCreator;
		
		[Inject]
		public var tick : ResolveCollisions;
		
		[Inject]
		public var stageState : StageState;
		
		private var _poos : IFamily;
		private var _heatControls : IFamily;
		private var _bacterias : IFamily;
		private var _userTaps : IFamily;
		private var _badGuys : IFamily;
		
		// holder variables
		private var _targetPosition : Vector3D = new Vector3D();
		private var _currentPosition : Vector3D = new Vector3D();
		private var _distance : Number;
		
		private var _badNode : BadGuyCollisionNode;
		private var _pooNode : PooCollisionNode;
		private var _bacteriaNode : BacteriaCollisionNode;
		private var _tapNode : TapControlNode;
		private var _heatControlNode : HeatControlNode;
		
		private var _repell : Boolean = false;
		
		private var _rectangle : Rectangle;
		
		
		public function onRegister() : void
		{
			_poos = entityManager.getEntityFamily( PooCollisionNode );
			_bacterias = entityManager.getEntityFamily( BacteriaCollisionNode );
			_userTaps = entityManager.getEntityFamily( TapControlNode );
			_heatControls = entityManager.getEntityFamily( HeatControlNode );
			_badGuys = entityManager.getEntityFamily( BadGuyCollisionNode );
			
			
			_rectangle = new Rectangle(0, 0, stageState.width, stageState.height);
						
			tick.add( update );
		}
		
		private function update( time : Number ) : void
		{
			
			
			//TODO: calc repell time, ramp velocity back down normal velocity of bacteria
						
			
			
			_repell = false;
			
			_heatControls.start();
			_heatControlNode = _heatControls.currentNode;
			
			//var badGuyNode : BadGuyNode;
			
			/** Bacterias Loop */
			for(_bacterias.start(); _bacterias.hasNext; _bacterias.next() )
			{
				_bacteriaNode = _bacterias.currentNode;
				
				/** Check if already Repelling */
				if(!_bacteriaNode.bacteria.isRepelling){
				
					/** Taps Loop */
					for(_userTaps.start(); _userTaps.hasNext; _userTaps.next() )
					{
						_tapNode = _userTaps.currentNode;
						
						if ( Point.distance( _bacteriaNode.position.point, _tapNode.position.point) <= _tapNode.position.collisionRadius )
						{							
							_repell = true;
							
							if(_bacteriaNode.bacteria.isEating){
								
								/** Pop off of Poo */
								_bacteriaNode.bacteria.isEating = false;
								
								if(_bacteriaNode.bacteria.poo != null){
									
									/** Is Bad Bacteria */
									if(_bacteriaNode.bacteria.isBad){
										
										if(_bacteriaNode.bacteria.poo.badCount > 0){
											_bacteriaNode.bacteria.poo.badCount--;
										}
										
										
									}else{
										
										if(_bacteriaNode.bacteria.poo.collectionCount > 0){
											_bacteriaNode.bacteria.poo.collectionCount--;
										}
										
									}
									
									/** Clean up dictionary */
									delete _bacteriaNode.bacteria.poo.bacteriaCollection[_bacteriaNode.bacteria];
									
									/** dispatch collection signal */
//									stageState.numberOfBacteria.dispatch(_bacteriaNode.bacteria.poo.ID, _bacteriaNode.bacteria.poo.collectionCount);
									
									_bacteriaNode.bacteria.poo = null;
								}
								
								
																
							}else{
								
								_bacteriaNode.bacteria.isRepelling = true;
								_bacteriaNode.bacteria.target = _tapNode.position.point;
								
							}
							
						}
						
					}
					
				} /** END IF BACTERIA REPELLING */
					
					
				
				if(!stageState.finished){
					
					/** Poos Loop test against Bacteria*/
					for(_poos.start(); _poos.hasNext; _poos.next()){
						
						
						_pooNode = _poos.currentNode;
						
						
						/************************************ 
						 * If Optimal Temperature
						 ************************************/
						if(_heatControlNode.control.isOptimal){
							
							_pooNode.poo.isOptimal = true;
							
							/************************************ 
							 * Expand radius of Poo 
							 ************************************/
							if(_pooNode.position.collisionRadius != _pooNode.poo.maxRadius) {
								
								_pooNode.position.collisionRadius += _pooNode.poo.rateOfRadiusExpand * time;
								
								if(_pooNode.position.collisionRadius > _pooNode.poo.maxRadius){
									
									_pooNode.position.collisionRadius  = _pooNode.poo.maxRadius;
									
								}
								
							}
							
							/** If the Bacteria is not eating a Poo */
							if(!_bacteriaNode.bacteria.isEating){
								
								/** If the Poo is not Full */
								if(_pooNode.poo.collectionCount < _pooNode.poo.maxBacteria){
								
									/** Test Collision */
									if( Point.distance( _pooNode.position.point, _bacteriaNode.position.point ) <= _pooNode.position.collisionRadius ) {
										
										/** Is Bad Bacteria */
										if(_bacteriaNode.bacteria.isBad){
											
											_pooNode.poo.badCount++;
											
										}else{
											
											_pooNode.poo.collectionCount++;
											
										}
										
										/** Push onto Collection */
										_pooNode.poo.bacteriaCollection[_bacteriaNode.bacteria] = _bacteriaNode.bacteria;
										
										/** dispatch collection signal */
										//stageState.numberOfBacteria.dispatch(_pooNode.poo.ID, _pooNode.poo.collectionCount);
										
										_bacteriaNode.bacteria.poo = _pooNode.poo;
										_bacteriaNode.bacteria.isRepelling = false;
										_bacteriaNode.bacteria.isEating = true;
										_bacteriaNode.bacteria.target = _pooNode.position.point;
										
									}
									
								}
								
							}  /** END HEAT IS OPTIMAL */
							
						}else{
							/************************************ 
							 * If NOT Optimal Temperature
							 ************************************/
							/************************************ 
							 * Collapse radius of Poo 
							 ************************************/
							if(_pooNode.position.collisionRadius != _pooNode.poo.minRadius) {
								
								_pooNode.position.collisionRadius -= _pooNode.poo.rateOfRadiusCollapse * time;
								
								if(_pooNode.position.collisionRadius < _pooNode.poo.minRadius){
									
									_pooNode.position.collisionRadius  = _pooNode.poo.minRadius;
									
								}
								
							}
													
							_pooNode.poo.isOptimal = false;
							
						}
						
						_pooNode.position.numBacteria = _pooNode.poo.collectionCount;
						
					} /** END POO LOOP */
				
				}else{
					
					/** Release Optimal */
					
					if(_bacteriaNode.bacteria.isEating){
						
						/** Pop off of Poo */
						_bacteriaNode.bacteria.isEating = false;
					}
					
					for(_poos.start(); _poos.hasNext; _poos.next()){
						
						_poos.currentNode.poo.isOptimal = false;
						
					}
					
				}
				
				/** BadGuys Loop test against Bacteria*/
				for(_badGuys.start(); _badGuys.hasNext; _badGuys.next()){
					
					_badNode = _badGuys.currentNode;
					
					/** If the Bacteria is not eating a Poo */
					if(!_bacteriaNode.bacteria.isEating){
					
						/** Test Collision */
						if( Point.distance( _badNode.position.point, _bacteriaNode.position.point ) <= _badNode.position.collisionRadius ) {
							
							if(_badNode.badguy.type == BadGuyTypes.DOUCHEBAG){
								
								/** Is Bad Bacteria */
								if(_bacteriaNode.bacteria.isBad){
									
									_badNode.badguy.badCount++;
									
								}else{
									
									_badNode.badguy.collectionCount++;
									
								}
								
								/** Push onto Collection */
								_badNode.badguy.bacteriaCollection[_bacteriaNode.bacteria] = _bacteriaNode.bacteria;
								
	//							/** dispatch collection signal */
	//							stageState.numberOfBacteria.dispatch(_pooNode.poo.ID, _pooNode.poo.collectionCount);
								
								_bacteriaNode.bacteria.poo = _badNode.badguy;
								_bacteriaNode.bacteria.isRepelling = false;
								_bacteriaNode.bacteria.isEating = true;
								_bacteriaNode.bacteria.target = _badNode.position.point;
								
							}else{
								
								//Killer Poo
								_bacteriaNode.position.isDead = true;
								_bacterias.remove(_bacteriaNode.entityID);
								
							}
							
						}
					
					_badNode.position.numBacteria = _badNode.badguy.collectionCount;
					
					}
					
					
				}
				
				//TODO: pass time
				move(_bacteriaNode, _repell);
				constrainToRect(_bacteriaNode);
								
				_repell = false;
				
			}
			
		}
		
		private function move(bacteriaNode:BacteriaCollisionNode, repell:Boolean):void
		{
			_currentPosition.x = bacteriaNode.position.point.x;
			_currentPosition.y = bacteriaNode.position.point.y;
			
			if(bacteriaNode.bacteria.target){
				
				_targetPosition.x = bacteriaNode.bacteria.target.x;
				_targetPosition.y = bacteriaNode.bacteria.target.y;
				
				if(bacteriaNode.bacteria.isEating){
					//
					//flee(false, bacteriaNode, bacteriaNode.bacteria.target);
					bacteriaNode.motion.steeringForce = steer(bacteriaNode, _targetPosition , true, 150);
					
				}else if(bacteriaNode.bacteria.isRepelling){
					//
					//flee(true, bacteriaNode, bacteriaNode.bacteria.target);
					_distance = Vector3D.distance(_currentPosition, _targetPosition);
					
					bacteriaNode.motion.steeringForce = steer(bacteriaNode, _targetPosition , true, 60);
					
				}
				
				//distance from flee/attract position
				if(repell){
					bacteriaNode.motion.steeringForce.negate();
				}
				
				//wander(bacteriaNode, 1);
				
				bacteriaNode.motion.acceleration.incrementBy(bacteriaNode.motion.steeringForce);
								
			}else{
				
				//steer down
				//accelerate down
								
			}
			
			
			
			//TODO: IMPLEMENT WANDER INTO MOVE
			
			wander(bacteriaNode, 1);
			
		}
				
		private function wander( node : BacteriaCollisionNode, multiplier : Number = 0.1 ) : void
		{
			node.motion.wanderTheta += -node.motion.wanderStep + FastRandom.getRandom() * node.motion.wanderStep;
			node.motion.wanderPhi += -node.motion.wanderStep + FastRandom.getRandom() * node.motion.wanderStep;
			
			
			if ( FastRandom.getRandom() < 0.5 )
			{
				node.motion.wanderTheta = -node.motion.wanderTheta;
			}
			
			
			_currentPosition.x = node.position.point.x;
			_currentPosition.y = node.position.point.y;
			
			
			var pos : Vector3D = node.motion.velocity.clone();
			pos.normalize();
			pos.scaleBy(node.motion.wanderDistance);
			pos.incrementBy(_currentPosition);
						
			var offset : Vector3D = new Vector3D();
			
			offset.x = node.motion.wanderRadius * Math.cos(node.motion.wanderTheta);
			offset.y = node.motion.wanderRadius * Math.sin(node.motion.wanderPhi);
			
			node.motion.steeringForce = steer(node, pos.add(offset));
			
			node.motion.acceleration.incrementBy(node.motion.steeringForce);
			
		}
		
		private function steer( node:BacteriaCollisionNode, target : Vector3D, ease : Boolean = true, easeDistance : Number = 10 ) : Vector3D
		{
			node.motion.steeringForce = target.clone();
			node.motion.steeringForce.decrementBy(_currentPosition);
			
			_distance = node.motion.steeringForce.normalize();
			
			//trace(_distance);
			
//			if ( _distance > 0.00001 )
//			{
				if ( _distance < easeDistance && ease )
				{
					node.motion.steeringForce.scaleBy(node.motion.maxSpeed * ( _distance / easeDistance ));
				}
				else
				{
					if(node.bacteria.isRepelling){
						node.bacteria.isRepelling = false;
						node.bacteria.target = null;
					}
					node.motion.steeringForce.scaleBy(node.motion.maxSpeed);
				}
				
				node.motion.steeringForce.decrementBy(node.motion.velocity);
				
				if ( node.motion.steeringForce.lengthSquared > node.motion.maxForceSQ )
				{
					node.motion.steeringForce.normalize();
					node.motion.steeringForce.scaleBy(node.motion.maxForce);
				}
		//	}
			
			return node.motion.steeringForce;
		}
		
		private function constrainToRect(node : BacteriaCollisionNode) : void
		{
			if ( node.position.point.x < _rectangle.left + node.position.collisionRadius )
			{			
				node.position.point.x = _rectangle.left + node.position.collisionRadius;
				node.motion.velocity.x *= -1;
				
			}
			else if ( node.position.point.x > _rectangle.right - node.position.collisionRadius )
			{
				node.position.point.x = _rectangle.right - node.position.collisionRadius;
				node.motion.velocity.x *= -1;
			}
			
			if ( node.position.point.y < _rectangle.top + node.position.collisionRadius )
			{
				node.position.point.y = _rectangle.top + node.position.collisionRadius;
				node.motion.velocity.y *= -1;
			}
			else if ( node.position.point.y > _rectangle.bottom - node.position.collisionRadius )
			{	
				node.position.point.y = _rectangle.bottom - node.position.collisionRadius;
				node.motion.velocity.y *= -1;
			}
		}
		
		public function dispose() : void
		{
			tick.remove( update );
		}
	}
}