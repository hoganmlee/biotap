package com.sajakfarki.biopets.systems
{
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.scenes.GameScene;
	import com.sajakfarki.biopets.graphics.bacteria.*;
	import com.sajakfarki.biopets.graphics.compounds.PooView;
	import com.sajakfarki.biopets.graphics.compounds.base.DefaultPooView;
	import com.sajakfarki.biopets.graphics.compounds.base.IPooView;
	import com.sajakfarki.biopets.graphics.compounds.base.StickyPoo;
	import com.sajakfarki.biopets.graphics.controls.HeatView;
	import com.sajakfarki.biopets.graphics.controls.TapView;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.nodes.*;
	import com.tomseysdavies.ember.core.IEntityManager;
	import com.tomseysdavies.ember.core.IFamily;
	import com.tomseysdavies.ember.core.ISystem;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	public class RenderSystem implements ISystem
	{
		
		
		[Inject]
		public var contextView : Scene2D;
		
		[Inject]
		public var entityManager : IEntityManager;
				
		[Inject]
		public var tick : Render;
		
		[Inject]
		public var stageState : StageState;
		
		private var family : IFamily;
		
		
		/** Bacteria Death */
		[Embed(source = "../sounds/BacteriaDeath.mp3")]
		private var _bdMP3:Class;
		private var _bd:Sound = new _bdMP3() as Sound;
		
		private var _channel:SoundChannel;
		
		
		public function onRegister() : void
		{
			family = entityManager.getEntityFamily( RenderNode );
			tick.add( render );
		}
		
		public function render() : void
		{
			var node : RenderNode;
			var position : Position;
			var display : Display;
			var displayObject : Node2D;
			
			for( family.start(); family.hasNext; family.next() )
			{
				node = family.currentNode;
				display = node.display;
				displayObject = display.displayObject;
				position = node.position;
				
				if( !displayObject.parent )
				{
					//	trace("contextView.numChildren = " + contextView.numChildren);
					//	trace("contextView.addChild ( " + displayObject + " )");
//					displayObject.alpha = 0;
					
					// Game Scene children - 1 Info Panel
					if(displayObject is HeatView || displayObject is DefaultPooView){
						contextView.addChildAt( displayObject , contextView.numChildren - GameScene.CHILDREN);
					}
					
				}
				
				displayObject.x = position.point.x;
				displayObject.y = position.point.y;
				
				
				//TODO:
				if(displayObject is HeatView){
					
					/** Set Heat Control */
					if(!stageState.showHeat){
						(displayObject as HeatView).kill();
						family.remove(node.entityID);
					}else{
						(displayObject as HeatView).heat(position.temperature);
					}
					
					
				}else if(displayObject is IBacteriaView){
					
					/** Set Bacteria */
					if(position.isDead){
						
						
						_channel = _bd.play();
						
						
						(displayObject as IBacteriaView).kill();
						family.remove(node.entityID);
						
						stageState.numBacteria--;
						
						stageState.numberOfBacteria.dispatch("default", stageState.numBacteria);
												
					}else{
						
						displayObject.rotation = position.rotation;
						
					}
					
				}else if(displayObject is TapView){
					
					/** Set Tap */
					displayObject.width = position.collisionRadius*2 + 0.1;
					displayObject.height = position.collisionRadius*2 + 0.1;
					
				}else if(displayObject is DefaultPooView){
					
					/** Set Poo */
					if(position.isDead){
						
						(displayObject as DefaultPooView).kill();
						family.remove(node.entityID);
												
					}else{
						
						if(position.numberOfGems != 0){
//							(displayObject as IPooView).progress(position.numberOfGems, (position.startTime - position.reactionTime) / position.startTime);
							(displayObject as IPooView).progress(position.numberOfGems, position.reactionTime / position.startTime);
						}
						
						(displayObject as DefaultPooView).bacteriaCollected(position.numBacteria, position.numBacteria > position.minBacteria);
						(displayObject as DefaultPooView).ring(position.collisionRadius);
						(displayObject as DefaultPooView).rotate(position.rotation);
						
						
					}
					
				}else if(displayObject is StickyPoo){
					
					if(position.numBacteria > 0){
						
//						trace("position.numBacteria = " + position.numBacteria);
							
						(displayObject as StickyPoo).shake(true);
						
					}else{
						
						(displayObject as StickyPoo).shake(false);
						
					}
					
				}
				
			}
			
		}
		
		public function dispose() : void
		{
			tick.remove( render );
		}
	}
}