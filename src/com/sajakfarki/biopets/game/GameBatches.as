package com.sajakfarki.biopets.game
{
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	
	import flash.utils.Dictionary;

	public class GameBatches
	{
		//private var _batches:Vector.<Sprite2DBatch> = new Vector.<Sprite2DBatch>();
		
		private var _dictionary:Dictionary = new Dictionary();
		
		public function GameBatches(){}
		
		public function get batch():Dictionary
		{
			return _dictionary;
		}
		
		public function addBatch(sb:Sprite2DBatch, key:String):void
		{
			_dictionary[key] = sb;
		}
		
	}
}