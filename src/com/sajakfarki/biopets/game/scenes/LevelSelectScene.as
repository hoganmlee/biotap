package com.sajakfarki.biopets.game.scenes 
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.GameState;
	import com.sajakfarki.biopets.game.scenes.buttons.EmptyButton;
	import com.sajakfarki.biopets.graphics.text.NumView;
	import com.sajakfarki.biopets.graphics.text.NumberView;
	import com.sajakfarki.biopets.graphics.types.TextTypes;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Quad2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display3D.textures.Texture;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.events.TouchEvent;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import org.osflash.signals.Signal;
	
	public class LevelSelectScene extends Scene2D
	{	
		public static const LEVEL_ONE:String = "level1";
		public static const LEVEL_TWO:String = "level2";
		public static const LEVEL_THREE:String = "level3";
		public static const LEVEL_FOUR:String = "level4";
		public static const LEVEL_FIVE:String = "level5";
		public static const ACHIEVEMENTS:String = "achieve";
		
		public static var FIRST_TIME:Boolean = true;
		
//		public var doneShow:Signal = new Signal();
		
		public var doneHide:Signal = new Signal();
		public var levelSelected:Signal = new Signal(String);
		
		
		private var _screenHeight:Number;
		private var _screenWidth:Number;
		
		
		/** Map selects */
		[Embed(source = "../media/level_select/map_1.jpg")]
		private var _level1Select:Class;
		
		/** Map selects */
		[Embed(source = "../media/level_select/map_2.jpg")]
		private var _level2Select:Class;
		
		/** Map selects */
		[Embed(source = "../media/level_select/map_3.jpg")]
		private var _level3Select:Class;
		
		/** Map selects */
		[Embed(source = "../media/level_select/map_4.jpg")]
		private var _level4Select:Class;
		
		/** Map selects */
		[Embed(source = "../media/level_select/map_5.jpg")]
		private var _level5Select:Class;
				
		/** Map selects */
		[Embed(source = "../media/level_select/map_allUnlocked.jpg")]
		private var _gameFinished:Class;

		
		/** Map Help Panel */
		[Embed(source = "../media/level_select/map_help1.png")]
		private var _helpScreen1:Class;
		private var _helpScreenSprite1:Sprite2D;
				
		/** Map Help Panel */
		[Embed(source = "../media/level_select/map_help2.png")]
		private var _helpScreen2:Class;
		private var _helpScreenSprite2:Sprite2D;
		
		/** Map Help Panel */
		[Embed(source = "../media/level_select/map_help3.png")]
		private var _helpScreen3:Class;
		private var _helpScreenSprite3:Sprite2D;
		
		
		/** Map Help Panel Button*/
		[Embed(source = "../media/level_select/map_help_button.png")]
		private var _helpTexture : Class;
		private var _helpSprite : Sprite2D;
		private var _helpButton : EmptyButton;
				
		
		private var _helpContinueButton:EmptyButton;
		private var _disableScreen:Quad2D;
		
		
		private var _bg : Sprite2D;
		
		
		/** Title LevelSelect */
		[Embed(source = "../media/level_select/select_level.png")]
		private var _titleLevelSelect:Class;
		private var _title : Sprite2D;
		
		/** BTU LevelSelect */
		[Embed(source = "../media/level_select/box.png")]
		private var _btuLevelSelect:Class;
		private var _btu : Sprite2D;
		
		[Embed(source = "../media/level_select/achieve_1.png")]
		private var _achieveOnClass:Class;
		
		[Embed(source = "../media/level_select/achieve_0.png")]
		private var _achieveOffClass:Class;
		
		
		
		private var _select1:EmptyButton;
		private var _select2:EmptyButton;
		private var _select3:EmptyButton;
		private var _select4:EmptyButton;
		private var _select5:EmptyButton;
		
		private var _achievementBTN:EmptyButton;
		
		
//		[Embed( "../media/text/whiteText.png" )]
//		private var _textAtlasBitmap:Class;
//		[Embed( "../media/text/whiteText.plist", mimeType = "application/octet-stream" )]
//		private var _textAtlasXML:Class;
//		private var _textSpriteBatch:Sprite2DBatch;
		
		
		private var _btuView:NumView;	
		
		
		
		public function LevelSelectScene(width:Number, height:Number)
		{
			super();
			
			_screenHeight = height;
			_screenWidth = width;
			
			var bgt:Texture2D 
			
			/** background LevelSelect */
			switch(GameState.levelsOpen){
				
				case 1:

					bgt = Texture2D.textureFromBitmapData(Bitmap(new _level1Select()).bitmapData);
					break;
				
				case 2:
					
					bgt = Texture2D.textureFromBitmapData(Bitmap(new _level2Select()).bitmapData);
					break;
				
				case 3:
					
					bgt = Texture2D.textureFromBitmapData(Bitmap(new _level3Select()).bitmapData);
					break;
					
				case 4:
					
					bgt = Texture2D.textureFromBitmapData(Bitmap(new _level4Select()).bitmapData);
					break;
					
				default:
					
					if(GameState.globalBTUs < GamePlay.LEVEL_5_PASS){
						
						bgt = Texture2D.textureFromBitmapData(Bitmap(new _level5Select()).bitmapData);
						
					}else{
						
						bgt = Texture2D.textureFromBitmapData(Bitmap(new _gameFinished()).bitmapData);
						
					}
					
					break;
					
			}
						
			_bg = new Sprite2D(bgt);
			_bg.alpha = 0;
			_bg.x = width/2;
			_bg.y = height/2;
			
			addChild(_bg);
			
			/** title Texture */
			var titleT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _titleLevelSelect()).bitmapData);
			_title = new Sprite2D(titleT);
			_title.x = width/2;
			_title.y = -_title.height/2;
			
			addChild(_title);
			
			/** btu Texture */
			var btuT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _btuLevelSelect()).bitmapData);
			_btu = new Sprite2D(btuT);
			_btu.x = width/2;
			_btu.y = height + _btu.height/2;
			
			addChild(_btu);
			
			
			switch(GameState.levelsOpen){
				
				case 5:
					_select5 = new EmptyButton(LEVEL_FIVE, 200, 330);
					_select5.x = 880;
					_select5.y = 177;
					_select5.click.add(dispatchLevel);
					
					addChild(_select5);
					
					if(GameState.globalBTUs < GamePlay.LEVEL_5_PASS) break;
										
				case 4:
					_select4 = new EmptyButton(LEVEL_FOUR, 200, 200);
					_select4.x = 810;
					_select4.y = 470;
					_select4.click.add(dispatchLevel);
					
					addChild(_select4);
					
					if(GameState.globalBTUs < GamePlay.LEVEL_5_PASS) break;
					
				case 3:
					_select3 = new EmptyButton(LEVEL_THREE, 200, 200);
					_select3.x = 560;
					_select3.y = 210;
					_select3.click.add(dispatchLevel);
					
					addChild(_select3);
					
					if(GameState.globalBTUs < GamePlay.LEVEL_5_PASS) break;
					
				case 2:
					
					_select2 = new EmptyButton(LEVEL_TWO, 200, 200);
					_select2.x = 425;
					_select2.y = 490;
					_select2.click.add(dispatchLevel);
					
					addChild(_select2);
					
					if(GameState.globalBTUs < GamePlay.LEVEL_5_PASS) break;
					
				case 1:
					
					_select1 = new EmptyButton(LEVEL_ONE, 200, 200);
					_select1.x = 148;
					_select1.y = 312;
					_select1.click.add(dispatchLevel);
					
					addChild(_select1);
					
					if(GameState.globalBTUs < GamePlay.LEVEL_5_PASS) break;
			}
			
			
			if(GameState.globalBTUs < GamePlay.LEVEL_1_PASS){
				
				createLocked(1);
				
			}else if(GameState.globalBTUs < GamePlay.LEVEL_2_PASS){
				
				createLocked(2);
				createCards(1);
				
			}else if(GameState.globalBTUs < GamePlay.LEVEL_3_PASS){
				
				createCards(2);
				createLocked(3);
				
			}else if(GameState.globalBTUs < GamePlay.LEVEL_4_PASS){
				
				createCards(3);
				createLocked(4);
				
			}else if(GameState.globalBTUs < GamePlay.LEVEL_5_PASS){
				
				createCards(4);
				createLocked(5);
				
			}else{
				
				createCards(5);
				
			}
		
			_achievementBTN = new EmptyButton(ACHIEVEMENTS, 300, 100);
			_achievementBTN.x = _screenWidth/2 - 150;
			_achievementBTN.y = _screenHeight - 50;
			_achievementBTN.click.add(dispatchLevel);
			
			addChild(_achievementBTN);
			
			
			
			//TODO: BTUS
//			/** Brown Text Sprite Batch **/
//			var textTex:Texture2D = Texture2D.textureFromBitmapData(new _textAtlasBitmap().bitmapData);
//			var textAtlas:TextureAtlas = new TextureAtlas(textTex.bitmapWidth, textTex.bitmapHeight, new XML(new _textAtlasXML()), TextureAtlas.XML_FORMAT_COCOS2D, 60, false);
//			
//			_textSpriteBatch = new Sprite2DBatch(textTex);
//			_textSpriteBatch.setSpriteSheet(textAtlas);
//			addChild(_textSpriteBatch);
			
			var helpT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _helpTexture()).bitmapData);
			_helpSprite = new Sprite2D(helpT);
			_helpSprite.x = 978;
			_helpSprite.y = 717;
			_helpSprite.alpha = 0;
			addChild(_helpSprite);
			
			_helpButton = new EmptyButton("helpme", 200, 200);
			_helpButton.x = 978;
			_helpButton.y = 717;
			_helpButton.click.add(helpDispatch);
			addChild(_helpButton);
			
			_disableScreen = new Quad2D(1024, 768);
			_disableScreen.bottomLeftColor = 
			_disableScreen.bottomRightColor = 
			_disableScreen.topLeftColor = 
			_disableScreen.topRightColor =  0x99000000;
			_disableScreen.x = _screenWidth/2;
			_disableScreen.y = _screenHeight/2;
			_disableScreen.alpha = 0;
			addChild(_disableScreen);

			
			var h1T:Texture2D =  Texture2D.textureFromBitmapData(Bitmap(new _helpScreen1()).bitmapData);
			_helpScreenSprite1 = new Sprite2D(h1T);
			_helpScreenSprite1.alpha = 0;
			_helpScreenSprite1.x = width/2;
			_helpScreenSprite1.y = height/2;
			addChild(_helpScreenSprite1);
			
			var h2T:Texture2D =  Texture2D.textureFromBitmapData(Bitmap(new _helpScreen2()).bitmapData);
			_helpScreenSprite2 = new Sprite2D(h2T);
			_helpScreenSprite2.alpha = 0;
			_helpScreenSprite2.x = width/2;
			_helpScreenSprite2.y = height/2;
			addChild(_helpScreenSprite2);
			
			var h3T:Texture2D =  Texture2D.textureFromBitmapData(Bitmap(new _helpScreen3()).bitmapData);
			_helpScreenSprite3 = new Sprite2D(h3T);
			_helpScreenSprite3.alpha = 0;
			_helpScreenSprite3.x = width/2;
			_helpScreenSprite3.y = height/2;
			addChild(_helpScreenSprite3);
			
			
			_helpContinueButton = new EmptyButton("helpContinue", 200, 200);
			addChild(_helpContinueButton);
			
			
			_btuView = new NumView(10);
			_btuView.scaleX = 0.9;
			_btuView.scaleY = 0.9;
			_btuView.setNum(GameState.globalBTUs);
			_btuView.x = 35;
			_btuView.y =  10;
			_btu.addChild(_btuView);
			
			//TODO: Help button
			//TODO: disable map Quad
			//TODO: Help/Continue panels
			
			
			//TweenLite.to(_disableScreen, 0.5, {delay:5, alpha:0.5});
			
			
		}
		
		private function helpDispatch(s:String):void
		{
			TweenMax.killDelayedCallsTo(showFirstTime);
			
			_helpContinueButton.show();
			_helpContinueButton.x = 434;
			_helpContinueButton.y = 289;
			TweenLite.to(_disableScreen, 0.6, {alpha:0.2});
			TweenLite.to(_helpScreenSprite1, 0.6, {delay:0.6, alpha:1});
			_helpContinueButton.click.add(help2);
		}
		
		private function help2(s:String):void
		{
			_helpContinueButton.click.remove(help2);
			
			TweenLite.to(_helpScreenSprite1, 0.6, {alpha:0});
			TweenLite.to(_helpScreenSprite2, 0.6, {delay:0.6, alpha:1});
			
			_helpContinueButton.show();
			_helpContinueButton.x = 785;
			_helpContinueButton.y = 555;
			_helpContinueButton.click.add(help3);
			
			setChildIndex(_btu, this.numChildren-1);
		}
		
		private function help3(s:String):void
		{
			_helpContinueButton.click.remove(help3);
			TweenLite.to(_helpScreenSprite2, 0.6, {alpha:0});
			TweenLite.to(_helpScreenSprite3, 0.6, {delay:0.6, alpha:1});
			
			_helpContinueButton.show();
			_helpContinueButton.x = 543;
			_helpContinueButton.y = 555;
			_helpContinueButton.click.add(help4);
			
		}
		
		private function help4(s:String):void
		{
			_helpContinueButton.click.remove(help4);
			TweenLite.to(_helpScreenSprite3, 0.6, {alpha:0});
			_helpContinueButton.hide();
			
			_helpButton.show();
			TweenLite.to(_disableScreen, 0.6, {alpha:0}); 
		}
		
		private function createLocked(position:int):void
		{
			switch(position){
				
				case 1:
					var close5t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOffClass()).bitmapData);
					var close5:Sprite2D = new Sprite2D(close5t);
					close5.x = -160;
					close5.y = 5;
					
					_btu.addChild(close5);
					
				case 2:
					var close4t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOffClass()).bitmapData);
					var close4:Sprite2D = new Sprite2D(close4t);
					close4.x = -135;
					close4.y = 5;
					
					_btu.addChild(close4);
				case 3:
					var close3t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOffClass()).bitmapData);
					var close3:Sprite2D = new Sprite2D(close3t);
					close3.x = -110;
					close3.y = 5;
					
					_btu.addChild(close3);
					
				case 4:
					var close2t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOffClass()).bitmapData);
					var close2:Sprite2D = new Sprite2D(close2t);
					close2.x = -85;
					close2.y = 5;
					
					_btu.addChild(close2);
					
				case 5:
					
					var close1t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOffClass()).bitmapData);
					var close1:Sprite2D = new Sprite2D(close1t);
					close1.x = -60;
					close1.y = 5;
					
					_btu.addChild(close1);
				
			}
		}
		
		private function createCards(position:int):void
		{
			switch(position){
				
				case 5:
					
					var open1t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOnClass()).bitmapData);
					var open1:Sprite2D = new Sprite2D(open1t);
					open1.x = -60;
					open1.y = 5;
					
					_btu.addChild(open1);
					
				case 4:
					
					var open2t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOnClass()).bitmapData);
					var open2:Sprite2D = new Sprite2D(open2t);
					open2.x = -85;
					open2.y = 5;
					
					_btu.addChild(open2);
				
				case 3:
					
					var open3t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOnClass()).bitmapData);
					var open3:Sprite2D = new Sprite2D(open3t);
					open3.x = -110;
					open3.y = 5;
					
					_btu.addChild(open3);
						
				case 2:
							
					var open4t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOnClass()).bitmapData);
					var open4:Sprite2D = new Sprite2D(open4t);
					open4.x = -135;
					open4.y = 5;
					
					_btu.addChild(open4);
					
				case 1:
					
					var open5t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _achieveOnClass()).bitmapData);
					var open5:Sprite2D = new Sprite2D(open5t);
					open5.x = -160;
					open5.y = 5;
					
					_btu.addChild(open5);
				
			}
		}
		
		// *****************************************************************************************************************
		// * Protected																									   *
		// *****************************************************************************************************************
		override protected function step(elapsed:Number):void
		{
			super.step(elapsed);
		}
				
		//
		public function show():void
		{
			TweenMax.to(_bg, 1, {alpha:1});
			TweenMax.to(_title, 0.6, {delay:0.8, y:_title.height/2, ease:Quint.easeOut});
			TweenMax.to(_btu, 0.6, {delay:1, y:_screenHeight -_btu.height/2 + 20, ease:Quint.easeOut});
			TweenMax.to(_helpSprite, 0.6, {delay:1, alpha:1});
			
			_helpButton.show();
			
//			TweenMax.to(_textSpriteBatch, 1, {delay:1, alpha:1});
//			TweenMax.delayedCall(1.6, _btuView.show);
			_achievementBTN.show();
			
			switch(GameState.levelsOpen){
				
				case 5:
					_select5.show();
				case 4:
					_select4.show();
				case 3:
					_select3.show();
				case 2:
					_select2.show();
				case 1:
					_select1.show();
					break;
				
			}
			
			if(FIRST_TIME){
				TweenMax.delayedCall(1, showFirstTime);
				FIRST_TIME = false;
			}
			
		}
		
		private function showFirstTime():void
		{
			helpDispatch("helpme");
		}
		
		private function dispatchLevel(s:String):void
		{
			levelSelected.dispatch(s);
			hide();
		}
		
		private function hide():void
		{
			TweenMax.killDelayedCallsTo(showFirstTime);
			
			_helpButton.hide();
			
			TweenMax.to(_helpSprite, 0.7, {alpha:0});
						
			TweenMax.killTweensOf(_bg);
			TweenMax.killTweensOf(_title);
			TweenMax.killTweensOf(_btu);
//			TweenMax.killTweensOf(_textSpriteBatch);
			
			TweenMax.to(_title, 0.7, {y:-_title.height/2, ease:Quint.easeIn});
			TweenMax.to(_btu, 0.7, {y:_screenHeight +_btu.height/2, ease:Quint.easeIn});
			TweenMax.to(_bg, 1, {delay:0.5, alpha:0, onComplete:dispatchDone})
//			TweenMax.to(_textSpriteBatch, 1, {delay:0.5, alpha:0})
			
		}
		
		private function dispatchDone():void
		{
			doneHide.dispatch();
		}
		
	}
	
}