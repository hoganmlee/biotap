package com.sajakfarki.biopets.game.scenes
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Linear;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.game.GameBatches;
	import com.sajakfarki.biopets.game.scenes.buttons.HelpButton;
	import com.sajakfarki.biopets.game.scenes.buttons.PauseResumeButton;
	import com.sajakfarki.biopets.game.scenes.buttons.QuitButton;
	import com.sajakfarki.biopets.game.scenes.buttons.RetryButton;
	import com.sajakfarki.biopets.game.scenes.buttons.StartButton;
	import com.sajakfarki.biopets.game.scenes.panels.DefaultPanel;
	import com.sajakfarki.biopets.game.scenes.panels.HelpPanel;
	import com.sajakfarki.biopets.game.scenes.panels.LevelCompletePanel;
	import com.sajakfarki.biopets.game.scenes.panels.StageCompletePanel;
	import com.sajakfarki.biopets.graphics.compounds.base.DefaultPooView;
	import com.sajakfarki.biopets.graphics.compounds.base.IPooView;
	import com.sajakfarki.biopets.graphics.compounds.base.StickyPoo;
	import com.sajakfarki.biopets.graphics.text.*;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.events.TouchEvent;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	
	import org.osflash.signals.Signal;
	
	public class GameScene extends Scene2D
	{
		public static var CHILDREN : int = 7;		//Number to addChild in RenderSystem underneath
		
		public static var BUTTON_Y_OFFSET : int = 170;
		
		
		public static var GAME_BATCH:String = "game";
		public static var BACTERIA_BATCH:String = "bacteria";
		
		
		public static var END_SCREEN_BATCH:String = "endScreen";
		 
		
		//public var complete : Signal = new Signal();
		
		public var doneHide : Signal = new Signal();
		public var doneShow : Signal = new Signal();
		public var paused : Signal = new Signal(Boolean);
		public var help : Signal = new Signal();
		public var start : Signal = new Signal();
		public var retry : Signal = new Signal();
		public var quit : Signal = new Signal();
		//public var added:Signal = new Signal();
		
		private var _screenWidth : Number;
		private var _screenHeight : Number;
		
		private var _helpPanel : HelpPanel;
		
		private var _pauseResumeButton : PauseResumeButton;
		private var _startButton : StartButton;
		private var _helpButton : HelpButton;
		private var _quitButton : QuitButton;
		private var _retryButton : RetryButton;
		
		private var _isStarted : Boolean = false;
		private var _isHelp : Boolean = false;
		
		private var _delay:Number;
		
		
		[Embed( "../media/gameSpriteSheet.png" )]
		private var textureAtlasBitmap:Class;
		[Embed( "../media/gameSpriteSheet.plist", mimeType = "application/octet-stream" )]
		private var textureAtlasXML:Class;
		
		
		
		private var _gameSpriteBatch:Sprite2DBatch;
		
		
		
		private var _gameBatches:GameBatches;
		
		
		
		public function GameScene(width:Number, height:Number)
		{
			super();
			
			
			/** Game Sprite Batch **/
			var atlasTex:Texture2D = Texture2D.textureFromBitmapData(new textureAtlasBitmap().bitmapData);
			var atlas:TextureAtlas = new TextureAtlas(atlasTex.bitmapWidth, atlasTex.bitmapHeight, new XML(new textureAtlasXML()), TextureAtlas.XML_FORMAT_COCOS2D, 0, false);
			
			_gameSpriteBatch = new Sprite2DBatch(atlasTex);
			_gameSpriteBatch.setSpriteSheet(atlas);
			
			addChild(_gameSpriteBatch);
			
			/** GameBatches Dictionary **/
			_gameBatches = new GameBatches();
			_gameBatches.addBatch(_gameSpriteBatch, GAME_BATCH);
			
						
			_screenWidth = width;
			_screenHeight = height;
			
			/** pause/resume button */
			_pauseResumeButton = new PauseResumeButton();
			_pauseResumeButton.pause.add(dispatchPause);
			_pauseResumeButton.x = 40;
			_pauseResumeButton.y = 34;
			addChild(_pauseResumeButton);
			
			/** help panel */
			_helpPanel = new HelpPanel();
			_helpPanel.back.add(hideHelpShowPanel);
			_helpPanel.x = _screenWidth/2;
			_helpPanel.y = -300;
			addChild(_helpPanel);
			
			/** help button */
			_helpButton = new HelpButton();
			_helpButton.help.add(showHelp);
			_helpButton.x = _screenWidth/2 + 285;
			_helpButton.y = _screenHeight/2 + BUTTON_Y_OFFSET + DefaultPanel.PANEL_Y_OFFSET;
			addChild(_helpButton);
			
			/** start button */
			_startButton = new StartButton();
			_startButton.start.add(startGame);
			_startButton.x = _screenWidth/2 - 100;
			_startButton.y = _screenHeight/2 + BUTTON_Y_OFFSET + DefaultPanel.PANEL_Y_OFFSET;
			
			addChild(_startButton);
			
			/** quit */
			_quitButton = new QuitButton();
			_quitButton.quit.add(quitGame);
			_quitButton.x = _screenWidth/2 - 284;
			_quitButton.y = _screenHeight/2 + BUTTON_Y_OFFSET + DefaultPanel.PANEL_Y_OFFSET;
			
			addChild(_quitButton);

			/** retry */
			_retryButton = new RetryButton();
			_retryButton.retry.add(retryGame);
			_retryButton.x = _screenWidth/2 - 196;
			_retryButton.y = _screenHeight/2 + BUTTON_Y_OFFSET + DefaultPanel.PANEL_Y_OFFSET;
			
			addChild(_retryButton);

		}
		
		public function get gameBatches():GameBatches
		{
			return _gameBatches;
		}
		
		public function get isStarted():Boolean
		{
			return _isStarted;
		}
		
		public function hidePause():void
		{
			_pauseResumeButton.hide();
		}
		
		/**************************************************
		 * 
		 * 	Dispatch Signals
		 * 
		 **************************************************/
		private function retryGame():void
		{
			_pauseResumeButton.hide();
			hideButtons();
			retry.dispatch();
		}
		
		private function quitGame():void
		{
			_pauseResumeButton.hide();
			hideButtons();
			quit.dispatch();
		}
		
		/**************************************************
		 * 
		 * 	Hide Help Panel, Show Level Panel
		 * 
		 **************************************************/
		private function hideHelpShowPanel():void
		{
			hideHelp();
			TweenMax.delayedCall(0.2, dispatchPause, [true]);
		}	
		
		
		/**************************************************
		 * 
		 * 	Show / Hide buttons based on State
		 * 
		 **************************************************/
		public function showButtons():void
		{
			if(_isStarted){
				
				//retry 
				_retryButton.show();
				//quit
				_quitButton.show();
				
			}else{
				
				_startButton.show();
				
			}
			
			_helpButton.show();
			
		}
		
		public function hideButtons():void
		{
			
			if(_isStarted){
					
				//retry 
				_retryButton.hide();
				//quit
				_quitButton.hide();
															
			}else{
					
				_startButton.hide();
					
			}
				
			_helpButton.hide();
			
		}
		
		/**************************************************
		 * 
		 * 	Show / Hide Help
		 * 
		 **************************************************/
		private function showHelp():void
		{
			hideButtons();
			
			_isHelp = true;
			
			//closes _level info panel
			TweenMax.delayedCall(0.4, help.dispatch);
			
			//show help panel
			TweenMax.to(_helpPanel, 1 , {delay:0.5, y:_screenHeight/2, ease:Quint.easeInOut, onComplete:_helpPanel.enable});
						
		}
		
		private function hideHelp():void
		{
			_isHelp = false;
			
			TweenMax.to(_helpPanel, 1 , {y:-300, ease:Quint.easeInOut});
		}
		
		/**************************************************
		 * 
		 * 	Start Game
		 * 
		 **************************************************/
		private function startGame():void
		{
			_isStarted = true;
			
			start.dispatch();
			
			_helpButton.hide();
			
			_pauseResumeButton.show();
			
		}
		
		
		/**************************************************
		 * 
		 * 	Dispatch Pause
		 * 
		 **************************************************/
		private function dispatchPause(value:Boolean):void
		{
			TweenMax.killDelayedCallsTo(showButtons);
			
			paused.dispatch(value);
			
			if(!value){
				
				if(_isHelp){
					hideHelp();
				}else{
					hideButtons();
				}
				
			}else{
				
				TweenMax.delayedCall(1.1, showButtons);
				
			}
			
		}
				
		// *****************************************************************************************************************
		// * Protected																									   *
		// *****************************************************************************************************************
		override protected function step(elapsed:Number):void
		{
			super.step(elapsed);
		}
		
		
		/************************************************************************
		 * 
		 *   Fade Out Level
		 * 
		 ************************************************************************/
		public function hide():void
		{
			_isStarted = false;
			
			var delay:Number = 0.1;
						
			for (var i:int = this.numChildren-1; i >= 0; i--) 
			{
				/************************************************************************
				 * 
				 *   Cleanup everything but the GameScene assets
				 * 
				 ************************************************************************/
								
				TweenMax.to(getChildAt(i), 0.6, {delay:delay, alpha:0, onComplete:removeChild, onCompleteParams:[getChildAt(i)]});
				
				trace("getChildAt(i) = " + getChildAt(i));
				
				if(getChildAt(i) is IPooView){
					
					(getChildAt(i) as IPooView).stopSound();
					
				}
				delay += 0.05;
				
			}
			
			
			TweenMax.delayedCall(delay+1, hideDone);
			
			trace(">>>>> Clean up GameScene");
			
		}
		
		private function hideDone():void
		{
			//_kill = true;
			doneHide.dispatch();
		}
		
		/************************************************************************
		 * 
		 *    Show level after Initialization is done
		 * 
		 ************************************************************************/
		public function show():void
		{			
//			var delay:Number = 0.1;
//			
//			for (var i:int = 0; i < this.numChildren-CHILDREN; i++) 
//			{
//					TweenMax.to(getChildAt(i), 0.7, {delay:delay, alpha:1});
//					delay += 0.05;
//			}
//			
//			TweenMax.delayedCall(delay+1, showDone);
			
			
			
			
			showDone();
			
			
		}
		
		private function showDone():void
		{
			_pauseResumeButton.reset();
			_helpPanel.reset();
			
			showButtons();
			doneShow.dispatch();
		}
		
//		public function killTweens():void
//		{
//			TweenMax.killAll(true,true,true);
//		}
		
	}
	
}