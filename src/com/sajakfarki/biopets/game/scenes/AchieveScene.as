package com.sajakfarki.biopets.game.scenes
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.Cards;
	import com.sajakfarki.biopets.components.GameState;
	import com.sajakfarki.biopets.game.scenes.panels.AchievePanel;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.Signal;
	
	public class AchieveScene extends Scene2D
	{
		public static var PADDING:int = 200;
		public static var BOTTOM_PADDING:int = 50;
		public static var Y_OFFSET:int = 30;
		public static var NUM_CARDS:int = 4;
		
		public var doneHide:Signal = new Signal(); 
		
		/** arrow */
		[Embed(source = "../media/buttons/button_arrow.png")]
		private var _buttonTexture:Class;
		
		
		private var _leftArrow:Sprite2D;
		private var _rightArrow:Sprite2D;
		
		/** back */
		[Embed(source = "../media/buttons/button_exit.png")]
		private var _exitTexture:Class;
		private var _exit:Sprite2D;
		
		/** background */
		[Embed(source = "../media/achievements/background.jpg")]
		private var _bgTexture:Class;
		
		
		/** Card 1 */
		[Embed(source = "../media/achievements/manuelle_back.png")]
		private var _card1back:Class;
		
//		[Embed(source = "../media/achievements/manuelle_front.png")]
		private var _card1front:Class = Cards.CARD1;
		
		[Embed(source = "../media/achievements/manuelle_locked.png")]
		private var _card1locked:Class;
		
		/** Card 2 */
		[Embed(source = "../media/achievements/hyde_back.png")]
		private var _card2back:Class;
		
//		[Embed(source = "../media/achievements/hyde_front.png")]
		private var _card2front:Class = Cards.CARD2;
		
		[Embed(source = "../media/achievements/hyde_locked.png")]
		private var _card2locked:Class;
		
		/** Card 3 */
		[Embed(source = "../media/achievements/fergus_back.png")]
		private var _card3back:Class;
		
//		[Embed(source = "../media/achievements/fergus_front.png")]
		private var _card3front:Class = Cards.CARD3;
		
		[Embed(source = "../media/achievements/fergus_locked.png")]
		private var _card3locked:Class;
		
		/** Card 4 */
		[Embed(source = "../media/achievements/aceto_back.png")]
		private var _card4back:Class;
		
//		[Embed(source = "../media/achievements/aceto_front.png")]
		private var _card4front:Class = Cards.CARD4;
		
		[Embed(source = "../media/achievements/aceto_locked.png")]
		private var _card4locked:Class;
		
		
		/** Card 5 */
		[Embed(source = "../media/achievements/methilda_back.png")]
		private var _card5back:Class;
		
//		[Embed(source = "../media/achievements/methilda_front.png")]
		private var _card5front:Class = Cards.CARD5;
		
		[Embed(source = "../media/achievements/methilda_locked.png")]
		private var _card5locked:Class;
		
		
		
		
		private var _holder:Node2D;
		
				
		
		private var _screenHeight:Number;
		private var _screenWidth:Number;
		
		
		private var _currentPosition:int = 0;
		private var _cardDistance:Number = 600;
		
		
		public function AchieveScene(width:Number, height:Number)
		{
			super();
			
			_screenHeight = height;
			_screenWidth = width;
			
			_holder = new Node2D();
			_holder.alpha = 0;
			addChild(_holder);
			
			
			/** bg Texture */
			for (var i:int = 0; i < 8; i++) 
			{
				
				var bgT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgTexture()).bitmapData);
				var bg:Sprite2D = new Sprite2D(bgT);
				bg.x = i * bg.width - (i*1);
				bg.y = bg.height/2;
				
				_holder.addChild(bg);
				
			}
			
			if(GameState.globalBTUs < GamePlay.LEVEL_1_PASS){
				
				createLocked(1);
				
			}else if(GameState.globalBTUs < GamePlay.LEVEL_2_PASS){
				
				createLocked(2);
				createCards(1);
				
			}else if(GameState.globalBTUs < GamePlay.LEVEL_3_PASS){
				
				createCards(2);
				createLocked(3);
				
			}else if(GameState.globalBTUs < GamePlay.LEVEL_4_PASS){
				
				createCards(3);
				createLocked(4);
				
			}else if(GameState.globalBTUs < GamePlay.LEVEL_5_PASS){
				
				createCards(4);
				createLocked(5);
				
			}else{
				
				createCards(5);
				
			}
			
			var arrowT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _buttonTexture()).bitmapData);
			
			_leftArrow = new Sprite2D(arrowT);
			_leftArrow.alpha = 0;
			_leftArrow.scaleX = 0.1;
			_leftArrow.scaleY = 0.1;
			_leftArrow.x = PADDING;
			_leftArrow.y = _screenHeight/2;
			addChild(_leftArrow);
			
			_rightArrow = new Sprite2D(arrowT);
			_rightArrow.alpha = 0;
			_rightArrow.scaleX = 0.1;
			_rightArrow.scaleY = 0.1;
			_rightArrow.x = _screenWidth - PADDING;
			_rightArrow.y = _screenHeight/2;
			_rightArrow.rotation = 180;
			addChild(_rightArrow);
						
			var exitT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _exitTexture()).bitmapData);
			_exit = new Sprite2D(exitT);
			_exit.alpha = 0;
			_exit.scaleX = 0.1;
			_exit.scaleY = 0.1;
			_exit.x = _screenWidth/2;
			_exit.y = _screenHeight - BOTTOM_PADDING;
			
			addChild(_exit);
			
		}
		
		private function createLocked(position:int):void
		{
			
			switch(position){
				
				case 1:
					var locked1:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card1locked()).bitmapData);
					var card1:Sprite2D = new Sprite2D(locked1);
					card1.x = _screenWidth/2;
					card1.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card1);
					
				case 2:
					var locked2:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card2locked()).bitmapData);
					var card2:Sprite2D = new Sprite2D(locked2);
					card2.x = _screenWidth/2 + _cardDistance;
					card2.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card2);
					
				case 3:
					var locked3:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card3locked()).bitmapData);
					var card3:Sprite2D = new Sprite2D(locked3);
					card3.x = _screenWidth/2 + _cardDistance*2;
					card3.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card3);
					
				case 4:
					var locked4:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card4locked()).bitmapData);
					var card4:Sprite2D = new Sprite2D(locked4);
					card4.x = _screenWidth/2 + _cardDistance*3;
					card4.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card4);
					
				case 5:
					var locked5:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card5locked()).bitmapData);
					var card5:Sprite2D = new Sprite2D(locked5);
					card5.x = _screenWidth/2 + _cardDistance*4;
					card5.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card5);
					
					break;
			}
						
		}
		
		private function createCards(position:int):void
		{
			
			switch(position){
				
				case 5:
					
					var front52:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card5front()).bitmapData);
					var back52:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card5back()).bitmapData);
					
					var card5:AchievePanel = new AchievePanel(front52, back52);
					card5.x = _screenWidth/2 + _cardDistance*4;
					card5.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card5);
					
				case 4:
					
					var front42:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card4front()).bitmapData);
					var back42:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card4back()).bitmapData);
					
					var card4:AchievePanel = new AchievePanel(front42, back42);
					card4.x = _screenWidth/2 + _cardDistance*3;
					card4.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card4);
					
				case 3:
					
					var front32:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card3front()).bitmapData);
					var back32:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card3back()).bitmapData);
					
					var card3:AchievePanel = new AchievePanel(front32, back32);
					card3.x = _screenWidth/2 + _cardDistance*2;
					card3.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card3);
					
				case 2:
					
					var front22:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card2front()).bitmapData);
					var back22:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card2back()).bitmapData);
					
					var card2:AchievePanel = new AchievePanel(front22, back22);
					card2.x = _screenWidth/2 + _cardDistance*1;
					card2.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card2);
					
				case 1:
					
					var front12:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card1front()).bitmapData);
					var back12:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _card1back()).bitmapData);
					
					var card1:AchievePanel = new AchievePanel(front12, back12);
					card1.x = _screenWidth/2;
					card1.y = _screenHeight/2 - Y_OFFSET;
					_holder.addChild(card1);
					
					break;
				
			}
			
			
		}
		
		public function show(cardPosition:int = 0, reveal:Boolean = false):void
		{
			_currentPosition = cardPosition;
			
			
			_holder.x = -_currentPosition * _cardDistance;
			
			
			
			//TODO: reveal animation
			
			
			
			TweenMax.to(_holder, 0.5, {alpha:1, onComplete:tweenToPosition});
			
			TweenMax.to(_exit, 0.6, {delay:1, scaleX:1, scaleY:1, alpha:1, ease:Back.easeOut});
			
			TweenMax.delayedCall(1.6, enable);
			
		}
		
		private function tweenToPosition():void
		{
			TweenMax.to(_holder, 0.7, {x:-_currentPosition*_cardDistance, ease:Quint.easeInOut, onComplete:checkPosition});
		}
		
		private function enable():void
		{
			checkPosition();
			
			_exit.mouseEnabled = true;
			_exit.addEventListener(MouseEvent.CLICK, exitAchieve);
			
		}
		
		private function checkPosition():void
		{
			if(_currentPosition == 0){
				
				hideLeft();
				showRight();
				enableRight();
				
			}else if(_currentPosition == NUM_CARDS){
				
				hideRight();
				showLeft();
				enableLeft();
				
			}else{
				
				showRight();
				enableRight();
				showLeft();
				enableLeft();
				
			}
			
		}
		
		
		protected function moveRight(event:MouseEvent):void
		{
			_currentPosition++;
			
			disableLeft();
			disableRight();
			
			tweenToPosition();
		}
		
		protected function moveLeft(event:MouseEvent):void
		{
			_currentPosition--;
			
			disableLeft();
			disableRight();
			
			tweenToPosition();
			
		}
		
		
		
		private function showRight():void
		{
			TweenMax.to(_rightArrow, 0.3, {alpha:1,scaleX:1, scaleY:1, ease:Back.easeOut});
		}
		private function hideRight():void
		{
			TweenMax.to(_rightArrow, 0.3, {alpha:0,scaleX:0.1, scaleY:0.1, ease:Back.easeIn});
		}
		
		
		
		private function enableRight():void
		{
			_rightArrow.mouseEnabled = true;
			_rightArrow.addEventListener(MouseEvent.CLICK, moveRight);
		}
		private function disableRight():void
		{
			_rightArrow.mouseEnabled = false;
			_rightArrow.removeEventListener(MouseEvent.CLICK, moveRight);
		}
		
		
		
		private function showLeft():void
		{
			TweenMax.to(_leftArrow, 0.3, {alpha:1,scaleX:1, scaleY:1, ease:Back.easeOut});
		}
		private function hideLeft():void
		{
			TweenMax.to(_leftArrow, 0.3, {alpha:0,scaleX:0.1, scaleY:0.1, ease:Back.easeIn});
		}
		
		
		
		private function enableLeft():void
		{
			_leftArrow.mouseEnabled = true;
			_leftArrow.addEventListener(MouseEvent.CLICK, moveLeft);
		}
		
		private function disableLeft():void
		{
			_leftArrow.mouseEnabled = false;
			_leftArrow.removeEventListener(MouseEvent.CLICK, moveLeft);
		}
		
		private function disable():void
		{
			_exit.mouseEnabled = false;
			_exit.removeEventListener(MouseEvent.CLICK, exitAchieve);
		}
		
		protected function exitAchieve(event:MouseEvent):void
		{
			
			hide();
			
		}
		
		private function hide():void
		{
			disable();
			disableLeft();
			disableRight();
			
			TweenMax.killTweensOf(_holder);
			TweenMax.killTweensOf(_leftArrow);
			TweenMax.killTweensOf(_rightArrow);
			TweenMax.killTweensOf(_exit);
			
			TweenMax.to(_exit, 0.5, {scaleX:0.1, scaleY:0.1, alpha:0, ease:Back.easeIn})
				
			hideLeft();
			hideRight()
				
			TweenMax.to(_holder, 0.5, {delay:0.6, alpha:0, onComplete:dispatchDone});
			
		}
		
		private function dispatchDone():void
		{
			doneHide.dispatch();
		}
		
	}
}