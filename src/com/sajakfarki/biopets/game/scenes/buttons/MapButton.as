package com.sajakfarki.biopets.game.scenes.buttons
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.Signal;
	
	public class MapButton extends Sprite2D
	{
		public var map:Signal = new Signal();
		
		/** Pause Button */
		[Embed(source = "../media/buttons/viewmap.png")]
		private var _mapButtonImage:Class;
		
		
		public function MapButton()
		{
			var hbt:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _mapButtonImage()).bitmapData);
			
			super(hbt);
			
			this.alpha = 0;
			
		}
		
		public function show():void
		{
		//	TweenMax.to(this, 0.4, {alpha:1});
			
			this.mouseEnabled = true;
			this.addEventListener(MouseEvent.CLICK, dispatchStart);
		}
		
		public function hide():void
		{
			//TweenMax.to(this, 0.4, {alpha:0});
			
			this.mouseEnabled = false;
			this.removeEventListener(MouseEvent.CLICK, dispatchStart);
			
		}
		
		public function dispatchStart(event:MouseEvent):void
		{
			hide();
			map.dispatch();
		}
		
	}
}