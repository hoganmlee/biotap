package com.sajakfarki.biopets.game.scenes.buttons
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.Signal;
	
	public class PauseResumeButton extends Node2D
	{
		public var pause:Signal = new Signal(Boolean);
				
		/** Pause Button */
		[Embed(source = "../media/buttons/pause.png")]
		private var _pauseButtonImage:Class;
		private var _pauseButton : Sprite2D;
		
		/** Resume Button */
		[Embed(source = "../media/buttons/resume.png")]
		private var _resumeButtonImage:Class;
		private var _resumeButton : Sprite2D;
		
		
		
		public function PauseResumeButton()
		{
			super();
			
			this.alpha = 0;
			
			/** resume */
			var rbt:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _resumeButtonImage()).bitmapData);
			_resumeButton = new Sprite2D(rbt);
			
			addChild(_resumeButton);
			
			/** pause */
			var pbt:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _pauseButtonImage()).bitmapData);
			_pauseButton = new Sprite2D(pbt);
			
			addChild(_pauseButton);
			
			reset();
			
		}
		
		public function set paused(value:Boolean):void
		{
			if(value){
				
				_pauseButton.mouseEnabled = false;
				_pauseButton.removeEventListener(MouseEvent.CLICK, dispatchPause);
				
				TweenMax.to(_pauseButton, 0.4, {alpha:0, scaleX:0.3, scaleY:0.3, ease:Back.easeIn});
				
				_resumeButton.mouseEnabled = true;
				_resumeButton.addEventListener(MouseEvent.CLICK, dispatchResume);
				
				TweenMax.to(_resumeButton, 0.4, {delay:0.4, alpha:1, scaleX:1, scaleY:1, ease:Back.easeOut});
				
				
			}else{
				
				_pauseButton.mouseEnabled = true;
				_pauseButton.addEventListener(MouseEvent.CLICK, dispatchPause);
				
				TweenMax.to(_pauseButton, 0.4, {delay:0.4, alpha:1, scaleX:1, scaleY:1, ease:Back.easeOut});
				
				
				_resumeButton.mouseEnabled = false;
				_resumeButton.removeEventListener(MouseEvent.CLICK, dispatchResume);
				
				TweenMax.to(_resumeButton, 0.4, {alpha:0, scaleX:0.3, scaleY:0.3, ease:Back.easeIn});
								
			}
			
		}
		
		public function reset():void
		{
			_resumeButton.alpha = 0;
			_resumeButton.scaleX = 0.3;
			_resumeButton.scaleY = 0.3;
			
			_pauseButton.alpha = 0;
			_pauseButton.scaleX = 0.3;
			_pauseButton.scaleY = 0.3;
		}
			
		
		private function dispatchResume(event:MouseEvent):void
		{
			this.paused = false;
			pause.dispatch(false);
		}
	
		private function dispatchPause(event:MouseEvent):void
		{
			this.paused = true;
			pause.dispatch(true);
		}
		
		public function show():void
		{
			TweenMax.to(this, 0.7, {alpha:1});
			this.paused = false;
			
		}
				
		public function hide():void
		{
			TweenMax.to(this, 0.7, {alpha:0});
			
			_resumeButton.removeEventListener(MouseEvent.CLICK, dispatchResume);
			_pauseButton.removeEventListener(MouseEvent.CLICK, dispatchPause);
			
		}
		
	}
}