package com.sajakfarki.biopets.game.scenes.buttons
{
	import com.greensock.TweenMax;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.Signal;
	
	public class EmptyButton extends Sprite2D
	{
		public var click:Signal = new Signal(String);
		
		/** Pause Button */
		[Embed(source = "../media/buttons/empty.png")]
		private var _buttonImage:Class;
		
		
		private var _s:String;
		
		public function EmptyButton(s:String, w:Number, h:Number)
		{
			_s = s;
			
			/** back */
			var sbt:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _buttonImage()).bitmapData);
			
			super(sbt);
			
			this.width = w;
			this.height = h;
			
		}
		
		public function show():void
		{
			//TweenMax.to(this, 0.4, {alpha:1});
			this.mouseEnabled = true;
			this.addEventListener(MouseEvent.CLICK, dispatchClick);
			
		}
		
		public function hide():void
		{
			//TweenMax.to(this, 0.4, {alpha:0});
			
			this.mouseEnabled = false;
			this.removeEventListener(MouseEvent.CLICK, dispatchClick);
			
		}
		
		public function dispatchClick(event:MouseEvent):void
		{
			hide();
			click.dispatch(_s);
		}
		
	}
}