package com.sajakfarki.biopets.game.scenes.panels
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Quint;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	public class AchievePanel extends Node2D
	{
		public static var Y_OFFSET:int = 800;
		
		private var _isFront:Boolean = true;
		
		private var _cardFront:Sprite2D;
		private var _cardBack:Sprite2D;
		
		public function AchievePanel(front:Texture2D, back:Texture2D)
		{
			super();
			
			_cardFront = new Sprite2D(front);
			addChild(_cardFront);
			
			
			_cardBack = new Sprite2D(back);
			_cardBack.y = Y_OFFSET;
			addChild(_cardBack);
			
			enableFront();
			
		}
		
		private function enableFront():void
		{
			_cardFront.mouseEnabled = true;
			_cardFront.addEventListener(MouseEvent.CLICK, gotoBack);
		}
		
		private function disableFront():void
		{
			_cardFront.mouseEnabled = false;
			_cardFront.removeEventListener(MouseEvent.CLICK, gotoBack);
		}
		
		private function enableBack():void
		{
			_cardBack.mouseEnabled = true;
			_cardBack.addEventListener(MouseEvent.CLICK, gotoFront);
		}
		
		
		private function disableBack():void
		{
			_cardBack.mouseEnabled = false;
			_cardBack.removeEventListener(MouseEvent.CLICK, gotoFront);
		}
		
		
		protected function gotoFront(event:MouseEvent):void
		{
			enableFront();
			disableBack();
			
			TweenMax.to(_cardFront, 0.7, {y:0, ease:Quint.easeInOut});
			TweenMax.to(_cardBack, 0.7, {y:Y_OFFSET, ease:Quint.easeInOut});
		}
		
		protected function gotoBack(event:MouseEvent):void
		{
			disableFront();
			enableBack();
			
			TweenMax.to(_cardFront, 0.7, {y:-Y_OFFSET, ease:Quint.easeInOut});
			TweenMax.to(_cardBack, 0.7, {y:0, ease:Quint.easeInOut});
		}
		
	}
}