package com.sajakfarki.biopets.game.scenes.panels
{
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	
	public class StageCompletePanel extends Sprite2D
	{
		[Embed(source = "../media/level_complete/panel_stage_complete.png")]
		private var _panelImage:Class;
		
		
		public function StageCompletePanel()
		{
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _panelImage()).bitmapData);
			
			super(p);
		}
		
	}
	
}