package com.sajakfarki.biopets.game.scenes.panels
{
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;

	public class Level51_Panel extends DefaultPanel implements IPanel
	{
		
		/** Background texture */
		[Embed(source = "../media/level_start/5-1.png")]
		private var _bgTexture:Class;
		
		
		public function Level51_Panel()
		{
			super();
			
			var bgT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgTexture()).bitmapData);
			var bgS:Sprite2D = new Sprite2D(bgT);
			bgS.y = CONTENT_Y_OFFSET + 20;
			
			addChild(bgS);
			
		}
		
		public function get panelHeight():Number
		{
			return PANEL_HEIGHT + 20;
		}
		
		public function get px():Number
		{
			return this.x;
		}
		
		public function get py():Number
		{
			return this.y;
		}
		
		public function set px(value:Number):void
		{
			this.x = value;
		}
		
		public function set py(value:Number):void
		{
			this.y = value;
		}
		
	}
}