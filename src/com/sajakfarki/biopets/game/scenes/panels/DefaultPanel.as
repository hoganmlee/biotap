package com.sajakfarki.biopets.game.scenes.panels
{
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	
	public class DefaultPanel extends Node2D
	{
		public static var CONTENT_Y_OFFSET:int = -60;
		public static var PANEL_HEIGHT:int = 236;
		public static var TITLE_Y_OFFSET:int = 20;
		public static var PANEL_Y_OFFSET:int = 50;
		
		
		/** Panel */
		[Embed(source = "../media/level_start/panel_default.png")]
		public var _panelImage:Class;
		private var _panel : Sprite2D;
		
		
		public function DefaultPanel()
		{
			super();
			
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _panelImage()).bitmapData);
			_panel = new Sprite2D(p);
			
			addChild(_panel);
			
		}
		
		public function reset():void
		{
			this.alpha = 1;
		}

	}
}