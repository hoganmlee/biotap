package com.sajakfarki.biopets.game.scenes.panels
{
	
	import com.sajakfarki.biopets.game.scenes.buttons.BackButton;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.Signal;
	
	public class HeatWarningPanel extends Sprite2D
	{
		
		/** asset */
		[Embed(source = "../media/alerts/watch_temp.png")]
		private var _panelImage:Class;
		
		public function HeatWarningPanel()
		{
			/** start */
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _panelImage()).bitmapData);
			
			super(p);
			
		}
				
	}
}