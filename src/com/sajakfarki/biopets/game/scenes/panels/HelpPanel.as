package com.sajakfarki.biopets.game.scenes.panels
{
	
	import com.greensock.TweenMax;
	import com.sajakfarki.biopets.components.Cards;
	import com.sajakfarki.biopets.game.scenes.buttons.BackButton;
	import com.sajakfarki.biopets.game.scenes.buttons.EmptyButton;
	import com.sajakfarki.biopets.game.scenes.buttons.TutButton;
	import com.sajakfarki.biopets.systems.TapSystem;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	import org.osflash.signals.Signal;
	
	public class HelpPanel extends Node2D
	{
		//public static const HELP_PANEL_HEIGHT:Number = 186;
		
		public var back:Signal = new Signal();
		
		private var _backButton:BackButton;
		
		[Embed(source = "../media/level_select/help_screens1.png")]
		private var _help1Image:Class
		private var _help1:Sprite2D;
		[Embed(source = "../media/level_select/help_screens2.png")]
		private var _help2Image:Class
		private var _help2:Sprite2D;
		[Embed(source = "../media/level_select/help_screens3.png")]
		private var _help3Image:Class
		private var _help3:Sprite2D;
		[Embed(source = "../media/level_select/help_screens4.png")]
		private var _help4Image:Class
		private var _help4:Sprite2D;
		
		
		
		/** Pause Button */
		[Embed(source = "../media/level_start/panel_help.png")]
		private var _panelImage:Class;
		private var _panel : Sprite2D;
		
		/** Music */
		private var _heatMusic:Sound = new Cards.HEATMP3() as Sound;
		private var _scratchMusic:Sound = new Cards.SCRATCH1MP3() as Sound;
		
		private var _channel:SoundChannel;
		
		private var _heatitup:EmptyButton;
		private var _isPlaying:Boolean = false;
		private var _tumblerCount:uint = 0;
		private var _scratchCount:int;
		private var _isScratching:Boolean;
		private var _scratchitup:EmptyButton;
		private var _tut:TutButton;
		private var _tutNext:EmptyButton;
		
		public function HelpPanel()
		{
			super();
			
			/** start */
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _panelImage()).bitmapData);
			_panel = new Sprite2D(p);
			
			addChild(_panel);
						
			_backButton = new BackButton();
			_backButton.back.add(dispatchBack);
			_backButton.x = 285;
			_backButton.y = 207;
			
			addChild(_backButton);
			
			_heatitup = new EmptyButton("heatitup", 100, 100);
			_heatitup.x = -284;
			_heatitup.y = -198;
			_heatitup.click.add(playHeat);
			
			addChild(_heatitup);
			
			_tut = new TutButton();
			_tut.x = 10;
			_tut.y = 207;
			_tut.help.add(startTut);
			_tut.show();
			
			addChild(_tut);
			
			_heatitup.show();
			
			_scratchitup = new EmptyButton("scratchitup", 100, 100);
			_scratchitup.x = -284;
			_scratchitup.y = -48;
			_scratchitup.click.add(playScratch);
			
			addChild(_scratchitup);
			
			_scratchitup.show();
			
		}
		
		public function startTut():void
		{
			_backButton.back.remove(dispatchBack);
			
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _help1Image()).bitmapData);
			_help1 = new Sprite2D(p);
			_help1.alpha = 0;
			addChild(_help1);
			
			TweenMax.to(_help1, 1 , {alpha:1});
						
			_tutNext = new EmptyButton("tutButton", 150, 50);
			_tutNext.x = 240;
			_tutNext.y = 213;
			_tutNext.click.add(nextPanel2);
			_tutNext.show();
			
			addChild(_tutNext);
			
		}
		
		private function nextPanel2(s:String):void
		{
			_tutNext.click.remove(nextPanel2);
			
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _help2Image()).bitmapData);
			_help2 = new Sprite2D(p);
			
			addChild(_help2);
			
			_tutNext.x = -239;
			_tutNext.y = 151;
			_tutNext.click.add(nextPanel3);
			_tutNext.show();
			
			removeChild(_help1);
			
		}
		
		private function nextPanel3(s:String):void
		{
			_tutNext.click.remove(nextPanel3);
			
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _help3Image()).bitmapData);
			_help3 = new Sprite2D(p);
			
			addChild(_help3);
			
			_tutNext.x = -51;
			_tutNext.y = 212;
			_tutNext.click.add(nextPanel4);
			_tutNext.show();
			
			removeChild(_help2);
			
		}
		
		private function nextPanel4(s:String):void
		{
			_tutNext.click.remove(nextPanel4);
			
			
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _help4Image()).bitmapData);
			_help4 = new Sprite2D(p);
			
			addChild(_help4);
			
			_tutNext.x = 24;
			_tutNext.y = 190;
			_tutNext.click.add(closeHelpPanel);
			_tutNext.show();
			
			removeChild(_help3);
			
		}
		
		private function closeHelpPanel(s:String):void
		{
			_backButton.back.add(dispatchBack);
			
			_tut.show();
			
			removeChild(_tutNext);
			removeChild(_help4);
		}
				
		private function playHeat(s:String):void
		{
			_tumblerCount++;
			_heatitup.show();
			
//			trace("_tumblerCount = " + _tumblerCount);
			
			if(_tumblerCount > 5){
				
				_tumblerCount = 0;
				
				if(!_isPlaying){
					
					_isPlaying = true;
					
					
					if(_channel){
						_channel.stop();
						_channel = null;
					}
					
					_channel = _heatMusic.play();
					
					Cards.ISHEAT = true;
					
					_heatitup.hide();
				
				}
				
			}
			
		}
		
		private function playScratch(s:String):void
		{
			_scratchCount++;
			_scratchitup.show();
			
			//			trace("_tumblerCount = " + _tumblerCount);
			
			if(_scratchCount > 5){
				
				_scratchCount = 0;
				
				if(!_isScratching){
					
					_isScratching = true;
					
					if(_channel){
						_channel.stop();
						_channel = null;
					}
					
					_channel = _scratchMusic.play();
					
					TapSystem.scratches = new Vector.<Sound>();
					TapSystem.scratches.push(new Cards.SCRATCH1MP3() as Sound);
					TapSystem.scratches.push(new Cards.SCRATCH2MP3() as Sound);
					TapSystem.scratches.push(new Cards.SCRATCH3MP3() as Sound);
					TapSystem.scratches.push(new Cards.SCRATCH4MP3() as Sound);
					TapSystem.scratches.push(new Cards.SCRATCH5MP3() as Sound);
					
					Cards.ISSCRATCH= true;
					
					_scratchitup.hide();
					
				}
				
			}
			
			
		}
				
		private function dispatchBack():void
		{
			back.dispatch();
		}
		
		public function enable():void
		{
			_backButton.show();
		}
		
		public function reset():void
		{
			this.alpha = 1;
		}
		
	}
}