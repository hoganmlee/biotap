package com.sajakfarki.biopets.game.scenes.panels
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.sajakfarki.biopets.game.scenes.buttons.*;
	import com.sajakfarki.biopets.graphics.text.NumBrownView;
	import com.sajakfarki.biopets.graphics.text.NumberView;
	import com.sajakfarki.biopets.graphics.text.PerBrownView;
	import com.sajakfarki.biopets.graphics.text.PercentView;
	import com.sajakfarki.biopets.graphics.text.TimerBrownView;
	import com.sajakfarki.biopets.graphics.types.TextTypes;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	
	import org.osflash.signals.Signal;
	
	public class LevelCompletePanel extends Node2D
	{
		public static var PANEL_HEIGHT:Number = 258;
		public static var OFFSET:int = 40;
		
		private static const STAR_WIDTH:int = 36;
		private static const BUTTON_Y_OFFSET:int = 170;
		
		private static const BTU_X:int = 130;
		private static const PER_X:int = -68;
		private static const TIME_X:int = 320;
		
		private static const BTU_SCALE:Number = 0.5;
		private static const PER_SCALE:Number = 1;
		
		public var retry : Signal = new Signal();
		public var map : Signal = new Signal();
		public var unlock : Signal = new Signal();
		
		/** Panel */
		[Embed(source = "../media/level_complete/panel_end_level.png")]
		private var _panelImage : Class;
		private var _panel : Sprite2D;
		
		[Embed(source = "../media/level_complete/good_work.png")]
		private var _goodImage : Class;
		private var _good : Sprite2D;
		
		[Embed(source = "../media/level_complete/production_rating.png")]
		private var _prImage : Class;
		private var _prSprite : Sprite2D;
		
		[Embed(source = "../media/level_complete/production_btus.png")]
		private var _btuImage : Class;
		private var _btuSprite : Sprite2D;
		
		[Embed(source = "../media/level_complete/time_left.png")]
		private var _timeImage : Class;
		private var _timeSprite : Sprite2D;		
		
		[Embed(source = "../media/level_complete/star_gold.png")]
		private var _goldImage : Class;
		private var _goldSprite : Sprite2D;
		
		[Embed(source = "../media/level_complete/star_silver.png")]
		private var _silverImage : Class;
		private var _silverSprite : Sprite2D;
		
		[Embed(source = "../media/willy/willy_happy.png")]
		private var _willyImage : Class;
		private var _willy : Sprite2D;
		
		private var _percentView:PerBrownView;
		private var _btuView:NumBrownView;
		private var _timeView:TimerBrownView;
		
//		[Embed(source = "../media/level_complete/temp_level_complete.png")]
//		private var _tempImage:Class;
//		private var _temp : Sprite2D;
		
		
		private var _mapButton : MapButton;
		private var _retryButton : RetryLevelButton;
		private var _unlockButton : UnlockButton;
		
		
		public function LevelCompletePanel()
		{
			super();
			
			/** start */
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _panelImage()).bitmapData);
			_panel = new Sprite2D(p);
			
			addChild(_panel);
			
			/** willy */
			var w:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _willyImage()).bitmapData);
			_willy = new Sprite2D(w);
			_willy.x = -309;
			_willy.y = 10;
			addChild(_willy);
			
			/** good work */
			var g:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _goodImage()).bitmapData);
			_good = new Sprite2D(g);
			_good.x = 63;
			_good.y = -169;
			addChild(_good);
			
			
			/** production rating */
			var pr:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _prImage()).bitmapData);
			_prSprite = new Sprite2D(pr);
			_prSprite.x = PER_X;
			_prSprite.y = -116;
			addChild(_prSprite);
			
			/** production btus */
			var b:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _btuImage()).bitmapData);
			_btuSprite = new Sprite2D(b);
			_btuSprite.x = BTU_X;
			_btuSprite.y = -108;
			addChild(_btuSprite);
			
			/** production btus */
			var c:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _timeImage()).bitmapData);
			_timeSprite = new Sprite2D(c);
			_timeSprite.x = TIME_X;
			_timeSprite.y = -116;
			addChild(_timeSprite);			
			
			_percentView= new PerBrownView(4);
			_percentView.y = -63;
			_percentView.scaleX = PER_SCALE
			_percentView.scaleY = PER_SCALE
			addChild(_percentView);
			//			_percentView.alpha = 0;3
			
			_btuView = new NumBrownView(10);
			_btuView.y = -59;
			_btuView.scaleX = BTU_SCALE
			_btuView.scaleY = BTU_SCALE
			addChild(_btuView);
			//			_btuView.alpha = 0;
			
			_timeView = new TimerBrownView(4);
			_timeView.y = -63;
			_timeView.scaleX = PER_SCALE
			_timeView.scaleY = PER_SCALE
			addChild(_timeView);
			
			
			/** temp */
//			var t:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _tempImage()).bitmapData);
//			_temp = new Sprite2D(t);
//			
//			addChild(_temp);
			
			
			/** quit */
			_mapButton = new MapButton();
			_mapButton.alpha = 0;
			_mapButton.map.add(quitGame);
			_mapButton.scaleX = 0.1;
			_mapButton.scaleY = 0.1;
			_mapButton.x =  185;
			_mapButton.y = BUTTON_Y_OFFSET;
			//_mapButton.show();
			
			addChild(_mapButton);
			
			/** retry */
			_retryButton = new RetryLevelButton();
			_retryButton.alpha = 0;
			_retryButton.retry.add(retryGame);
			_retryButton.scaleX = 0.1;
			_retryButton.scaleY = 0.1;
			_retryButton.x = 285;
			_retryButton.y = BUTTON_Y_OFFSET;
			//_retryButton.show();
			
			addChild(_retryButton);
			
			/** retry */
			_unlockButton = new UnlockButton();
			_unlockButton.alpha = 0
			_unlockButton.scaleX = 0.1;
			_unlockButton.scaleY = 0.1;
			_unlockButton.x = 385;
			_unlockButton.y = BUTTON_Y_OFFSET;
			//_unlockButton.show();
			
			addChild(_unlockButton);
			
		}
		
		public function setPercent(param0:int):void
		{
			_percentView.setNum(param0);
			_percentView.x = PER_X - (PER_SCALE * _percentView.spriteWidth)/2 + 8;
			
			if(param0 < 25){
				
				addStar("silver", 0);
				addStar("silver", 1);
				addStar("silver", 2);
				addStar("silver", 3);
				
			}else if(param0 >= 25 && param0 < 50){
				
				addStar("gold", 0);
				addStar("silver", 1);
				addStar("silver", 2);
				addStar("silver", 3);
				
			}else if(param0 >= 50 && param0 < 75){
				
				addStar("gold", 0);
				addStar("gold", 1);
				addStar("silver", 2);
				addStar("silver", 3);
								
			}else if(param0 >= 75 && param0 < 100){
				
				addStar("gold", 0);
				addStar("gold", 1);
				addStar("gold", 2);
				addStar("silver", 3);
				
			}else{
				
				addStar("gold", 0);
				addStar("gold", 1);
				addStar("gold", 2);
				addStar("gold", 3);
				
			}
			
		}
		
		private function addStar(type:String, position:int):void
		{
			var b:Texture2D 
			var star:Sprite2D;
			
			if(type == "gold"){
				
				b = Texture2D.textureFromBitmapData(Bitmap(new _goldImage()).bitmapData);
				star = new Sprite2D(b);
				
			}else{
				
				b = Texture2D.textureFromBitmapData(Bitmap(new _silverImage()).bitmapData);
				star = new Sprite2D(b);
				
			}
			
			star.x = -125 + STAR_WIDTH * position;
			star.y = -5
			addChild(star);
			
		}
		
		public function setNumber(param0:int, time:int):void
		{
			_btuView.setNum(param0);
			_btuView.x = BTU_X - (BTU_SCALE*_btuView.spriteWidth)/2 + 3
						
			_timeView.setNum(time);
			_timeView.x = TIME_X - (PER_SCALE*_timeView.spriteWidth)/2 + 16;
			
		}
		
		public function showButtons(unlock:Boolean):void
		{
			_mapButton.show();
			_retryButton.show();
			
			if(unlock){
			
				_unlockButton.show();
				_unlockButton.unlock.add(unlockGame);
				
				TweenMax.to(_unlockButton, 0.5, {delay:1.7, scaleX:1, scaleY:1, alpha:1, ease:Back.easeOut});
				
				
				
				
			}else{
				
				_mapButton.x  = 265;
				_retryButton.x = 365;
				
			}
			
			TweenMax.to(_mapButton, 0.5, {delay:0.2, scaleX:1, scaleY:1, alpha:1, ease:Back.easeOut});
			TweenMax.to(_retryButton, 0.5, {delay:0.3, scaleX:1, scaleY:1, alpha:1, ease:Back.easeOut});
			
		}
		
				
		/**************************************************
		 * 
		 * 	Dispatch Signals
		 * 
		 **************************************************/
		private function unlockGame():void
		{
			hideButtons();
			TweenMax.delayedCall(0.8, unlock.dispatch);
		}
		
		private function retryGame():void
		{
			hideButtons();
			TweenMax.delayedCall(0.8, retry.dispatch);
		}
		
		private function quitGame():void
		{
			hideButtons();
			TweenMax.delayedCall(0.8, map.dispatch);
		}
		
		private function hideButtons():void
		{
			TweenMax.to(_mapButton, 0.5, {delay:0., scaleX:0.1, scaleY:0.1, alpha:0, ease:Back.easeIn});
			TweenMax.to(_retryButton, 0.5, {delay:0.1, scaleX:0.1, scaleY:0.1, alpha:0, ease:Back.easeIn});
			TweenMax.to(_unlockButton, 0.5, {delay:0.2, scaleX:0.1, scaleY:0.1, alpha:0, ease:Back.easeIn});
		}
		
	}
	
}