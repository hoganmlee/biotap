package com.sajakfarki.biopets.game.scenes.panels
{
	import com.sajakfarki.biopets.game.scenes.buttons.*;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.events.Event;
	
	import org.osflash.signals.Signal;
	
	public class FailPanel extends Node2D
	{
		public static var PANEL_HEIGHT:Number = 258;
		public static var OFFSET:int = 40;
		
		private static const BUTTON_Y_OFFSET:int = 170;
		
		public var retry : Signal = new Signal();
		public var map : Signal = new Signal();
		
		/** Panel */
		[Embed(source = "../media/level_complete/panel_end_level.png")]
		private var _panelImage : Class;
		private var _panel : Sprite2D;
		
		[Embed(source = "../media/level_complete/fail_2.png")]
		private var _failImage : Class;
		private var _fail : Sprite2D;
		
		[Embed(source = "../media/level_complete/fail_1.png")]
		private var _diedImage : Class;
		private var _died : Sprite2D;
		
		[Embed(source = "../media/level_complete/fail_3.png")]
		private var _notEnoughImage : Class;
		private var _notEnough : Sprite2D;
		
		[Embed(source = "../media/willy/willy_sad.png")]
		private var _willyImage : Class;
		private var _willy : Sprite2D;
		
		private var _mapButton : MapButton;
		private var _retryButton : RetryLevelButton;
		
		public function FailPanel()
		{
			super();
			
			/** start */
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _panelImage()).bitmapData);
			_panel = new Sprite2D(p);
			
			addChild(_panel);
			
			/** willy */
			var w:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _willyImage()).bitmapData);
			_willy = new Sprite2D(w);
			
			addChild(_willy);
			
			/** blast! */
			var g:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _failImage()).bitmapData);
			_fail = new Sprite2D(g);
			_fail.alpha = 0;
			_fail.x = 150;
			_fail.y = -120;
			//			
			addChild(_fail);
			
			/** blast! */
			var b2:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _diedImage()).bitmapData);
			_died= new Sprite2D(b2);
			_died.alpha = 0;
			_died.x = 150;
			_died.y = -120;
			//			
			addChild(_died);
			
			/** blast! */
			var b3:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _notEnoughImage()).bitmapData);
			_notEnough = new Sprite2D(b3);
			_notEnough.alpha = 0;
			_notEnough.x = 150;
			_notEnough.y = -110;
			//			
			addChild(_notEnough);
			
			
			/** quit */
			_mapButton = new MapButton();
			_mapButton.map.add(quitGame);
			_mapButton.x =  265;
			_mapButton.y = BUTTON_Y_OFFSET;
			_mapButton.show();
			_mapButton.alpha = 1;
			
			addChild(_mapButton);
			
			/** retry */
			_retryButton = new RetryLevelButton();
			_retryButton.retry.add(retryGame);
			_retryButton.x = 365;
			_retryButton.y = BUTTON_Y_OFFSET;
			_retryButton.show();
			_retryButton.alpha = 1;
			
			addChild(_retryButton);
	
		}
		
		public function show(value:int):void
		{
			
			switch(value){
				
				case 0:
					_fail.alpha = 1;
					break;
				case 1:
					_died.alpha = 1;
					break;
				case 2:
					_notEnough.alpha = 1;
					break;
				
			}
			
		}
		
		/**************************************************
		 * 
		 * 	Dispatch Signals
		 * 
		 **************************************************/
		
		private function retryGame():void
		{
			retry.dispatch();
		}
		
		private function quitGame():void
		{
			map.dispatch();
		}
		
	}
	
}