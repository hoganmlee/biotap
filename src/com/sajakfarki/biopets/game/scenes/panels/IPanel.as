package com.sajakfarki.biopets.game.scenes.panels
{
	public interface IPanel
	{
		function get px():Number;
		function set px(value:Number):void;
		function get py():Number;
		function set py(value:Number):void;
		
		function get panelHeight():Number;
	}
}