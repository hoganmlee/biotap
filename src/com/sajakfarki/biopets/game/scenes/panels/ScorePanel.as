package com.sajakfarki.biopets.game.scenes.panels
{
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	
	public class ScorePanel extends Node2D
	{
		public static var PANEL_HEIGHT:Number = 49;
		
		/** Background texture */
		[Embed(source = "../media/game_interface/gemCollector_back.png")]
		private var _bgTexture:Class;
		private var _panel : Sprite2D;
		
		public function ScorePanel()
		{
			super();
			
			var p:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgTexture()).bitmapData);
			_panel = new Sprite2D(p);
			
			addChild(_panel);
			
			//TODO: score text;
			
			
		}
	}
}