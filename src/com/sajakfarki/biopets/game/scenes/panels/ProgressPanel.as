package com.sajakfarki.biopets.game.scenes.panels
{
	import com.greensock.TweenMax;
	import com.sajakfarki.biopets.components.GameState;
	import com.sajakfarki.biopets.graphics.text.NumView;
	import com.sajakfarki.biopets.graphics.text.NumberView;
	import com.sajakfarki.biopets.graphics.text.PerView;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	import com.sajakfarki.biopets.graphics.types.TextTypes;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Quad2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.geom.Point;
	
	public class ProgressPanel extends Node2D
	{
		public static var BACK_COLOR:int = 0xCC453e02;
		public static var ACTIVE_COLOR:int = 0xFF718611;
		public static var INACTIVE_COLOR:int = 0xFF8a8457;
		
		private static var OFFSET:int = -15;
		
		private var _stageNum:int;
		
		/** Gas */
		[Embed(source = "../media/game_interface/gas_potential.png")]
		private var _gasImage:Class;
		private var _gas : Sprite2D;
		
		[Embed(source = "../media/game_interface/title_hydro.png")]
		private var _hydroImage:Class;
		private var _hydroTitle : Sprite2D;
		
		[Embed(source = "../media/game_interface/title_acido.png")]
		private var _acidoImage:Class;
		private var _acidoTitle : Sprite2D;
		
		[Embed(source = "../media/game_interface/title_aceto.png")]
		private var _acetoImage:Class;
		private var _acetoTitle : Sprite2D;
		
		[Embed(source = "../media/game_interface/title_meth.png")]
		private var _methImage:Class;
		private var _methTitle : Sprite2D;
				
		private var _hydroBack:Quad2D;
		private var _acidoBack:Quad2D;
		private var _acetoBack:Quad2D;
		private var _methBack:Quad2D;
		
		private var _hydroOver:Quad2D;
		private var _acidoOver:Quad2D;
		private var _acetoOver:Quad2D;
		private var _methOver:Quad2D;
		
		private var _progress:Quad2D;
		
		private var _percentView0:PerView;
		private var _percentView1:PerView;
		private var _percentView2:PerView;
		private var _percentView3:PerView;
		
		private var _btuView:NumView;
		
		public function ProgressPanel()
		{
			super();
			
			var g:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _gasImage()).bitmapData);
			_gas = new Sprite2D(g);
			_gas.x = 196;
			_gas.y = -44;
			addChild(_gas);
			
			var t1:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _hydroImage()).bitmapData);
			_hydroTitle = new Sprite2D(t1);
			_hydroTitle.x = -250;
			_hydroTitle.y = -35;
			addChild(_hydroTitle);
			
			var t2:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _acidoImage()).bitmapData);
			_acidoTitle = new Sprite2D(t2);
			_acidoTitle.x = -150;
			_acidoTitle.y = -35;
			addChild(_acidoTitle);

			var t3:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _acetoImage()).bitmapData);
			_acetoTitle = new Sprite2D(t3);
			_acetoTitle.x = -50;
			_acetoTitle.y = -35;
			addChild(_acetoTitle);
			
			var t4:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _methImage()).bitmapData);
			_methTitle = new Sprite2D(t4);
			_methTitle.x = 50;
			_methTitle.y = -35;
			addChild(_methTitle);
			
			_hydroBack = new Quad2D(100, 18);
			_hydroBack.pivot = new Point(-50, 9);
			_hydroBack.topLeftColor = _hydroBack.topRightColor = _hydroBack.bottomRightColor = _hydroBack.bottomLeftColor = BACK_COLOR;
			_hydroBack.x = -300;
			
			_acidoBack = new Quad2D(100, 18);
			_acidoBack.pivot = new Point(-50, 9);
			_acidoBack.topLeftColor = _acidoBack.topRightColor = _acidoBack.bottomRightColor = _acidoBack.bottomLeftColor = BACK_COLOR;
			_acidoBack.x = -200;
			
			_acetoBack = new Quad2D(100, 18);
			_acetoBack.pivot = new Point(-50, 9);
			_acetoBack.topLeftColor = _acetoBack.topRightColor = _acetoBack.bottomRightColor = _acetoBack.bottomLeftColor = BACK_COLOR;
			_acetoBack.x = -100;
			
			_methBack = new Quad2D(100, 18);
			_methBack.pivot = new Point(-50, 9);
			_methBack.topLeftColor = _methBack.topRightColor = _methBack.bottomRightColor = _methBack.bottomLeftColor = BACK_COLOR;
			_methBack.x = 0;
			
			addChild(_hydroBack);
			addChild(_acidoBack);
			addChild(_acetoBack);
			addChild(_methBack);
			
			
			_btuView = new NumView(10);
			_btuView.scaleX = 0.6;
			_btuView.scaleY = 0.6;
			_btuView.x = 134;
			_btuView.y = -18;
			
			addChild(_btuView);
			
			
			_percentView0 = new PerView(4);
			_percentView0.scaleX = 0.6;
			_percentView0.scaleY = 0.6;
			_percentView0.setNum(0);
			_percentView0.x = -250 - _percentView0.spriteWidth/4;
			_percentView0.y = -50;
			addChild(_percentView0);
			
			_percentView1 = new PerView(4);
			_percentView1.scaleX = 0.6;
			_percentView1.scaleY = 0.6;
			_percentView1.setNum(0);
			_percentView1.x = -150 - _percentView1.spriteWidth/4;
			_percentView1.y = -50;
			addChild(_percentView1);
			
			_percentView2 = new PerView(4);
			_percentView2.scaleX = 0.6;
			_percentView2.scaleY = 0.6;
			_percentView2.setNum(0);
			_percentView2.x = -50 - _percentView2.spriteWidth/4;
			_percentView2.y = -50;
			addChild(_percentView2);
			
			_percentView3 = new PerView(4);
			_percentView3.scaleX = 0.6;
			_percentView3.scaleY = 0.6;
			_percentView3.setNum(0);
			_percentView3.x = 50 - _percentView3.spriteWidth/4;
			_percentView3.y = -50;
			addChild(_percentView3);
			
			
			
		}
		
		public function updateProgress():void
		{
			_btuView.setNum(GameState.btuMultiplier * GameState.stageGems);
			_progress.width = GameState.stageGems / GameState.totalGemsPossible * 100;
			
			switch(_stageNum){
				
				case 0:
					_percentView0.setNum(_progress.width);
					_percentView0.x = -250 - _percentView0.spriteWidth/4;
					break;
				case 1:
					_percentView1.setNum(_progress.width);
					_percentView1.x = -150 - _percentView1.spriteWidth/4;
					break;
				case 2:
					_percentView2.setNum(_progress.width);
					_percentView2.x = -50 - _percentView2.spriteWidth/4;
					break;
				case 3:
					_percentView3.setNum(_progress.width);
					_percentView3.x = 50 - _percentView3.spriteWidth/4;
					break;
					
			}
			
		}
		
		public function setStage(value:int):void
		{
			_stageNum = value;
						
			_progress = new Quad2D(100, 18);
			_progress.pivot = new Point(-50, 9);
			_progress.topLeftColor = _progress.topRightColor = _progress.bottomRightColor = _progress.bottomLeftColor = ACTIVE_COLOR;
			
			switch(value){
				
				case 0:
					
					_percentView0.alpha = 1;
					_percentView1.alpha = 0;
					_percentView2.alpha = 0;
					_percentView3.alpha = 0;
					
					_progress.x = -300;
					
					_percentView0.x = -250 - _percentView0.spriteWidth/4;
					_percentView0.y += OFFSET;
					
					_hydroTitle.y += OFFSET;
					_hydroBack.height  = 34;
					
					
					_acidoTitle.alpha = 0.5;
					_acetoTitle.alpha = 0.5;
					_methTitle.alpha = 0.5;
					
					break;
				
				case 1:
					
					addHydro();
					_percentView0.alpha = 0.5;
					_percentView1.alpha = 1;
					_percentView2.alpha = 0;
					_percentView3.alpha = 0;
					
					_progress.x = -200;

					_percentView1.x = -150 - _percentView1.spriteWidth/4;
					_percentView1.y += OFFSET;
										
					_acidoTitle.y += OFFSET;
					_acidoBack.height = 34;
					
					_acetoTitle.alpha = 0.5;
					_methTitle.alpha = 0.5;
					
					break;
				
				case 2:
					
					addHydro();
					addAcido();
					
					_percentView0.alpha = 0.5;
					_percentView1.alpha = 0.5;
					_percentView2.alpha = 1;
					_percentView3.alpha = 0;
					
					_progress.x = -100;
					
					_percentView2.x = -50 - _percentView2.spriteWidth/4;
					_percentView2.y += OFFSET;
					
					_acetoTitle.y += OFFSET;
					_acetoBack.height = 34;
					
					_methTitle.alpha = 0.5;
					
					break;
				
				case 3:
					
					addHydro();
					addAcido();
					addAceto();
					
					_percentView0.alpha = 0.5;
					_percentView1.alpha = 0.5;
					_percentView2.alpha = 0.5;
					_percentView3.alpha = 1;
					
					_percentView3.x = 50 - _percentView3.spriteWidth/4;
					_percentView3.y += OFFSET
					
					_methTitle.y += OFFSET;
					_methBack.height = 34;
					
					_progress.x = 0;
										
					break;
				
			}
			
			_progress.width = 0.1;
			_progress.height = 34;
			
			addChild(_progress);
			
		}
		
		private function addHydro():void
		{
			_hydroTitle.alpha = 0.5;
			_hydroOver = new Quad2D(100, 18)
			_hydroOver.pivot = new Point(-50, 9);
			_hydroOver.topLeftColor = _hydroOver.topRightColor = _hydroOver.bottomRightColor = _hydroOver.bottomLeftColor = INACTIVE_COLOR;
			_hydroOver.x = -300;
			_hydroOver.width = GameState.stage1Collected / GameState.totalGemsPossible * 100;
			
			_percentView0.setNum(GameState.stage1Collected / GameState.totalGemsPossible * 100);
			_percentView0.x = -250 - _percentView0.spriteWidth/4;
			
			addChild(_hydroOver);
		}
		
		private function addAcido():void
		{
			_acidoTitle.alpha = 0.5;
			_acidoOver = new Quad2D(100, 18)
			_acidoOver.pivot = new Point(-50, 9);
			_acidoOver.topLeftColor = _acidoOver.topRightColor = _acidoOver.bottomRightColor = _acidoOver.bottomLeftColor = INACTIVE_COLOR;
			_acidoOver.x = -200;
			_acidoOver.width = GameState.stage2Collected / GameState.totalGemsPossible * 100;
			addChild(_acidoOver);
			
			_percentView1.setNum(GameState.stage2Collected / GameState.totalGemsPossible * 100);
			_percentView1.x = -150 - _percentView1.spriteWidth/4;
			
		}
		
		private function addAceto():void
		{
			_acetoTitle.alpha = 0.5;
			_acetoOver = new Quad2D(100, 18)
			_acetoOver.pivot = new Point(-50, 9);
			_acetoOver.topLeftColor = _acetoOver.topRightColor = _acetoOver.bottomRightColor = _acetoOver.bottomLeftColor = INACTIVE_COLOR;
			_acetoOver.x = -100;
			_acetoOver.width = GameState.stage3Collected / GameState.totalGemsPossible * 100;
			
			_percentView2.setNum(GameState.stage3Collected / GameState.totalGemsPossible * 100);
			_percentView2.x = -50 - _percentView2.spriteWidth/4;
			
			addChild(_acetoOver);
		}
		
	}
}