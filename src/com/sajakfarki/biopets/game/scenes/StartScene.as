package com.sajakfarki.biopets.game.scenes 
{
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Linear;
	import com.sajakfarki.biopets.components.GameState;
	import com.sajakfarki.biopets.game.scenes.panels.FailPanel;
	import com.sajakfarki.biopets.game.scenes.panels.LevelCompletePanel;
	import com.sajakfarki.biopets.game.scenes.panels.ProgressPanel;
	import com.sajakfarki.biopets.graphics.compounds.base.PooReactionSegment;
	import com.sajakfarki.biopets.graphics.controls.HeatView;
	import com.sajakfarki.biopets.graphics.text.*;
	import com.sajakfarki.biopets.graphics.types.CompoundTypes;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	import com.sajakfarki.biopets.graphics.types.TextTypes;
	import com.sajakfarki.biopets.utils.GemCounter;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Quad2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.geom.Vector3D;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	
	import org.flexunit.internals.runners.statements.StatementSequencer;
	import org.osflash.signals.Signal;
	
	public class StartScene extends Scene2D
	{	
		public var start:Signal = new Signal();
		public var done:Signal = new Signal();
	
		/** Background texture */
		[Embed(source = "../media/start_screen/startScreen.jpg")]
		private var _bgStart:Class;
		private var _bg : Sprite2D;
		
		
		/** start button texture */
		[Embed(source = "../media/start_screen/startButton.png")]
		private var _sbStart:Class;
		private var _sb : Sprite2D;
		
		/** loading texture */
		[Embed(source = "../media/start_screen/loading.png")]
		private var _loading:Class;
		private var _load : Sprite2D;
		
		private var _levelComplete:LevelCompletePanel;

		public function StartScene(width:Number, height:Number)
		{
			super();
			
			/** background texture */
			var bgt:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgStart()).bitmapData);
			_bg = new Sprite2D(bgt);
						
			//_bg.alpha = 1;
			_bg.x = width/2;
			_bg.y = height/2;
			
			addChild(_bg);
			
			
			var lt:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _loading()).bitmapData);
			_load = new Sprite2D(lt);
			_load.mouseEnabled = true;
			
			_load.x = 818;
			_load.y = 572;
			
			addChild(_load);
			
			var sbt:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _sbStart()).bitmapData);
			_sb = new Sprite2D(sbt);
			_sb.mouseEnabled = true;
			
			_sb.alpha = 0;
			_sb.x = 818;
			_sb.y = 572
			
			addChild(_sb);
			
//			var f:FailPanel = new FailPanel();
//			f.x = width/2;
//			f.y = height/2;
//			
//			addChild(f);
			
//			_levelComplete = new LevelCompletePanel();
//			_levelComplete.x = width/2;
//			_levelComplete.y = height/2+LevelCompletePanel.OFFSET;
//			_levelComplete.setPercent(77);
//			_levelComplete.setNumber(100000, 99);
//			
//			addChild(_levelComplete);
//			
//			_levelComplete.showButtons(true);
			
//			var pp:ProgressPanel = new ProgressPanel();
//			pp.x = width/2;
//			pp.y = height;
//			addChild(pp);
//			
//			pp.setStage(2);
//			pp.updateProgress();
			
			
//			var per:PercentView = new PercentView(TextTypes.BROWN);
//			per.x = 150;
//			per.y = 300;
//			per.setPercent(97);
//			addChild(per);
			
			
//			var n:NumberView = new NumberView(TextTypes.BROWN, 10020120);
//			n.x = 150;
//			n.y = 300;
//			addChild(n);
//			
//			
//			var hv:HeatView = new HeatView(100, 100, 0, 83, 30);
//			hv.x = 650;
//			hv.y = 600;
//			addChild(hv);
			
			
//			var gc:GemCounterView = new GemCounterView();
//			gc.x = 450;
//			gc.y = 200;
//			
//			addChild(gc);
//			
//			var ti:TimerView = new TimerView();
//			TimerView.TEXT_PADDING =3;
//			TimerView.NUM_CHARS = 4;
//			
//			ti.time = 500;
//			//ti.timesup.add(DoSomething);
//			ti.x = 150;
//			ti.y = 300;
//			addChild(ti);
////			
//			TweenMax.delayedCall(1, ti.start);
			
		}
		
//		private function DoSomething():void
//		{
//			trace("timesup!!!");
//		}
		
		// *****************************************************************************************************************
		// * Protected																									   *
		// *****************************************************************************************************************
		override protected function step(elapsed:Number):void
		{
			super.step(elapsed);
		}
		
		public function show():void
		{
			_load.alpha = 0;
			
			//_sb.addEventListener(TouchEvent.TOUCH_BEGIN, startGame);
			_sb.addEventListener(MouseEvent.CLICK, startGame);
			TweenMax.to(_sb, 1, {alpha:1});
			
		}
		
//		protected function startGame(event:TouchEvent):void
		protected function startGame(event:MouseEvent):void
		{
			_sb.mouseEnabled = false;
			_sb.removeEventListener(MouseEvent.CLICK, startGame);
			hide();
		}
		
		private function hide():void
		{
			//_startScreenMusic.
			//_startScreenMusic.close();
			
			TweenMax.to(_bg, 1, {alpha:0})
			TweenMax.to(_sb, 0.6, {delay:0.2, y: 850, ease:Back.easeIn, onComplete:dispatchDone});
		}
		
		private function dispatchDone():void
		{
			done.dispatch();
		}
		
	}
	
}