package com.sajakfarki.biopets.game.levels
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.GameBatches;
	import com.sajakfarki.biopets.game.LevelCreator;
	import com.sajakfarki.biopets.game.base.DefaultGame;
	import com.sajakfarki.biopets.game.base.ILevel;
	import com.sajakfarki.biopets.game.scenes.GameScene;
	import com.sajakfarki.biopets.game.scenes.panels.Level11_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.Level14_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.LevelCompletePanel;
	import com.sajakfarki.biopets.graphics.text.NumberView;
	import com.sajakfarki.biopets.graphics.text.PercentView;
	import com.sajakfarki.biopets.graphics.types.BacteriaTypes;
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	import com.sajakfarki.biopets.graphics.types.CompoundTypes;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	import com.sajakfarki.biopets.graphics.types.TextTypes;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.*;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Quad2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	import org.osflash.signals.Signal;
	
	public class Level_5_4 extends DefaultGame implements ILevel
	{
		
		[Inject]
		public var levelCreator : LevelCreator;
		
		[Inject]
		public var stageState : StageState;
		
		/** Background texture */
		[Embed(source = "../media/backgrounds/level5stage4.jpg")]
		private var _bgTexture:Class;
		
		[Embed(source = "../media/titles/stage_level_panel.png")]
		private var _titleTexture:Class;
		
		[Embed(source = "../media/titles/level5.png")]
		private var _levelTexture:Class;
		
		[Embed(source = "../media/titles/stage4.png")]
		private var _stageTexture:Class;
		
//		
//		[Embed( "../media/text/brownText.png" )]
//		private var _textAtlasBitmap:Class;
//		[Embed( "../media/text/brownText.plist", mimeType = "application/octet-stream" )]
//		private var _textAtlasXML:Class;
//		
//		private var _textSpriteBatch:Sprite2DBatch;
		
		
		
		
		
		private var _levelComplete : LevelCompletePanel;
		private var _infoPanel:Level14_Panel;
		
		
//		private var _percentView:PercentView;
//		private var _btuView:NumberView;
		private var _unlock:Boolean;
		private var _shade:Quad2D;
		
		
		[Embed(source = "../media/level_complete/t_youHaveUnlocked.png")]
		private var _unlockedTexture:Class;
		private var _unlocked:Sprite2D;
		
		[Embed(source = "../media/level_complete/veh_5.png")]
		private var _carTexture:Class;
		private var _car:Sprite2D;
		
		private var _cardTexture:Class = Cards.CARD5;
		private var _card:Sprite2D;		
		
		public function Level_5_4(contextView:Scene2D, gb:GameBatches, w:Number, h:Number, gemsPerCompound:int)
		{
			var bgT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgTexture()).bitmapData);
			var bgS:Sprite2D = new Sprite2D(bgT);
			
			var tT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _titleTexture()).bitmapData);
			var titleSprite:Sprite2D = new Sprite2D(tT);
			
			var lT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _levelTexture()).bitmapData);
			var lS:Sprite2D = new Sprite2D(lT);
			
			var sT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _stageTexture()).bitmapData);
			var sS:Sprite2D = new Sprite2D(sT);
			
			lS.x = -80;
			sS.x = 80;
			sS.y = -13
				
			titleSprite.addChild(lS);
			titleSprite.addChild(sS);
			
			_infoPanel = new Level14_Panel();
			
			super(contextView,
						gb, 
						w, 
						h,
						bgS,
						titleSprite,
						_infoPanel,
						gemsPerCompound);
			
			stageS = stageState;
			
			init();
			
			_shade = new Quad2D(w, h);
			_shade.topLeftColor = _shade.topRightColor = _shade.bottomRightColor = _shade.bottomLeftColor = 0x99000000;
			_shade.alpha = 0;
			_shade.x = w/2;
			_shade.y = h;
			_shade.scaleX = 0.01;
			_shade.scaleY = 0.01;
			
			contextView.addChildAt(_shade, contextView.numChildren - GameScene.CHILDREN+1);
			
			contextView.addChildAt(titleSprite, contextView.numChildren - GameScene.CHILDREN+1);
			contextView.addChildAt(infoPanel as Node2D, contextView.numChildren - GameScene.CHILDREN+1);
			
			_levelComplete = new LevelCompletePanel();
			_levelComplete.alpha = 0.1;
			_levelComplete.x = w/2;
			_levelComplete.y = h + LevelCompletePanel.PANEL_HEIGHT - 1;
			
			contextView.addChildAt(_levelComplete, contextView.numChildren);
			
			var ut:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _unlockedTexture()).bitmapData);
			_unlocked = new Sprite2D(ut);
			_unlocked.x = 40;
			_unlocked.y = 34;
			_unlocked.scaleX = 0.01;
			_unlocked.scaleY = 0.01;
			_unlocked.alpha = 0.01;
			contextView.addChildAt(_unlocked, contextView.numChildren);
			
			var cd:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _cardTexture()).bitmapData);
			_card = new Sprite2D(cd);
			_card.x = 40;
			_card.y = 34;
			_card.scaleX = 0.01;
			_card.scaleY = 0.01;
			_card.alpha = 0.01;
			contextView.addChildAt(_card, contextView.numChildren);
			
			var cr:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _carTexture()).bitmapData);
			_car = new Sprite2D(cr);
			_car.x = 40;
			_car.y = 34;
			_car.scaleX = 0.01;
			_car.scaleY = 0.01;
			_car.alpha = 0.01;
			contextView.addChildAt(_car, contextView.numChildren);
			
			_levelComplete.retry.add(retry.dispatch);
			_levelComplete.map.add(levelFinished.dispatch);
			_levelComplete.unlock.add(unlock.dispatch);
			
		}
				
		/**************************************************
		 * 
		 * 	Show Level Complete
		 * 
		 **************************************************/
		public function showLevelComplete(tweenTime:Number, delay:Number, unlock:Boolean):void
		{
			_unlock = unlock;
			
			
			_levelComplete.setPercent(GameState.gemsCollected/GameState.totalGemsPossible*100);
			_levelComplete.setNumber(GameState.btuMultiplier * GameState.gemsCollected, levelTime);
			
			
			TweenMax.delayedCall(0.95, showShade);
			TweenMax.to(_shade, 1, {delay:1, alpha:1, scaleX:1, scaleY:1, ease:Quint.easeOut	});
			
			//			_shade.alpha = 1;
			
			
			if(unlock){
				
				_unlocked.y = - 150
				_unlocked.x = screenWidth/2;
				_unlocked.alpha = 1;
				_unlocked.scaleX = 1;
				_unlocked.scaleY = 1;
				
				TweenMax.to(_unlocked, 0.5, {delay:1.5, y:100, ease:Back.easeOut});
				
				TweenMax.delayedCall(1.85, showCar);
				TweenMax.to(_car, 0.5, {delay:1.9, rotation:0, scaleX:1, scaleY:1, ease:Back.easeOut});
				
				
				TweenMax.to(_car, 0.6, {delay:6, rotation:160, scaleX:0.05, scaleY:0.05, ease:Back.easeIn, onComplete:function():void
				{
					_car.alpha = 0;				
				}});
				
				TweenMax.to(_card, 0.6, {delay:6.5, rotation:0, scaleX:0.8, scaleY:0.8, ease:Back.easeOut});
				
				
				TweenMax.to(_unlocked, 0.5, {delay:10, y:-100, ease:Back.easeIn});
				
				TweenMax.to(_card, 0.9, {delay:10.4, x:521, y:545, rotation:345, scaleX:0.4, scaleY:0.4, ease:Back.easeInOut});
				
			}
			
			_levelComplete.alpha = 1;
			TweenMax.to(_levelComplete, tweenTime, {delay:delay, y:screenHeight/2+LevelCompletePanel.OFFSET, ease:Quint.easeInOut, onComplete:showScore});
			//			TweenMax.to(_levelComplete, tweenTime, {delay:delay, y:screenHeight/2+LevelCompletePanel.OFFSET, ease:Quint.easeInOut, onComplete:showScore});
			
		}
		
		private function showShade():void
		{
			_shade.y = screenHeight/2;
		}
		
		private function showCar():void
		{
			_car.rotation = -120;
			_car.y = screenHeight/2;
			_car.x = screenWidth/2;
			_car.alpha = 1;
			
			_card.alpha = 1;
			_card.rotation = -120;
			_card.y = screenHeight/2;
			_card.x = screenWidth/2;
		}
		
		private function showScore():void
		{
			_levelComplete.showButtons(_unlock);
//			_percentView.show();
//			_btuView.show();
			
		}
		
		public function showStageComplete(tweenTime:Number, delay:Number):void{}
		
		public function get nextStage():NextStage
		{
			return null;
		}
		
		public function get initialized():LevelInitialized
		{
			return stageState.levelInitialized;
		}
		
		public function get stageFinished():StageFinished
		{
			return stageState.stageFinished;
		}
		
		override protected function startUp() : void
		{
			injector.injectInto( this );
			
			//stageState.hud.add(dispatchHUD);
			/** Stage variables */
			stageState.width = contextView.stage.stageWidth;
			stageState.height = contextView.stage.stageHeight;
			
			
			stageState.stageType = StageTypes.METHANOGENESIS
			stageState.bacteriaType = BacteriaTypes.SABBY
			stageState.timeLimit = 30
			stageState.maxBacteriaForce = 3
			stageState.maxBacteriaSpeed = 3
			stageState.currentHeat = 71
			stageState.maxHeatOptimal  = 76
			stageState.minHeatOptimal = 56
			stageState.coolRate = 3
			stageState.heatRate = 20
			stageState.rateOfReaction = 495
			stageState.pooPostion.push(new Point(87 , 94),
				new Point(933 , 93),
				new Point(145 , 627),
				new Point(690 , 683));
			
			stageState.bacteriaPostion.push(new Point(447 , 397),
				new Point(527 , 333),
				new Point(468 , 329),
				new Point(578 , 370),
				new Point(525 , 423));
			
			stageState.stickyPosition.push({pos:new Point(368 , 631), scale:0.29},
				{pos:new Point(42 , 752), scale:0.29},
				{pos:new Point(299 , 736), scale:0.36},
				{pos:new Point(472 , 723), scale:0.4},
				{pos:new Point(991 , 234), scale:0.28},
				{pos:new Point(982 , 337), scale:0.39},
				{pos:new Point(956 , 476), scale:0.36},
				{pos:new Point(808 , 554), scale:0.39},
				{pos:new Point(919 , 699), scale:0.35},
				{pos:new Point(731 , 331), scale:0.27},
				{pos:new Point(524 , 93), scale:0.47},
				{pos:new Point(256 , 356), scale:0.26},
				{pos:new Point(174 , 293), scale:0.25},
				{pos:new Point(136 , 382), scale:0.35},
				{pos:new Point(34 , 400), scale:0.26},
				{pos:new Point(49 , 289), scale:0.4},
				{pos:new Point(354 , 315), scale:0.17},
				{pos:new Point(884 , 372), scale:0.23},
				{pos:new Point(364 , 217), scale:0.35});
			
			stageState.killerPosition.push({pos:new Point(282 , 650), scale:0.16},
				{pos:new Point(446 , 614), scale:0.19},
				{pos:new Point(839 , 471), scale:0.2},
				{pos:new Point(268 , 265), scale:0.23},
				{pos:new Point(733 , 16), scale:0.19});
			
			
			super.startUp();
			
			/** Add Systems */
			addSystems();
			
		}
		
		override protected function mapInjectors() : void
		{
			super.mapInjectors();
			
			injector.mapSingleton( LevelCreator );
			injector.mapSingleton( StageState );
			injector.mapSingleton( PreUpdate );
			injector.mapSingleton( Update );
			injector.mapSingleton( Move );
			injector.mapSingleton( ResolveCollisions );
			injector.mapSingleton( Render );
			
			//injector.mapValue( KeyPoll, new KeyPoll( contextView.stage ) );
			
			injector.injectInto( this );
			
		}
		
		public function addSystems() : void
		{
			systemManager.addSystem( GameManager );
			
			systemManager.addSystem( TapSystem );
			systemManager.addSystem( HeatControlSystem );
			systemManager.addSystem( PooSystem );
			
			systemManager.addSystem( OrbitSystem );
			systemManager.addSystem( MotionSystem );
			
			systemManager.addSystem( CollisionSystem );
			systemManager.addSystem( RenderSystem );
			systemManager.addSystem( ProcessManager );			
		}
		
		public function finish():void
		{
			systemManager.removeSystem(PooSystem);
			systemManager.removeSystem(OrbitSystem);
			systemManager.removeSystem(HeatControlSystem);
		}
		
		public function disposeLevel():void
		{
			dispose();
		}
	}
}