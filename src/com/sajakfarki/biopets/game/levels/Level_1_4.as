package com.sajakfarki.biopets.game.levels
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.GameBatches;
	import com.sajakfarki.biopets.game.LevelCreator;
	import com.sajakfarki.biopets.game.base.DefaultGame;
	import com.sajakfarki.biopets.game.base.ILevel;
	import com.sajakfarki.biopets.game.scenes.GameScene;
	import com.sajakfarki.biopets.game.scenes.panels.Level11_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.Level14_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.LevelCompletePanel;
	import com.sajakfarki.biopets.graphics.text.NumberView;
	import com.sajakfarki.biopets.graphics.text.PercentView;
	import com.sajakfarki.biopets.graphics.types.BacteriaTypes;
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	import com.sajakfarki.biopets.graphics.types.CompoundTypes;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	import com.sajakfarki.biopets.graphics.types.TextTypes;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.*;
	import com.sajakfarki.biopets.utils.VariableParticleSystem2D;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Quad2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	import de.nulldesign.nd2d.materials.Quad2DColorMaterial;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	import de.nulldesign.nd2d.utils.ParticleSystemPreset;
	
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	import org.osflash.signals.Signal;
	
	public class Level_1_4 extends DefaultGame implements ILevel
	{
		
		[Inject]
		public var levelCreator : LevelCreator;
		
		[Inject]
		public var stageState : StageState;
		
		/** Background texture */
		[Embed(source = "../media/backgrounds/level1stage4.jpg")]
		private var _bgTexture:Class;
		
		[Embed(source = "../media/titles/stage_level_panel.png")]
		private var _titleTexture:Class;
		
		[Embed(source = "../media/titles/level1.png")]
		private var _levelTexture:Class;
		
		[Embed(source = "../media/titles/stage4.png")]
		private var _stageTexture:Class;
				
		private var _levelComplete : LevelCompletePanel;
		private var _infoPanel:Level14_Panel;
		
		
		private var _unlock:Boolean;
		
		private var _shade:Quad2D;
		
		[Embed(source = "../media/level_complete/t_youHaveUnlocked.png")]
		private var _unlockedTexture:Class;
		private var _unlocked:Sprite2D;
		
		[Embed(source = "../media/level_complete/veh_1.png")]
		private var _carTexture:Class;
		private var _car:Sprite2D;
		
		private var _cardTexture:Class = Cards.CARD1;
		private var _card:Sprite2D;
				
		
		public function Level_1_4(contextView:Scene2D, gb:GameBatches, w:Number, h:Number, gemsPerCompound:int)
		{
			var bgT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgTexture()).bitmapData);
			var bgS:Sprite2D = new Sprite2D(bgT);
			
			var tT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _titleTexture()).bitmapData);
			var titleSprite:Sprite2D = new Sprite2D(tT);
			
			var lT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _levelTexture()).bitmapData);
			var lS:Sprite2D = new Sprite2D(lT);
			
			var sT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _stageTexture()).bitmapData);
			var sS:Sprite2D = new Sprite2D(sT);
			
			lS.x = -80;
			sS.x = 80;
			sS.y = -13
				
			titleSprite.addChild(lS);
			titleSprite.addChild(sS);
			
			_infoPanel = new Level14_Panel();
			
			super(contextView,
						gb, 
						w, 
						h,
						bgS,
						titleSprite,
						_infoPanel,
						gemsPerCompound);
			
			stageS = stageState;
			
			init();
			
			_shade = new Quad2D(w, h);
			_shade.topLeftColor = _shade.topRightColor = _shade.bottomRightColor = _shade.bottomLeftColor = 0x99000000;
			_shade.alpha = 0;
			_shade.x = w/2;
			_shade.y = h;
			_shade.scaleX = 0.01;
			_shade.scaleY = 0.01;
			
			contextView.addChildAt(_shade, contextView.numChildren - GameScene.CHILDREN+2);
			
			contextView.addChildAt(titleSprite, contextView.numChildren - GameScene.CHILDREN+2);
			contextView.addChildAt(infoPanel as Node2D, contextView.numChildren - GameScene.CHILDREN+2);
			
			_levelComplete = new LevelCompletePanel();
			_levelComplete.alpha = 0.1;
			_levelComplete.x = w/2;
			_levelComplete.y = h + LevelCompletePanel.PANEL_HEIGHT - 1;
			
			contextView.addChildAt(_levelComplete, contextView.numChildren);
			
			var ut:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _unlockedTexture()).bitmapData);
			_unlocked = new Sprite2D(ut);
			_unlocked.x = 40;
			_unlocked.y = 34;
			_unlocked.scaleX = 0.01;
			_unlocked.scaleY = 0.01;
			_unlocked.alpha = 0.01;
			contextView.addChildAt(_unlocked, contextView.numChildren);
			
			var cd:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _cardTexture()).bitmapData);
			_card = new Sprite2D(cd);
			_card.x = 40;
			_card.y = 34;
			_card.scaleX = 0.01;
			_card.scaleY = 0.01;
			_card.alpha = 0.01;
			contextView.addChildAt(_card, contextView.numChildren);
			
			var cr:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _carTexture()).bitmapData);
			_car = new Sprite2D(cr);
			_car.x = 40;
			_car.y = 34;
			_car.scaleX = 0.01;
			_car.scaleY = 0.01;
			_car.alpha = 0.01;
			contextView.addChildAt(_car, contextView.numChildren);
			
			_levelComplete.retry.add(retry.dispatch);
			_levelComplete.map.add(levelFinished.dispatch);
			_levelComplete.unlock.add(unlock.dispatch);
			
		}
		
		/**************************************************
		 * 
		 * 	Show Level Complete
		 * 
		 **************************************************/
		public function showLevelComplete(tweenTime:Number, delay:Number, unlock:Boolean):void
		{
			_unlock = unlock;
			
			
			_levelComplete.setPercent(GameState.gemsCollected/GameState.totalGemsPossible*100);
			_levelComplete.setNumber(GameState.btuMultiplier * GameState.gemsCollected, levelTime);
			
			
			TweenMax.delayedCall(0.95, showShade);
			TweenMax.to(_shade, 1, {delay:1, alpha:1, scaleX:1, scaleY:1, ease:Quint.easeOut	});
			
//			_shade.alpha = 1;
			
			
			if(unlock){
				
				_unlocked.y = - 150
				_unlocked.x = screenWidth/2;
				_unlocked.alpha = 1;
				_unlocked.scaleX = 1;
				_unlocked.scaleY = 1;
								
				TweenMax.to(_unlocked, 0.5, {delay:1.5, y:100, ease:Back.easeOut});
				
				TweenMax.delayedCall(1.85, showCar);
				TweenMax.to(_car, 0.5, {delay:1.9, rotation:0, scaleX:1, scaleY:1, ease:Back.easeOut});
								
				
				TweenMax.to(_car, 0.6, {delay:6, rotation:160, scaleX:0.05, scaleY:0.05, ease:Back.easeIn, onComplete:function():void
				{
					_car.alpha = 0;				
				}});
				
				TweenMax.to(_card, 0.6, {delay:6.5, rotation:0, scaleX:0.8, scaleY:0.8, ease:Back.easeOut});
				
				
				TweenMax.to(_unlocked, 0.5, {delay:10, y:-100, ease:Back.easeIn});
				
				TweenMax.to(_card, 0.9, {delay:10.4, x:521, y:545, rotation:345, scaleX:0.4, scaleY:0.4, ease:Back.easeInOut});
				
			}
			
			_levelComplete.alpha = 1;
			TweenMax.to(_levelComplete, tweenTime, {delay:delay, y:screenHeight/2+LevelCompletePanel.OFFSET, ease:Quint.easeInOut, onComplete:showScore});
//			TweenMax.to(_levelComplete, tweenTime, {delay:delay, y:screenHeight/2+LevelCompletePanel.OFFSET, ease:Quint.easeInOut, onComplete:showScore});
			
		}
		
		private function showShade():void
		{
			_shade.y = screenHeight/2;
		}
			
		private function showCar():void
		{
			_car.rotation = -120;
			_car.y = screenHeight/2;
			_car.x = screenWidth/2;
			_car.alpha = 1;
			
			_card.alpha = 1;
			_card.rotation = -120;
			_card.y = screenHeight/2;
			_card.x = screenWidth/2;
		}
		
		private function showScore():void
		{
//			_percentView.alpha =1;
//			_btuView.alpha = 1;
			
			_levelComplete.showButtons(_unlock);
//			_percentView.show();
//			_btuView.show();
			
		}
		
		public function showStageComplete(tweenTime:Number, delay:Number):void{}
		
		public function get nextStage():NextStage
		{
			return null;
		}
		
		public function get initialized():LevelInitialized
		{
			return stageState.levelInitialized;
		}
		
		public function get stageFinished():StageFinished
		{
			return stageState.stageFinished;
		}
		
		override protected function startUp() : void
		{
			injector.injectInto( this );
			
			/** Stage variables */
			stageState.width = contextView.stage.stageWidth;
			stageState.height = contextView.stage.stageHeight;
			
			stageState.stageType = StageTypes.METHANOGENESIS
			stageState.bacteriaType = BacteriaTypes.SABBY
			
			stageState.timeLimit = 30
			stageState.maxBacteriaForce = 3
			stageState.maxBacteriaSpeed = 3
			stageState.currentHeat = 60
			stageState.maxHeatOptimal  = 81
			stageState.minHeatOptimal = 46
			stageState.coolRate = 1.59
			stageState.heatRate = 20
			stageState.rateOfReaction = 561
			stageState.pooPostion.push(new Point(809 , 188));
			
			stageState.bacteriaPostion.push(new Point(128 , 500),
				new Point(219 , 664),
				new Point(118 , 616));
			
			stageState.stickyPosition.push({pos:new Point(451 , 628), scale:0.32},
				{pos:new Point(574 , 495), scale:0.16},
				{pos:new Point(676 , 411), scale:0.21},
				{pos:new Point(513 , 120), scale:0.23},
				{pos:new Point(303 , 230), scale:0.24},
				{pos:new Point(72 , 305), scale:0.61});
			
			stageState.killerPosition.push({pos:new Point(390 , 145), scale:0.26},
				{pos:new Point(759 , 358), scale:0.25},
				{pos:new Point(656 , 495), scale:0.19},
				{pos:new Point(241 , 334), scale:0.23},
				{pos:new Point(625 , 22), scale:0.43},
				{pos:new Point(520 , 548), scale:0.24});
			
			super.startUp();
			
			/** Add Systems */
			addSystems();
			
		}
		
		override protected function mapInjectors() : void
		{
			super.mapInjectors();
			
			injector.mapSingleton( LevelCreator );
			injector.mapSingleton( StageState );
			injector.mapSingleton( PreUpdate );
			injector.mapSingleton( Update );
			injector.mapSingleton( Move );
			injector.mapSingleton( ResolveCollisions );
			injector.mapSingleton( Render );
			
			//injector.mapValue( KeyPoll, new KeyPoll( contextView.stage ) );
			
			injector.injectInto( this );
			
		}
		
		public function addSystems() : void
		{
			systemManager.addSystem( GameManager );
			
			systemManager.addSystem( TapSystem );
			systemManager.addSystem( HeatControlSystem );
			systemManager.addSystem( PooSystem );
			
			systemManager.addSystem( OrbitSystem );
			systemManager.addSystem( MotionSystem );
			
			systemManager.addSystem( CollisionSystem );
			systemManager.addSystem( RenderSystem );
			systemManager.addSystem( ProcessManager );			
		}
		
		public function finish():void
		{
			systemManager.removeSystem(PooSystem);
			systemManager.removeSystem(OrbitSystem);
			systemManager.removeSystem(HeatControlSystem);
		}
		
		public function disposeLevel():void
		{
			dispose();
		}
	}
}