package com.sajakfarki.biopets.game.levels
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.GameBatches;
	import com.sajakfarki.biopets.game.LevelCreator;
	import com.sajakfarki.biopets.game.base.DefaultGame;
	import com.sajakfarki.biopets.game.base.ILevel;
	import com.sajakfarki.biopets.game.scenes.GameScene;
	import com.sajakfarki.biopets.game.scenes.panels.Level11_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.Level14_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.LevelCompletePanel;
	import com.sajakfarki.biopets.graphics.text.NumberView;
	import com.sajakfarki.biopets.graphics.text.PercentView;
	import com.sajakfarki.biopets.graphics.types.BacteriaTypes;
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	import com.sajakfarki.biopets.graphics.types.CompoundTypes;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	import com.sajakfarki.biopets.graphics.types.TextTypes;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.*;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Quad2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	import org.osflash.signals.Signal;
	
	public class Level_4_4 extends DefaultGame implements ILevel
	{
		
		[Inject]
		public var levelCreator : LevelCreator;
		
		[Inject]
		public var stageState : StageState;
		
		/** Background texture */
		[Embed(source = "../media/backgrounds/level4stage4.jpg")]
		private var _bgTexture:Class;
		
		[Embed(source = "../media/titles/stage_level_panel.png")]
		private var _titleTexture:Class;
		
		[Embed(source = "../media/titles/level4.png")]
		private var _levelTexture:Class;
		
		[Embed(source = "../media/titles/stage4.png")]
		private var _stageTexture:Class;
		
		
//		[Embed( "../media/text/brownText.png" )]
//		private var _textAtlasBitmap:Class;
//		[Embed( "../media/text/brownText.plist", mimeType = "application/octet-stream" )]
//		private var _textAtlasXML:Class;
//		
//		private var _textSpriteBatch:Sprite2DBatch;
//		
		
		
		
		
		private var _levelComplete : LevelCompletePanel;
		private var _infoPanel:Level14_Panel;
		
		
//		private var _percentView:PercentView;
//		private var _btuView:NumberView;
		private var _unlock:Boolean;
		private var _shade:Quad2D;
		
		[Embed(source = "../media/level_complete/t_youHaveUnlocked.png")]
		private var _unlockedTexture:Class;
		private var _unlocked:Sprite2D;
		
		[Embed(source = "../media/level_complete/veh_4.png")]
		private var _carTexture:Class;
		private var _car:Sprite2D;
		
		private var _cardTexture:Class = Cards.CARD4;
		private var _card:Sprite2D;		
		
		public function Level_4_4(contextView:Scene2D, gb:GameBatches, w:Number, h:Number, gemsPerCompound:int)
		{
			var bgT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgTexture()).bitmapData);
			var bgS:Sprite2D = new Sprite2D(bgT);
			
			var tT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _titleTexture()).bitmapData);
			var titleSprite:Sprite2D = new Sprite2D(tT);
			
			var lT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _levelTexture()).bitmapData);
			var lS:Sprite2D = new Sprite2D(lT);
			
			var sT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _stageTexture()).bitmapData);
			var sS:Sprite2D = new Sprite2D(sT);
			
			lS.x = -80;
			sS.x = 80;
			sS.y = -13
				
			titleSprite.addChild(lS);
			titleSprite.addChild(sS);
			
			_infoPanel = new Level14_Panel();
			
			super(contextView,
						gb, 
						w, 
						h,
						bgS,
						titleSprite,
						_infoPanel,
						gemsPerCompound);
			
			stageS = stageState;
			
			init();
			
			_shade = new Quad2D(w, h);
			_shade.topLeftColor = _shade.topRightColor = _shade.bottomRightColor = _shade.bottomLeftColor = 0x99000000;
			_shade.alpha = 0;
			_shade.x = w/2;
			_shade.y = h;
			_shade.scaleX = 0.01;
			_shade.scaleY = 0.01;
			
			contextView.addChildAt(_shade, contextView.numChildren - GameScene.CHILDREN+1);
			
			contextView.addChildAt(titleSprite, contextView.numChildren - GameScene.CHILDREN+1);
			contextView.addChildAt(infoPanel as Node2D, contextView.numChildren - GameScene.CHILDREN+1);
			
			_levelComplete = new LevelCompletePanel();
			_levelComplete.alpha = 0.1;
			_levelComplete.x = w/2;
			_levelComplete.y = h + LevelCompletePanel.PANEL_HEIGHT - 1;
			
			contextView.addChildAt(_levelComplete, contextView.numChildren);
			
			var ut:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _unlockedTexture()).bitmapData);
			_unlocked = new Sprite2D(ut);
			_unlocked.x = 40;
			_unlocked.y = 34;
			_unlocked.scaleX = 0.01;
			_unlocked.scaleY = 0.01;
			_unlocked.alpha = 0.01;
			contextView.addChildAt(_unlocked, contextView.numChildren);
			
			var cd:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _cardTexture()).bitmapData);
			_card = new Sprite2D(cd);
			_card.x = 40;
			_card.y = 34;
			_card.scaleX = 0.01;
			_card.scaleY = 0.01;
			_card.alpha = 0.01;
			contextView.addChildAt(_card, contextView.numChildren);
			
			var cr:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _carTexture()).bitmapData);
			_car = new Sprite2D(cr);
			_car.x = 40;
			_car.y = 34;
			_car.scaleX = 0.01;
			_car.scaleY = 0.01;
			_car.alpha = 0.01;
			contextView.addChildAt(_car, contextView.numChildren);
			
			_levelComplete.retry.add(retry.dispatch);
			_levelComplete.map.add(levelFinished.dispatch);
			_levelComplete.unlock.add(unlock.dispatch);
			
		}
		
		/**************************************************
		 * 
		 * 	Show Level Complete
		 * 
		 **************************************************/
		public function showLevelComplete(tweenTime:Number, delay:Number, unlock:Boolean):void
		{
			_unlock = unlock;
			
			
			_levelComplete.setPercent(GameState.gemsCollected/GameState.totalGemsPossible*100);
			_levelComplete.setNumber(GameState.btuMultiplier * GameState.gemsCollected, levelTime);
			
			
			TweenMax.delayedCall(0.95, showShade);
			TweenMax.to(_shade, 1, {delay:1, alpha:1, scaleX:1, scaleY:1, ease:Quint.easeOut	});
			
			//			_shade.alpha = 1;
			
			
			if(unlock){
				
				_unlocked.y = - 150
				_unlocked.x = screenWidth/2;
				_unlocked.alpha = 1;
				_unlocked.scaleX = 1;
				_unlocked.scaleY = 1;
				
				TweenMax.to(_unlocked, 0.5, {delay:1.5, y:100, ease:Back.easeOut});
				
				TweenMax.delayedCall(1.85, showCar);
				TweenMax.to(_car, 0.5, {delay:1.9, rotation:0, scaleX:1, scaleY:1, ease:Back.easeOut});
				
				
				TweenMax.to(_car, 0.6, {delay:6, rotation:160, scaleX:0.05, scaleY:0.05, ease:Back.easeIn, onComplete:function():void
				{
					_car.alpha = 0;				
				}});
				
				TweenMax.to(_card, 0.6, {delay:6.5, rotation:0, scaleX:0.8, scaleY:0.8, ease:Back.easeOut});
				
				
				TweenMax.to(_unlocked, 0.5, {delay:10, y:-100, ease:Back.easeIn});
				
				TweenMax.to(_card, 0.9, {delay:10.4, x:521, y:545, rotation:345, scaleX:0.4, scaleY:0.4, ease:Back.easeInOut});
				
			}
			
			_levelComplete.alpha = 1;
			TweenMax.to(_levelComplete, tweenTime, {delay:delay, y:screenHeight/2+LevelCompletePanel.OFFSET, ease:Quint.easeInOut, onComplete:showScore});
			//			TweenMax.to(_levelComplete, tweenTime, {delay:delay, y:screenHeight/2+LevelCompletePanel.OFFSET, ease:Quint.easeInOut, onComplete:showScore});
			
		}
		
		private function showShade():void
		{
			_shade.y = screenHeight/2;
		}
		
		private function showCar():void
		{
			_car.rotation = -120;
			_car.y = screenHeight/2;
			_car.x = screenWidth/2;
			_car.alpha = 1;
			
			_card.alpha = 1;
			_card.rotation = -120;
			_card.y = screenHeight/2;
			_card.x = screenWidth/2;
		}
		
		private function showScore():void
		{
			_levelComplete.showButtons(_unlock);
//			_percentView.show();
//			_btuView.show();
			
		}
		
		public function showStageComplete(tweenTime:Number, delay:Number):void{}
		
		public function get nextStage():NextStage
		{
			return null;
		}
		
		public function get initialized():LevelInitialized
		{
			return stageState.levelInitialized;
		}
		
		public function get stageFinished():StageFinished
		{
			return stageState.stageFinished;
		}

		override protected function startUp() : void
		{
			injector.injectInto( this );
			
			//stageState.hud.add(dispatchHUD);
			/** Stage variables */
			stageState.width = contextView.stage.stageWidth;
			stageState.height = contextView.stage.stageHeight;
			
			
			stageState.stageType = StageTypes.METHANOGENESIS
			stageState.bacteriaType = BacteriaTypes.SABBY
			stageState.timeLimit = 30
			stageState.maxBacteriaForce = 3
			stageState.maxBacteriaSpeed = 3
			stageState.currentHeat = 66
			stageState.maxHeatOptimal  = 76
			stageState.minHeatOptimal = 52
			stageState.coolRate = 2.77
			stageState.heatRate = 20
			stageState.rateOfReaction = 495
			stageState.pooPostion.push(new Point(91 , 666),
				new Point(823 , 609),
				new Point(538 , 142));
			
			stageState.bacteriaPostion.push(new Point(88 , 426),
				new Point(908 , 125),
				new Point(975 , 178),
				new Point(965 , 84),
				new Point(853 , 83));
			
			stageState.stickyPosition.push({pos:new Point(469 , 533), scale:0.37},
				{pos:new Point(339 , 550), scale:0.5},
				{pos:new Point(272 , 690), scale:0.45},
				{pos:new Point(457 , 693), scale:0.78},
				{pos:new Point(667 , 730), scale:0.59},
				{pos:new Point(821 , 764), scale:0.31},
				{pos:new Point(986 , 335), scale:0.4},
				{pos:new Point(970 , 470), scale:0.58},
				{pos:new Point(994 , 614), scale:0.39},
				{pos:new Point(950 , 729), scale:0.4},
				{pos:new Point(239 , 305), scale:0.37},
				{pos:new Point(175 , 57), scale:0.5},
				{pos:new Point(332 , 195), scale:0.63},
				{pos:new Point(736 , 266), scale:0.19},
				{pos:new Point(747 , 199), scale:0.22},
				{pos:new Point(696 , 127), scale:0.32},
				{pos:new Point(670 , 8), scale:0.53},
				{pos:new Point(500 , -6), scale:0.3},
				{pos:new Point(356 , 23), scale:0.64});
			
			stageState.killerPosition.push({pos:new Point(229 , 604), scale:0.26},
				{pos:new Point(595 , 596), scale:0.23},
				{pos:new Point(344 , 313), scale:0.25},
				{pos:new Point(192 , 159), scale:0.23});
			
			
			super.startUp();
			
			/** Add Systems */
			addSystems();
			
		}
		
		override protected function mapInjectors() : void
		{
			super.mapInjectors();
			
			injector.mapSingleton( LevelCreator );
			injector.mapSingleton( StageState );
			injector.mapSingleton( PreUpdate );
			injector.mapSingleton( Update );
			injector.mapSingleton( Move );
			injector.mapSingleton( ResolveCollisions );
			injector.mapSingleton( Render );
			
			//injector.mapValue( KeyPoll, new KeyPoll( contextView.stage ) );
			
			injector.injectInto( this );
			
		}
		
		public function addSystems() : void
		{
			systemManager.addSystem( GameManager );
			
			systemManager.addSystem( TapSystem );
			systemManager.addSystem( HeatControlSystem );
			systemManager.addSystem( PooSystem );
			
			systemManager.addSystem( OrbitSystem );
			systemManager.addSystem( MotionSystem );
			
			systemManager.addSystem( CollisionSystem );
			systemManager.addSystem( RenderSystem );
			systemManager.addSystem( ProcessManager );			
		}
		
		public function finish():void
		{
			systemManager.removeSystem(PooSystem);
			systemManager.removeSystem(OrbitSystem);
			systemManager.removeSystem(HeatControlSystem);
		}
		
		public function disposeLevel():void
		{
			dispose();
		}
	}
}