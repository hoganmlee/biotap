package com.sajakfarki.biopets.game.levels
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.GameBatches;
	import com.sajakfarki.biopets.game.LevelCreator;
	import com.sajakfarki.biopets.game.base.DefaultGame;
	import com.sajakfarki.biopets.game.base.ILevel;
	import com.sajakfarki.biopets.game.scenes.GameScene;
	import com.sajakfarki.biopets.game.scenes.panels.Level11_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.Level12_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.Level22_Panel;
	import com.sajakfarki.biopets.game.scenes.panels.StageCompletePanel;
	import com.sajakfarki.biopets.graphics.types.BacteriaTypes;
	import com.sajakfarki.biopets.graphics.types.BadGuyTypes;
	import com.sajakfarki.biopets.graphics.types.CompoundTypes;
	import com.sajakfarki.biopets.graphics.types.StageTypes;
	import com.sajakfarki.biopets.signals.*;
	import com.sajakfarki.biopets.systems.*;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	import de.nulldesign.nd2d.materials.texture.TextureAtlas;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	import org.osflash.signals.Signal;
	
	public class Level_2_2 extends DefaultGame implements ILevel
	{
		[Inject]
		public var levelCreator : LevelCreator;
		
		[Inject]
		public var stageState : StageState;
		
		private var nextStageSignal:NextStage = new NextStage();
		
		/** Background texture */
		[Embed(source = "../media/backgrounds/level2stage2.jpg")]
		private var _bgTexture:Class;
		
		[Embed(source = "../media/titles/stage_level_panel.png")]
		private var _titleTexture:Class;
		
		[Embed(source = "../media/titles/level2.png")]
		private var _levelTexture:Class;
		
		[Embed(source = "../media/titles/stage2.png")]
		private var _stageTexture:Class;
		
		private var _infoPanel:Level22_Panel;
		private var _stageComplete:StageCompletePanel;
		
		
		public function Level_2_2(contextView:Scene2D, gb:GameBatches, w:Number, h:Number, gemsPerCompound:int)
		{
			var bgT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _bgTexture()).bitmapData);
			var bgS:Sprite2D = new Sprite2D(bgT);
			
			var tT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _titleTexture()).bitmapData);
			var titleSprite:Sprite2D = new Sprite2D(tT);
			
			var lT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _levelTexture()).bitmapData);
			var lS:Sprite2D = new Sprite2D(lT);
			
			var sT:Texture2D = Texture2D.textureFromBitmapData(Bitmap(new _stageTexture()).bitmapData);
			var sS:Sprite2D = new Sprite2D(sT);
			
			lS.x = -80;
			sS.x = 80;
			sS.y = -13
			
			titleSprite.addChild(lS);
			titleSprite.addChild(sS);
	
			_infoPanel = new Level22_Panel();
			
			super(contextView, 
				gb,
				w, 
				h,
				bgS,
				titleSprite,
				_infoPanel,
				gemsPerCompound);
			
			stageS = stageState;
			
			init();
			
			contextView.addChildAt(titleSprite, contextView.numChildren - GameScene.CHILDREN+2);
			contextView.addChildAt(infoPanel as Node2D, contextView.numChildren - GameScene.CHILDREN+2);
			
			/** Show Stage Screen */
			_stageComplete = new StageCompletePanel();
			_stageComplete.x = w/2;
			_stageComplete.y = h/2;
			_stageComplete.visible = false;
			_stageComplete.alpha = 0;
			_stageComplete.scaleX = 0.2;
			_stageComplete.scaleY = 0.2;
			
			contextView.addChildAt(_stageComplete, contextView.numChildren);
			
		}
		
		/**************************************************
		 * 
		 * 	Show Level Complete
		 * 
		 **************************************************/	
		public function showLevelComplete(tweenTime:Number, delay:Number, unlock:Boolean):void{}
		
		public function showStageComplete(tweenTime:Number, delay:Number):void
		{
			trace("show stage complete");
			_stageComplete.visible = true;
			TweenMax.to(_stageComplete, tweenTime, {delay:delay, scaleX:1, scaleY:1, alpha:1, ease:Back.easeOut});
			
			_stageComplete.mouseEnabled = true;
			_stageComplete.addEventListener(MouseEvent.CLICK, gotoNextStage);
		}
		
		private function gotoNextStage(event:MouseEvent):void
		{
			_stageComplete.removeEventListener(MouseEvent.CLICK, gotoNextStage);
			nextStageSignal.dispatch();
		}		
		
		public function get initialized():LevelInitialized
		{
			return stageState.levelInitialized;
		}
		
		public function get nextStage():NextStage
		{
			return nextStageSignal;
		}
		
		public function get stageFinished():StageFinished
		{
			return stageState.stageFinished;
		}
		
		override protected function startUp() : void
		{
			/** Stage variables */
			stageState.width = contextView.stage.stageWidth;
			stageState.height = contextView.stage.stageHeight;
			
						
			stageState.stageType = StageTypes.ACIDOGENESIS
			stageState.bacteriaType = BacteriaTypes.CLOSTER
			stageState.timeLimit = 30
			stageState.maxBacteriaForce = 3
			stageState.maxBacteriaSpeed = 3
			stageState.currentHeat = 60
			stageState.maxHeatOptimal  = 82
			stageState.minHeatOptimal = 48
			stageState.coolRate = 1.8
			stageState.heatRate = 20
			stageState.rateOfReaction = 561
			stageState.pooPostion.push(new Point(172 , 616),
				new Point(843 , 159));
			
			stageState.bacteriaPostion.push(new Point(78 , 309),
				new Point(108 , 213),
				new Point(374 , 82),
				new Point(975 , 529),
				new Point(829 , 627));
			
			stageState.stickyPosition.push({pos:new Point(551 , 634), scale:0.56},
				{pos:new Point(904 , 350), scale:0.46},
				{pos:new Point(973 , 260), scale:0.19},
				{pos:new Point(813 , 420), scale:0.17},
				{pos:new Point(713 , 61), scale:0.15},
				{pos:new Point(606 , 138), scale:0.52},
				{pos:new Point(331 , 304), scale:0.35},
				{pos:new Point(260 , 372), scale:0.18},
				{pos:new Point(174 , 430), scale:0.35},
				{pos:new Point(56 , 453), scale:0.17});
			
			stageState.killerPosition.push({pos:new Point(427 , 669), scale:0.19},
				{pos:new Point(475 , 64), scale:0.19});
			
			
			
			super.startUp();
			
			/** Add Systems */
			addSystems();
			
		}
		
		override protected function mapInjectors() : void
		{
			super.mapInjectors();
			
			injector.mapSingleton( LevelCreator );
			injector.mapSingleton( StageState );
			injector.mapSingleton( PreUpdate );
			injector.mapSingleton( Update );
			injector.mapSingleton( Move );
			injector.mapSingleton( ResolveCollisions );
			injector.mapSingleton( Render );
			
			//injector.mapValue( KeyPoll, new KeyPoll( contextView.stage ) );
			
			injector.injectInto( this );
			
		}
		
		public function addSystems() : void
		{
			systemManager.addSystem( GameManager );
			
			systemManager.addSystem( TapSystem );
			systemManager.addSystem( HeatControlSystem );
			systemManager.addSystem( PooSystem );
			
			systemManager.addSystem( OrbitSystem );
			systemManager.addSystem( MotionSystem );
			
			systemManager.addSystem( CollisionSystem );
			systemManager.addSystem( RenderSystem );
			systemManager.addSystem( ProcessManager );			
		}
		
		public function finish():void
		{
			systemManager.removeSystem(PooSystem);
			systemManager.removeSystem(OrbitSystem);
			systemManager.removeSystem(HeatControlSystem);
		}
		
		public function disposeLevel():void
		{
			dispose();
		}
	}
}