package com.sajakfarki.biopets.game
{
	
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.scenes.GameScene;
	import com.sajakfarki.biopets.graphics.*;
	import com.sajakfarki.biopets.graphics.bacteria.*;
	import com.sajakfarki.biopets.graphics.compounds.*;
	import com.sajakfarki.biopets.graphics.compounds.base.DefaultPooView;
	import com.sajakfarki.biopets.graphics.compounds.base.KillerPoo;
	import com.sajakfarki.biopets.graphics.compounds.base.StickyPoo;
	import com.sajakfarki.biopets.graphics.controls.*;
	import com.sajakfarki.biopets.graphics.types.*;
	import com.sajakfarki.biopets.utils.FastRandom;
	import com.tomseysdavies.ember.core.IEntity;
	import com.tomseysdavies.ember.core.IEntityManager;
	
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.ui.Keyboard;
	
	public class LevelCreator
	{
		[Inject]
		public var entityManager : IEntityManager;
		
		[Inject]
		public var gameBatch:GameBatches;
		
						
		public function destroyEntity( entityId : String ) : void
		{
			trace("destroy entityId   = " + entityId);
			
			if ( !entityManager.hasEntity( entityId ) )
			{
				return;
			}
			if ( entityManager.hasComponent( entityId, Display ) )
			{
				var display : Display = entityManager.getComponent( entityId, Display );
				display.displayObject.parent.removeChild( display.displayObject );
			}
			entityManager.removeEntity( entityId );
		}
		
		
		/************************************************************
		 * 
		 *   Create Poop !
		 *   TODO: Add Poo Type
		 * 
		 ************************************************************/
		public function createPoo( type:String ,
								   				pooStage:String,
												id : String ,
												gems :  int ,  
												x : Number , 
												y : Number,
												minRadius : Number ,
												maxRadius : Number ,
												rateOfRadiusExpand : Number ,
												rateOfRadiusCollapse : Number ,
												rateOfPop : Number ,
												popTime : Number ,
												rateOfReaction : Number ,
												startTime : Number ,
												minBacteria : int ,
												maxBacteria : int
												) : IEntity
		{
			
//			trace("numGems = "+ gems);
//			trace("gameBatch = " + gameBatch);
			
			var poo : IEntity = entityManager.createEntity();
			
			var newPoo : Poo = new Poo();
			newPoo.ID = id;
			newPoo.minRadius = minRadius;
			newPoo.maxRadius = maxRadius;
			
			//newPoo.numberOfGems = gems;
			
			newPoo.rateOfRadiusExpand = rateOfRadiusExpand;
			newPoo.rateOfRadiusCollapse = rateOfRadiusCollapse;
			
			newPoo.rateOfPop = rateOfPop;
			newPoo.popTime = popTime;
			newPoo.popCount = 0;
			newPoo.rateOfReaction = rateOfReaction;

		//	
		//	newPoo.startTime = startTime;
		//	newPoo.reactionTime = startTime;
		//	
			
			newPoo.minBacteria = minBacteria;
			newPoo.maxBacteria = maxBacteria;
			
			newPoo.collectionCount = 0;
			newPoo.badCount = 0;
						
			poo.addComponent( newPoo );
			
			var position : Position = new Position();
			position.numberOfGems = gems;
			position.startTime = startTime;
			position.reactionTime = startTime;
			position.point.x = x;
			position.point.y = y;
			//position.collisionRadius = minRadius;
			position.collisionRadius = maxRadius;
			position.minBacteria = minBacteria;
			poo.addComponent( position );
			
			var orbit : Orbit = new Orbit();
			orbit.angularVelocity = FastRandom.getRandom() * 2 - 1;
			orbit.velocity.x = ( FastRandom.getRandom() - 0.5 ) * 1 * ( 10 );
			orbit.velocity.y = ( FastRandom.getRandom() - 0.5 ) * 1 * ( 10 );
			poo.addComponent( orbit );
			
			var display : Display = new Display();
//			var pooType : String;
//			var counterType : String;
//			var optimalType : String;
			
//			switch(type){
//				
//				case CompoundTypes.POO:
//					pooType = CompoundTypes.POO_ASSET;
////					counterType = CompoundTypes.POO_COUNTER;
////					optimalType = CompoundTypes.POO_OPTIMAL;
//					break;
//				
//				case CompoundTypes.
//				
//				//				case CompoundTypes.AMINO:
//				//					display.displayObject = new AminoAcidView( minRadius, maxRadius, maxBacteria );
//				//					break;
//				//				case CompoundTypes.KID:
//				//					display.displayObject = new KidView( minRadius, maxRadius, maxBacteria );
//				//					break;
//			}
						
			display.displayObject = new DefaultPooView(gameBatch.batch[GameScene.GAME_BATCH], minRadius, maxRadius, maxBacteria, gems, x , y, type, pooStage);
			
			poo.addComponent( display );
			return poo;
			
		}
		
		/************************************************************
		 * 
		 *   Create Bacteria !
		 *   TODO: Add Bacteria Type
		 * 
		 ************************************************************/
		public function createBacteria( type: String,
													  radius : Number , 
													  x : Number , 
													  y : Number , 
													  maxSpeed : Number , 
													  maxForce : Number,
													  isBad : Boolean) : IEntity
		{
			var bacteria : IEntity = entityManager.createEntity();
			
			
			var newBacteria : Bacteria = new Bacteria();
			newBacteria.isBad = isBad;
			//newBacteria.
				
			bacteria.addComponent( newBacteria )
				
			var position : Position = new Position();
			position.point.x = x;
			position.point.y = y;
			position.collisionRadius = radius;
			bacteria.addComponent( position );
			
			var motion : Motion = new Motion();
			motion.maxSpeed = maxSpeed;
			motion.maxSpeedSQ = maxSpeed * maxSpeed;
			motion.maxForce = maxForce;
			motion.maxForceSQ = maxForce * maxForce;
			
			//Random Start Velocity
			motion.normal = motion.velocity = new Vector3D(FastRandom.randomRange(-2, 2), FastRandom.randomRange(-2, 2), 0.0);
						
			bacteria.addComponent( motion );
						
			var display : Display = new Display();
			
			switch(type){
				case BacteriaTypes.ARCHIE:
					display.displayObject = new ArchieBacteriaView( gameBatch.batch[GameScene.GAME_BATCH]);
					break;
				case BacteriaTypes.SABBY:
					display.displayObject = new SabbyBacteriaView( gameBatch.batch[GameScene.GAME_BATCH] );
					break;
				case BacteriaTypes.BAXTER:
					display.displayObject = new BaxterBacteriaView( gameBatch.batch[GameScene.GAME_BATCH] );
					break;
				case BacteriaTypes.CLOSTER:
					display.displayObject = new ClosterBacteriaView( gameBatch.batch[GameScene.GAME_BATCH] );
					break;
			}
				
			bacteria.addComponent( display );
			
			return bacteria;
			
		}
		
		/************************************************************
		 * 
		 *   Create Tap !
		 * 
		 ************************************************************/
		public function createTap( radius : Number, 
								   				x : Number , 
												y : Number ,
												tapLife : Number = 2.5) : IEntity
		{
			var tap : IEntity = entityManager.createEntity();
			
			var newTap : Tap = new Tap();
			newTap.collisionSize = radius;
			newTap.tapLifeRemaining = 0;
			newTap.tapLifetime = tapLife;
			
			tap.addComponent( newTap)
						
			var position : Position = new Position();
			position.point.x = x;
			position.point.y = y;
			position.collisionRadius = 0;
			tap.addComponent( position );
			
			var display : Display = new Display();
			display.displayObject = new TapView ( gameBatch.batch[GameScene.GAME_BATCH], radius );
			tap.addComponent( display );
			
			return tap;
			
		}

		/************************************************************
		 * 
		 *   Create Heat Controls !
		 * 
		 ************************************************************/
		public function createHeat( radius:int, 
												 x : Number , 
												 y : Number, 
												 max:Number, 
												 min :Number, 
												 current : Number,
												 maxOptimal:Number, 
												 minOptimal:Number,
												 heatStep : Number,
												 coolRate : Number,
												 heatRate : Number): IEntity
		{
			var heat : IEntity = entityManager.createEntity();
			
			var heatControl : HeatControl = new HeatControl();
			heatControl.max = max;
			heatControl.min = min;
			heatControl.optimalMax = maxOptimal;
			heatControl.optimalMin = minOptimal;
			heatControl.currentHeat = current;
			heatControl.heatStep = heatStep;
			heatControl.coolRate = coolRate;
			heatControl.heatRate = heatRate;
			
			heat.addComponent( heatControl);
			
			var position : Position = new Position();
			position.point.x = x;
			position.point.y = y;
			position.collisionRadius = radius;
			position.temperature = min;
			
			heat.addComponent(position);
			
			var display : Display = new Display();
			display.displayObject = new HeatView(100, max, min, maxOptimal, minOptimal);
			
			heat.addComponent(display);
			
			return heat;
			
		}
		
		/************************************************************
		 * 
		 *   Create BadGuy !
		 *   TODO: add type
		 * 
		 ************************************************************/
		public function createBadPoo( type:String,
									  				 scale:Number,
													  x : Number , 
												   	  y : Number ): IEntity
		{
			
			var badguy : IEntity = entityManager.createEntity();
			
			var bd:BadGuy = new BadGuy();
			bd.type = type;
			badguy.addComponent(bd);
						
			var position : Position = new Position();
			position.point.x = x;
			position.point.y = y;
			position.collisionRadius = scale * 120;
			
			badguy.addComponent(position);
			
			var display : Display = new Display();
			
			switch(type)
			{
				case BadGuyTypes.DOUCHEBAG:
					display.displayObject = new StickyPoo( gameBatch.batch[GameScene.GAME_BATCH] );
					break;
				case BadGuyTypes.JERK:
					display.displayObject = new KillerPoo( gameBatch.batch[GameScene.GAME_BATCH] );
					break;
			}
			
			display.displayObject.scaleX = scale;
			display.displayObject.scaleY = scale;
			
			badguy.addComponent(display);
			
			return badguy;
			
		}
		
		
		
	}
}