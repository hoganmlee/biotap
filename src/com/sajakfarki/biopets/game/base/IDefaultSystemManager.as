package com.sajakfarki.biopets.game.base
{
	import com.tomseysdavies.ember.core.IDisposable;

	public interface IDefaultSystemManager extends IDisposable
	{
		
		function resume():void;
		function pause():void;
			
		/**
		 * registers a system
		 * 
		 * @param System 
		 * 
		 */	
		function addSystem(System:Class):*;
		
		
		/**
		 * unregisters a system
		 * 
		 * @param system
		 * 
		 */	
		function removeSystem(System:Class):void;
	
	}
}