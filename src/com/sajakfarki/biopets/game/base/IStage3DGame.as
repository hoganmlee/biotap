package com.sajakfarki.biopets.game.base
{
	import com.tomseysdavies.ember.core.IDisposable;
	import de.nulldesign.nd2d.display.Scene2D;
	
	public interface IStage3DGame extends IDisposable
	{
		function set contextView(value:Scene2D):void;
		function get contextView():Scene2D;
	}
}