/*
* Copyright (c) 2010 Tom Davies
*
* Permission is hereby granted to use, modify, and distribute this file
* in accordance with the terms of the license agreement accompanying it.
*/

package com.sajakfarki.biopets.game.base{
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Quint;
	import com.sajakfarki.biopets.components.GameState;
	import com.sajakfarki.biopets.components.StageState;
	import com.sajakfarki.biopets.game.GameBatches;
	import com.sajakfarki.biopets.game.scenes.GameScene;
	import com.sajakfarki.biopets.game.scenes.panels.DefaultPanel;
	import com.sajakfarki.biopets.game.scenes.panels.FailPanel;
	import com.sajakfarki.biopets.game.scenes.panels.HeatWarningPanel;
	import com.sajakfarki.biopets.game.scenes.panels.IPanel;
	import com.sajakfarki.biopets.game.scenes.panels.LevelCompletePanel;
	import com.sajakfarki.biopets.game.scenes.panels.ProgressPanel;
	import com.sajakfarki.biopets.game.scenes.panels.ScorePanel;
	import com.sajakfarki.biopets.graphics.compounds.base.IPooView;
	import com.sajakfarki.biopets.graphics.compounds.base.StickyPoo;
	import com.sajakfarki.biopets.graphics.text.*;
	import com.sajakfarki.biopets.graphics.text.TimerView;
	import com.sajakfarki.biopets.graphics.types.TextTypes;
	import com.sajakfarki.biopets.signals.LevelFinished;
	import com.sajakfarki.biopets.signals.Retry;
	import com.sajakfarki.biopets.signals.Unlock;
	import com.tomseysdavies.ember.base.EntityManager;
	import com.tomseysdavies.ember.core.IEntityManager;
	
	import de.nulldesign.nd2d.display.Node2D;
	import de.nulldesign.nd2d.display.Scene2D;
	import de.nulldesign.nd2d.display.Sprite2D;
	import de.nulldesign.nd2d.display.Sprite2DBatch;
	import de.nulldesign.nd2d.materials.texture.Texture2D;
	
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.geom.Point;
	
	import org.flexunit.internals.runners.statements.Fail;
	import org.osflash.signals.Signal;
	import org.swiftsuspenders.Injector;
	
	public class DefaultGame implements IStage3DGame  {
		
		private var quitGame : LevelFinished = new LevelFinished();
		private var retryGame : Retry = new Retry();
		private var achieveSignal : Unlock = new Unlock();
		private var timesupSignal:Signal = new Signal();
		
		private var _gb : GameBatches;
		private var _contextView:Scene2D;
		private var _entityManager:IEntityManager;
		private var _systemManager:IDefaultSystemManager;
		private var _injector:Injector;
		
		protected var stageS:StageState;
		
		protected var screenWidth:Number;
		protected var screenHeight:Number;
		
		protected var bg: Sprite2D;
		protected var title:Sprite2D;
//		protected var score:ScorePanel;
		protected var infoPanel:IPanel;
		
		protected var heatWarning:HeatWarningPanel;
		protected var failPanel:FailPanel;
		
		private var _started:Boolean = false;
		
		private var _levelTimer:TimerViewNoBatch;
//		private var _gemCounter:GemCounterView;
//		private var _gemCount:int = 0;
		
		private var _progressPanel:ProgressPanel;
		
		private var _gemsPerCompound:int;
		
		private var _bacteriaCount:int = 99;
		
		
		/**
		 * Abstract IGame Implementation
		 *
		 * <p>Extend this class to create a Game</p>
		 */
		public function DefaultGame(ct:Scene2D, 
									gb:GameBatches,
									w:Number, 
									h:Number,
									bgSprite:Sprite2D, 
									titleSprite:Sprite2D, 
									panel:IPanel,
									gemsPerCompound:int){
		
			screenWidth = w;
			screenHeight = h;
			
			_contextView = ct;
			_gb = gb;
			
			_gemsPerCompound = gemsPerCompound;
			
			bg = bgSprite;
			bg.alpha = 0.1;
			bg.x = contextView.stage.stageWidth/2;
			bg.y = contextView.stage.stageHeight/2;
			
			_contextView.addChildAt(bg, 0);
			
//			/** score back */
//			score = new ScorePanel();
//			score.alpha = 0;
//			score.x = screenWidth/2;
//			score.y = ScorePanel.PANEL_HEIGHT;
//			
//			_contextView.addChildAt(score, _contextView.numChildren - GameScene.CHILDREN);
			//_contextView.addChild(score);
			
			/** gem counter */
//			_gemCounter = new GemCounterView();
//			_gemCounter.alpha = 0;
//			_gemCounter.x = screenWidth/2;
//			_gemCounter.y = 30;
//			_contextView.addChildAt(_gemCounter, _contextView.numChildren - GameScene.CHILDREN);
			
//			_levelTimer.alpha = 0;
//			_levelTimer.x = 150;
//			_levelTimer.y = 30;
//			_contextView.addChildAt(_levelTimer, _contextView.numChildren - GameScene.CHILDREN);
			
			//Progress Panel
			_progressPanel = new ProgressPanel();
			_progressPanel.x = screenWidth/2;
			_progressPanel.y = screenHeight + 96;
			
			_contextView.addChildAt(_progressPanel, _contextView.numChildren - GameScene.CHILDREN+1);
			
			_progressPanel.setStage(GameState.CURRENT_PHASE);
			
			//heat warning
			heatWarning = new HeatWarningPanel();
			heatWarning.alpha = 0.01;
			heatWarning.x = screenWidth/2;
			heatWarning.y = screenHeight/2;
			_contextView.addChildAt(heatWarning, _contextView.numChildren - GameScene.CHILDREN+1);
			
			
			/** title texture */
			title = titleSprite;
			title.x = screenWidth/2;
			title.y = screenHeight + title.height/2;
			
			//_contextView.addChild(title);
			

			
			
			/** info panel */
			infoPanel = panel;
			infoPanel.px = screenWidth/2;
			infoPanel.py = screenHeight + infoPanel.panelHeight;
			
			
			//_contextView.addChild(infoPanel as Node2D);
			

			/** level timer */
			_levelTimer = new TimerViewNoBatch();
			_levelTimer.x = 168;
			_levelTimer.y = 32;
			_levelTimer.timesup.add(endLevel);
			_levelTimer.alpha = 0.01;
			//_levelTimer.time = 666;
			_contextView.addChildAt(_levelTimer, _contextView.numChildren - GameScene.CHILDREN);
			
			
			//fail panel
			failPanel = new FailPanel();
			failPanel.x = screenWidth/2;
			failPanel.y = screenHeight/2 + 660;
//			failPanel.visible = false;
			failPanel.alpha = 0.01;
			failPanel.map.add(quitGame.dispatch);
			failPanel.retry.add(retryGame.dispatch);
			
			_contextView.addChildAt(failPanel, _contextView.numChildren - GameScene.CHILDREN);
			
			
			
			TweenMax.to(bg, 1, {alpha:1});
			//			TweenMax.to(score, 1, {alpha:1});
			TweenMax.delayedCall(0.8, showPanel);
			
			if(_contextView){
				mapInjectors();
				//TweenMax.delayedCall(3, startUp);
			}
			
			trace(" >>>> Level " + this);
			
		}
		
		public function get levelTime():int
		{
			return _levelTimer.time;
		}
		
		public function get bacteriaCount():int
		{
			return _bacteriaCount;
		}
		
		public function get unlock():Unlock
		{
			return achieveSignal;
		}
		
		public function get levelFinished():LevelFinished
		{
			return quitGame;
		}
		
		public function get retry():Retry
		{
			return retryGame;
		}
		
		public function get timesup():Signal
		{
			return timesupSignal;
		}
		
		public function init():void
		{
			stageS.gemsPerCompound = _gemsPerCompound;
			
			stageS.optimalTemperature.add(optTemp);
			stageS.gemCollected.add(collectGem);
			stageS.numberOfBacteria.add(countBacteria);
			
		}
		
		private function optTemp(b:Boolean):void
		{
			//trace(b);
			if(b){
				TweenMax.to(heatWarning, 0.5, {alpha:0});
			}else{
				TweenMax.to(heatWarning, 0.5, {alpha:1});
			}
			
		}
		
		public function remove():void
		{
			stageS.optimalTemperature.remove(optTemp);
			stageS.gemCollected.remove(collectGem);
			stageS.numberOfBacteria.remove(countBacteria);
			
			optTemp(true);
			
			hideProgress();
			
//			trace("ASDFASDFASDFASDFASDF");
						
			for each (var j:Node2D in _gb.batch[GameScene.GAME_BATCH].children) 
			{
//				trace(j);
				
				if(j is IPooView){
					(j as IPooView).stopSound();
				}
				
				
			}

			
			
			
		}
		
		private function countBacteria(s:String, count:int):void
		{
//			trace("bacteria count = " + count);
			
			_bacteriaCount = count;
			
			if(count <= 0){
				
				endLevel();

			}
			
		}
		
		private function collectGem(p:Point):void
		{
			//trace("collect gem");
			GameState.stageGems++;
			
			_progressPanel.updateProgress();
			
//			_gemCounter.setCount(GameState.stageGems);
			
			//TODO: gem particle/pool, release animation
			
		}
		
		private function endLevel():void
		{
			stageS.finished = true;
			stageS.showHeat = false;
			timesup.dispatch();
		}
		
		private function hideProgress():void
		{
			//TweenMax.to(_progressPanel, 1, {y:screenHeight+300, ease:Quint.easeInOut});
		}
		
		private function frameUpdate(event:Event):void
		{
			if(infoPanel.py == screenHeight/2 + DefaultPanel.PANEL_Y_OFFSET){
				if(!_started){
					_contextView.stage.removeEventListener(Event.ENTER_FRAME, frameUpdate);
					_started = true;
					startUp();
				}
			}
		}
		
		public function showStageFail(type:int):void
		{
			trace("show stage fail!");
//			failPanel.visible = true;
			failPanel.alpha = 1;
			failPanel.show(type);
			TweenMax.to(failPanel, 1.2, {delay:0.1, y:screenHeight/2, ease:Quint.easeInOut});
			
		}
		
		public function showPanel():void
		{
			TweenMax.to(title, 1.3, {y:screenHeight/2 - DefaultPanel.TITLE_Y_OFFSET - DefaultPanel.PANEL_HEIGHT + DefaultPanel.PANEL_Y_OFFSET, ease:Quint.easeInOut});
			TweenMax.to(infoPanel, 1.2, {delay:0.1, py:screenHeight/2 + DefaultPanel.PANEL_Y_OFFSET, ease:Quint.easeInOut});
			
			_contextView.stage.addEventListener(Event.ENTER_FRAME, frameUpdate);
			
		}
		
		public function hidePanel():void
		{
			TweenMax.to(_progressPanel, 1, {delay:0.8, y:screenHeight, ease:Quint.easeInOut});
			
			TweenMax.to(title, 1, {delay:0.1, y:screenHeight + DefaultPanel.TITLE_Y_OFFSET + 50, ease:Quint.easeInOut});
			TweenMax.to(infoPanel, 1.1, {py:screenHeight + infoPanel.panelHeight, ease:Quint.easeInOut});
		}
		
		public function showTitle(tweenTime:Number, delay:Number):void
		{
			TweenMax.to(title, tweenTime, {delay:delay, y:screenHeight/2  - DefaultPanel.TITLE_Y_OFFSET - LevelCompletePanel.PANEL_HEIGHT + LevelCompletePanel.OFFSET, ease:Quint.easeInOut});
			
			TweenMax.to(_progressPanel, 1, {y:screenHeight+_progressPanel.height, ease:Quint.easeInOut});
		}
		
		
		public function resume():void
		{
			//trace("resume level");
			if(_levelTimer.alpha == 0.01){
				TweenMax.to(_levelTimer, 0.5, {alpha:1});
			}
			
			_systemManager.resume();
			_levelTimer.start();
		}
		
		public function pause():void
		{
			_systemManager.pause();
			_levelTimer.stop();
		}
		
		public function pauseTime():void
		{
			_levelTimer.stop();
			TweenMax.to(_levelTimer, 0.7, {delay:0.6, alpha:0});
		}
				
		/**
		 * The Startup Hook
		 *
		 * <p>Override this in your Game</p>
		 */
		protected function startUp():void{
			_levelTimer.time = stageS.timeLimit;
		}
		
		/**
		 * The ShutDown Hook
		 *
		 * <p>Override this in your Game</p>
		 */
		protected function shutDown():void{
			
		}		
		
		/**
		 * Injection Mapping Hook
		 *
		 * <p>Override this in your Game if you want to customise</p>
		 */
		protected function mapInjectors():void{
			
			trace(">>>> Map Injectors");
			injector.mapValue(GameBatches, _gb);
			injector.mapValue(IEntityManager, entityManager);
			injector.mapValue(Scene2D, contextView);
		} 
		
		/**
		 * @inheritDoc
		 */
		public function set contextView(value:Scene2D):void{
			_contextView = value;
			//checkAutoStartup();
		}
		
		/**
		 * @inheritDoc
		 */
		public function get contextView():Scene2D{
			return _contextView;
		}
		
		/**
		 * @inheritDoc
		 */
		final public function dispose():void{
			_entityManager.dispose();
			_entityManager = null;
			_systemManager.dispose();
			_systemManager = null;
			_contextView = null;
			_injector = null;
			shutDown();
		}
		
		/**
		 * The <code>Injector</code> for this <code>IGame</code>
		 */
		protected function get injector():Injector{
			return _injector || (_injector = new Injector());
		}
		
		/**
		 * The <code>IEntityManger</code> for this <code>IGame</code>
		 */
		protected function get entityManager():IEntityManager{
			return _entityManager || (_entityManager = new EntityManager());
		}
		
		/**
		 * The <code>ISystemManager</code> for this <code>IGame</code>
		 */
		protected function get systemManager():IDefaultSystemManager{
			return _systemManager || (_systemManager = new DefaultSystemManager(injector));
		}
		
		//---------------------------------------------------------------------
		// Internal
		//---------------------------------------------------------------------
		
//		/**
//		 * @private
//		 */
//		private function checkAutoStartup():void{
//			if (contextView){
//				mapInjectors();
//				contextView.stage ? startUp() : contextView.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage, false, 0, true);
//			}
//		}
//		
//		/**
//		 * @private
//		 */
//		private function onAddedToStage(event:Event):void{
//			startUp();
//		}
		
	}	
	
}