package com.sajakfarki.biopets.game.base
{
	import com.sajakfarki.biopets.signals.*;
	
	import org.osflash.signals.Signal;
	
	public interface ILevel
	{
	//	function get optimalTemperature(): OptimalTemperature;
		//function get hudPosition() : HUDPosition;
//		function get numberOfBacteria() : NumberOfBacteria;
//		function get reactionProgress() : ReactionProgress;
		
//		function get achievements() : Signal;
		function get timesup() : Signal;
		function get initialized() : LevelInitialized;
		function get stageFinished() : StageFinished;
		function get levelFinished() : LevelFinished;
		function get nextStage(): NextStage;
		function get retry() : Retry;
		function get unlock() : Unlock;
		
		function get levelTime():int;
		function get bacteriaCount():int;
	
		function remove() : void;
		function showPanel() : void;
		function hidePanel() : void;
		function showTitle(tweenTime:Number, delay:Number) : void;
		function showLevelComplete(tweenTime:Number, delay:Number, unlock:Boolean) : void;
		function showStageComplete(tweenTime:Number, delay:Number) : void;
		function showStageFail(type:int) : void;
		
		function resume() : void;
		function pause() : void;
		function pauseTime() : void;
		function finish() : void;
		function disposeLevel() : void;
		
		function addSystems() : void
	}
}