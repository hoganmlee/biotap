package com.sajakfarki.biopets.game.base
{
	import com.sajakfarki.biopets.systems.ProcessManager;
	import com.sajakfarki.biopets.systems.TapSystem;
	import com.tomseysdavies.ember.core.IDisposable;
	import com.tomseysdavies.ember.core.ISystem;
	
	import flash.utils.Dictionary;
	
	import org.swiftsuspenders.Injector;
	
	/**
	 * responsible for registering and unregistering systems
	 * @author Tom Davies
	 */
	public class DefaultSystemManager implements IDefaultSystemManager{
		
		private var _injector:Injector;
		private var _systems:Dictionary;
		
		/**
		 * Creates a new <code>SystemManager</code> object
		 *
		 * @param injector An <code>Injector</code> to use for this game
		 */
		public function DefaultSystemManager(injector:Injector){
			_injector = injector;
			_systems = new Dictionary();
		}
		
		
		public function resume():void
		{
			_systems[ProcessManager].start();
			_systems[TapSystem].start();
		}
		
		public function pause():void
		{
			_systems[ProcessManager].dispose();
			_systems[TapSystem].dispose();
		}
				
		/**
		 * @inheritDoc
		 */
		public function addSystem(System:Class):*{
			if(_systems[System] != null){
				trace("Warning system "+System+" already exists in system manager");
			}
			var system:ISystem = _injector.instantiate(System);
			_systems[System] = system;
			system.onRegister();
			return system;
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeSystem(System:Class):void{
			try{
				ISystem( _systems[System] ).dispose();
				delete _systems[System];
			}catch(e:Error){
				trace("Warning system "+ System +" dosn't exist in system manager");
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function dispose():void{
			for each(var item:IDisposable in _systems) {
				item.dispose();
			}
			_systems = new Dictionary();
		}
		
	}
}