package com.sajakfarki.biopets.components
{
	import com.sajakfarki.biopets.graphics.types.CompoundTypes;
	import com.sajakfarki.biopets.signals.GemCollected;
	import com.sajakfarki.biopets.signals.HUDPosition;
	import com.sajakfarki.biopets.signals.LevelFinished;
	import com.sajakfarki.biopets.signals.LevelInitialized;
	import com.sajakfarki.biopets.signals.NumberOfBacteria;
	import com.sajakfarki.biopets.signals.OptimalTemperature;
	import com.sajakfarki.biopets.signals.ReactionProgress;
	import com.sajakfarki.biopets.signals.StageFinished;
	
	import flash.geom.Vector3D;
	
	import org.osflash.signals.Signal;

	public class StageState
	{
		public var optimalTemperature:OptimalTemperature = new OptimalTemperature();
		public var numberOfBacteria:NumberOfBacteria = new NumberOfBacteria();
//		public var reactionProgress:ReactionProgress = new ReactionProgress();
//		public var hudPosition:HUDPosition = new HUDPosition();
		public var gemCollected:GemCollected = new GemCollected();
		public var stageFinished:StageFinished = new StageFinished();
		public var levelInitialized:LevelInitialized = new LevelInitialized();
				
		/************************************ 
		 * Level vars 
		 ************************************/
		public var timeLimit : int;		//
		public var numBacteria : int;	//
		
		public var init : Boolean = false;				//Stage initialized
		public var paused : Boolean = true;			//Stage paused
		public var finished : Boolean = false;			//Stage finished
		public var showHeat:Boolean = true;		//Show heat controls
				
		public var stageType:String;
		public var tapRadius : Number = 100;
		
		public var width : Number;
		public var height : Number;
		
		/************************************ 
		 * Position vars
		 ************************************/
		public var pooPostion:Array = new Array();
		public var bacteriaPostion:Array = new Array();
		public var badBacteriaPosition:Array = new Array();
		public var stickyPosition:Array = new Array();
		public var killerPosition:Array = new Array();
		
		/************************************ 
		 * Bad Bacteria vars 
		 ************************************/
		public var badBacteriaInfection:Number;				// used by poo system
		
		public var badBacteriaType : String;
		public var badBacteriaRadius : Number = 10;
		public var maxBadBacteriaForce : Number;
		public var maxBadBacteriaSpeed : Number;
		
		/************************************ 
		 * Bacteria vars 
		 ************************************/
		public var bacteriaType : String;
		public var bacteriaRadius : Number = 10;
		public var maxBacteriaForce : Number;
		public var maxBacteriaSpeed : Number;
		
		/************************************ 
		 * Poo vars 
		 ************************************/
		public var compoundType : String = CompoundTypes.POO;
		public var maxPooRadius : Number = 120;
		public var minPooRadius : Number = 119;
		
		public var rateOfRadiusExpand : Number = 0.8;
		public var rateOfRadiusCollapse : Number = 0.8;
		
		public var rateOfPop : Number = 30;
		public var popTime : Number = 30;
		public var popCount : Number = 0;
		
		public var gemsPerCompound : int;
		
		public var rateOfReaction : Number;
		
		public var reactionStartTime : Number = 100;
		
		public var reactionCurrentTime : Number;
		
		public var minBacteria : int = 0;
		public var maxBacteria : int = 15;
		
		/************************************ 
		 * Temperature vars 
		 ************************************/
		public var maxHeatTemp : Number = 100;
		public var minHeatTemp : Number = 0;
		
		public var maxHeatOptimal :Number;
		public var minHeatOptimal :Number;
		
		public var currentHeat:Number;
		
		public var heatStep:Number = 0.2;
		public var coolRate:Number;
		public var heatRate:Number;
				
	}
}