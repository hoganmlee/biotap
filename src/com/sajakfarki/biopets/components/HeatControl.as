package com.sajakfarki.biopets.components
{
	public class HeatControl
	{
		public var isOptimal : Boolean = false;
		
		public var max : Number;
		public var min : Number;
			
		public var optimalMin : Number;
		public var optimalMax :Number;
		
		public var currentHeat : Number;
		
		public var heatStep : Number;
		
		public var coolRate : Number;
		public var heatRate : Number;
		
	}
}