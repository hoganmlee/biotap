package com.sajakfarki.biopets.components
{
	import flash.utils.Dictionary;

	public class Poo
	{
		public var ID : String;
		public var isOptimal : Boolean = false;
		
		public var maxRadius : Number;
		public var minRadius : Number;
		
		public var rateOfRadiusExpand : Number;
		public var rateOfRadiusCollapse : Number;
		
		public var rateOfPop : Number;
		public var popTime : Number;
		public var popCount : Number;
		
		
		public var numberOfGems : int;
		
		
		public var rateOfReaction : Number;
		
		
	//	public var startTime : Number;
	//	public var reactionTime : Number;
		
		
		public var minBacteria : int;
		public var maxBacteria : int;
		
		
		public var bacteriaCollection : Dictionary = new Dictionary();
		public var collectionCount : uint;
		public var badCount : uint;
		
		
	}
}