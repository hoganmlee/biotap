package com.sajakfarki.biopets.components
{
	import flash.geom.Point;

	public class Orbit
	{
		public var velocity : Point = new Point();
		public var angularVelocity : Number = 0;
	}
}