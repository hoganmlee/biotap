package com.sajakfarki.biopets.components
{
	import flash.geom.Point;
	
	public class Position
	{
		public var isDead:Boolean = false;
		public var point : Point = new Point();
		public var rotation : Number = 0;
		public var collisionRadius : Number = 0;
		public var temperature : Number = 0;
		public var numBacteria : int = 0;
		public var minBacteria : int = 0;
		
		public var numberOfGems : int;
		public var startTime : int;
		public var reactionTime : int;
		
	}
}