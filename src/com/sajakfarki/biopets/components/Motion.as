package com.sajakfarki.biopets.components
{
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	public class Motion
	{
		public var normal : Vector3D = new Vector3D();
		public var velocity : Vector3D = new Vector3D();
		public var acceleration : Vector3D = new Vector3D();
		public var steeringForce : Vector3D = new Vector3D();
		
		//TODO: abstract to level logic
		//
		public var maxForce : Number;
		public var maxForceSQ : Number;
		public var maxSpeedSQ : Number;
		public var maxSpeed : Number;
		
		public var wanderTheta : Number = 0.0;
		public var wanderPhi : Number = 0.0;
		
		public var wanderRadius : Number = 1.0;
		public var wanderDistance : Number = 50.0;
		public var wanderStep : Number = 0.2;
		
		
//		boid.maxForce = random(_config.minForce, _config.maxForce);
//		boid.maxSpeed = random(_config.minSpeed, _config.maxSpeed);
//		boid.wanderDistance = random(_config.minWanderDistance, _config.maxWanderDisstance);
//		boid.wanderRadius = random(_config.minWanderRadius, _config.maxWanderRadius);
//		boid.wanderStep = random(_config.minWanderStep, _config.maxWanderStep);
		
		
	}
	
}