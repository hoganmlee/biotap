package com.sajakfarki.biopets.components
{
	import com.sajakfarki.biopets.game.scenes.LevelSelectScene;
	
	public class GameState
	{
		public static const START:String = "start";
		public static const LEVEL_SELECT:String = "select";
		public static const LEVEL_PLAY:String = "play";
		public static const LEVEL_FINISH:String = "level";
		public static const LEVEL_NEXT:String = "next";
		public static const GAME_FINISH:String = "finish";		
		
		
		public static var CURRENT_STATE:String = START;
		public static var CURRENT_LEVEL:String;
		public static var CURRENT_PHASE:int;
		
		
		public static var levelsOpen:int = 1;
		
		
		public static var btuMultiplier : int = 200;
		public static var totalGemsPossible : int = 100;
//		public static var gemsPerCompound : int = 100;
		public static var gemsCollected : int = 100;
		public static var gemsRetry : int = 0;
		
		
		public static var stageGems : int = 0;
		//public static var 
		
		
//		public static var globalBTUs : int = 1458985;
		public static var globalBTUs : int = 0;
		
		
		public static var stage1Collected : Number = 85;
		public static var stage2Collected : Number = 75;
		public static var stage3Collected : Number = 65;
		
		
		
	}
	
}