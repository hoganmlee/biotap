package com.sajakfarki.biopets.components
{
	public class Cards
	{
		[Embed(source = "../media/achievements/manuelle_front.png")]
		public static const CARD1:Class;
		
		[Embed(source = "../media/achievements/hyde_front.png")]
		public static const CARD2:Class;
		
		[Embed(source = "../media/achievements/fergus_front.png")]
		public static const CARD3:Class;
		
		[Embed(source = "../media/achievements/aceto_front.png")]
		public static const CARD4:Class;
		
		[Embed(source = "../media/achievements/methilda_front.png")]
		public static const CARD5:Class;
		
		[Embed(source = "../sounds/HeatItUp.mp3")]
		public static const HEATMP3:Class;
		
		[Embed(source = "../sounds/Torch.mp3")]
		public static const TORCHMP3:Class;
		
		[Embed(source = "../sounds/scratch_01.mp3")]
		public static const SCRATCH1MP3:Class;
		[Embed(source = "../sounds/scratch_02.mp3")]
		public static const SCRATCH2MP3:Class;
		[Embed(source = "../sounds/scratch_03.mp3")]
		public static const SCRATCH3MP3:Class;
		[Embed(source = "../sounds/scratch_04.mp3")]
		public static const SCRATCH4MP3:Class;
		[Embed(source = "../sounds/scratch_05.mp3")]
		public static const SCRATCH5MP3:Class;
		
		public static var ISHEAT:Boolean = false;
		public static var ISSCRATCH:Boolean = false;
		
	}
}