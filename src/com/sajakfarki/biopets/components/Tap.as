package com.sajakfarki.biopets.components
{
	import flash.geom.Point;

	public class Tap
	{
		public static const MAX_TAPS:int = 12;
		
		//public var position : Point = new Point();
		//public var collisionRadius : Number = 0;
		
		public var collisionSize: Number;
						
		//public var timeSinceLastTap : Number = 0;
		//public var minimumTapInterval : Number = 0;
		public var tapLifetime : Number;
		public var tapLifeRemaining : Number;
		
	}
}