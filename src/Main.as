package
{
	import com.greensock.TweenMax;
	import com.sajakfarki.biopets.components.GameState;
	import com.sajakfarki.biopets.game.scenes.LevelSelectScene;
	
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.SharedObject;
	import flash.utils.setInterval;
	
	
//	import flashx.textLayout.elements.BreakElement;
	
	
	[SWF(backgroundColor="#000")]
	public class Main extends Sprite
	{
		
		private static const DEBUG : Boolean = false;
//		private static const DEBUG : Boolean = true;

//		private static const WEB : Boolean = false;
		private static const WEB : Boolean = true;  
		
				
		/** Music */
		[Embed(source = "../sounds/StartScreen.mp3")]
		private var _startScreenMP3:Class;
		private var _startScreenMusic:Sound = new _startScreenMP3() as Sound;
		
		private var _channel:SoundChannel;
		
		private var _introVideo:String = "Intro.mp4";
		private var _level1Intro:String = "Level1.mp4";
		private var _level1End:String = "Level1_Exit.mp4";
		private var _level2Intro:String = "Level2.mp4";
		private var _level2End:String = "Level2_Exit.mp4";
		private var _level3Intro:String = "Level3.mp4";
		private var _level3End:String = "Level3_Exit.mp4";
		private var _level4Intro:String = "Level4.mp4";
		private var _level4End:String = "Level4_Exit.mp4";
		private var _level5Intro:String = "Level5.mp4";
		private var _level5End:String = "Level5_Exit.mp4";
		private var _gameEnd:String = "End.mp4";
		
		//Video paths
		private var _introVideoPath:String; 
		private var _level1IntroVideo:String;
		private var _level1EndVideo:String;
		private var _level2IntroVideo:String;
		private var _level2EndVideo:String;
		private var _level3IntroVideo:String;
		private var _level3EndVideo:String;
		private var _level4IntroVideo:String;
		private var _level4EndVideo:String;
		private var _level5IntroVideo:String;
		private var _level5EndVideo:String;
		private var _gameEndVideo:String;
		
		//STAGE VIDEO
		private var _videoWidth:Number; 
		private var _videoHeight:Number; 
		private var _sv:StageVideo;
		private var _obj:Object; 
		private var _ns:NetStream; 
		private var _nc:NetConnection;
		
		//Game class
		private var _gp:GamePlay;
		
		
		//stagevideo available
		private var _available:Boolean;
		
		
		public function Main()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			if(!WEB){
			
				// Listen for exiting event.
//				NativeApplication.nativeApplication.addEventListener(Event.EXITING, onExit);
				
			}
			
			if(!DEBUG){
				
				//load  Shared Object
				load();
				
			}
			
		}
		
		private function save():void
		{
			// Get the shared object.
			var so:SharedObject = SharedObject.getLocal("bioTap");
			
			// Update the age variable.
			so.data['btus'] = GameState.globalBTUs;
			
			// And flush our changes.
			so.flush();
			
			// Also, indicate the value for debugging.
			trace("Saved btus " + so.data['age']);
		}
		
		private function load():void
		{
			// Get the shared object.
			var so:SharedObject = SharedObject.getLocal("bioTap");
			
			// And indicate the value for debugging.
			trace("Loaded btus " + so.data['btus']);
			
			GameState.globalBTUs = so.data['btus'];
			
		}
	
		private function onExit(e:Event):void
		{
			trace("Save here.");
			save();
		}
		
		private function addedToStage(event:Event):void
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			//GAME PLAY CLASS			
			_gp = new GamePlay();
			_gp.addEventListener(Event.INIT, contextLoaded);
			
			addChild(_gp);
			
			stage.addEventListener(StageVideoAvailabilityEvent.STAGE_VIDEO_AVAILABILITY, stageVideoState);

		}
		
		/********************************************************
		 * 
		 * 	Stage Video 
		 * 
		 ********************************************************/
		private function stageVideoState(event:StageVideoAvailabilityEvent=null):void
		{
			
			_available = event.availability == StageVideoAvailability.AVAILABLE ;
			
			trace("stage video available = " + _available);
			
			if(!WEB){
				
//				_introVideoPath = new File(new File(File.applicationDirectory.resolvePath(_introVideo).url).nativePath).url;
//				_level1IntroVideo = new File(new File(File.applicationDirectory.resolvePath(_level1Intro).url).nativePath).url;
//				_level1EndVideo = new File(new File(File.applicationDirectory.resolvePath(_level1End).url).nativePath).url;
//				_level2IntroVideo = new File(new File(File.applicationDirectory.resolvePath(_level2Intro).url).nativePath).url;
//				_level2EndVideo = new File(new File(File.applicationDirectory.resolvePath(_level2End).url).nativePath).url;
//				_level3IntroVideo = new File(new File(File.applicationDirectory.resolvePath(_level3Intro).url).nativePath).url;
//				_level3EndVideo = new File(new File(File.applicationDirectory.resolvePath(_level3End).url).nativePath).url;
//				_level4IntroVideo = new File(new File(File.applicationDirectory.resolvePath(_level4Intro).url).nativePath).url;
//				_level4EndVideo = new File(new File(File.applicationDirectory.resolvePath(_level4End).url).nativePath).url;
//				_level5IntroVideo = new File(new File(File.applicationDirectory.resolvePath(_level5Intro).url).nativePath).url;
//				_level5EndVideo = new File(new File(File.applicationDirectory.resolvePath(_level5End).url).nativePath).url;
//				_gameEndVideo =  new File(new File(File.applicationDirectory.resolvePath(_gameEnd).url).nativePath).url;
				
			}else{
				
				_introVideoPath = _introVideo;
				_level1IntroVideo = _level1Intro;
				_level1EndVideo = _level1End;
				_level2IntroVideo = _level2Intro;
				_level2EndVideo = _level2End;
				_level3IntroVideo = _level3Intro;
				_level3EndVideo = _level3End;
				_level4IntroVideo = _level4Intro;
				_level4EndVideo = _level4End;
				_level5IntroVideo = _level5Intro;
				_level5EndVideo = _level5End;
				_gameEndVideo = _gameEnd;
			
			}
			
			_videoWidth = 1024; 
			_videoHeight = 768;
			
		} 
		
		//required metadata for stagevideo, even if not used		
		private function MetaData(info:Object):void{ } 
		
		//get video status 
		private function videoStatus(e:NetStatusEvent):void{ 
			
			switch(e.info.code){
				case "NetStream.Play.StreamNotFound": 
					//do something 
					break; 
				case "NetStream.Play.Start": 
					//do something 
					break 
				case "NetStream.Play.Stop": 
					stopVideo(); 
					break; 
				case "NetStream.Buffer.Empty": 
					//do something 
					break; 
				case "NetStream.Buffer.Full": 
					//do something 
					break; 
				case "NetStream.Buffer.Flush": 
					//do something 
					break; 
			} 
		} 
		
		//stop and clear the video 
		//public, can be called externally 
		public function stopVideo(e:MouseEvent=null):void{ 
			
			stage.removeEventListener(MouseEvent.CLICK, stopVideo);
			
			if(GameState.CURRENT_STATE != GameState.GAME_FINISH){			
				
				_gp.addEventListener(Event.INIT, contextLoaded);
				_gp.show();
				
			}else{
				
				trace("finish game!");
				playGameFinishedVideo();
				
			}
			
		}
		
		/********************************************************
		 * 
		 * 	Game State Handler 
		 * 
		 ********************************************************/
		protected function contextLoaded(event:Event):void
		{
			if(_sv){
				_ns.close();
				_ns = null;
				_nc.close();
				_nc = null;
				_sv.attachNetStream(null);
				_sv = null;
			}
			
			_gp.removeEventListener(Event.INIT, contextLoaded);
			_gp.saveGame.add(save);
			
			
			//CURRENT_STATE
			switch(GameState.CURRENT_STATE){
				
				case GameState.START:
					
					_channel = _startScreenMusic.play(0,99);
					
					_gp.startScene();
					_gp.startDone.add(loadIntroVideo);
					
					break;
				
				case GameState.LEVEL_SELECT:
					
					_gp.showSelectScene();
					_gp.levelSelected.add(playLevelVideo);
					
					break;
				
				case GameState.LEVEL_NEXT:
					
					playLevelVideo();
					
					break;
				
				case GameState.LEVEL_PLAY:
										
					_gp.startLevel();
					_gp.levelFinished.add(playLevelFinishedVideo);
					_gp.gameFinished.add(playGameFinishedVideo);
					
					break;
				
			}

		}
		
		/**********************************************************************
		 * 
		 * 	Clean Up
		 * 
		 **********************************************************************/
		private function cleanGame():void
		{
			if(_channel){
				_channel.stop();
				_channel = null;
			}
			
			//disable GamePlay
			//trace("_gp.hide()");
			_gp.hide();
			
			if(DEBUG){
				
				stopVideo();
				
			}else{
				
				//clean net connection
				_nc = new NetConnection(); 
				_nc.connect(null); 
				
				//clean net stream
				_ns =  new NetStream(_nc); 
				_obj = new Object();
				_obj.onMetaData = MetaData; 
				
				_ns.client = _obj; 
				_ns.bufferTime = 2;
				
				_ns.addEventListener(NetStatusEvent.NET_STATUS, videoStatus); 
				
				_sv = stage.stageVideos[0];
								
				trace("stageVideo =  " + _sv);
				
				_sv.viewPort = new Rectangle(0, 0, _videoWidth , _videoHeight ); 
				_sv.attachNetStream(_ns);
				
				stage.addEventListener(MouseEvent.CLICK, stopVideo);
				
			}
						
		}
		
		/**********************************************************************
		 * 
		 * 	Play Level Intro Video
		 * 
		 **********************************************************************/
		private function playLevelVideo():void
		{
			GameState.CURRENT_STATE = GameState.LEVEL_PLAY;
			
			cleanGame();
						
			if(!DEBUG){
				
				switch(GameState.CURRENT_LEVEL){
					
					case LevelSelectScene.LEVEL_ONE:
						
						_ns.play( _level1IntroVideo );
						break;
					
					case LevelSelectScene.LEVEL_TWO:
						
						_ns.play( _level2IntroVideo );
						break;
					
					case LevelSelectScene.LEVEL_THREE:
						
						_ns.play( _level3IntroVideo );
						break;
					
					case LevelSelectScene.LEVEL_FOUR:
						
						_ns.play( _level4IntroVideo );
						break;
					
					case LevelSelectScene.LEVEL_FIVE:
						
						_ns.play( _level5IntroVideo );
						break;
					
				}
			}

		}
		
		/**********************************************************************
		 * 
		 * 	Play Level Finished Video
		 * 
		 **********************************************************************/
		private function playLevelFinishedVideo(b:Boolean):void
		{
			
			//save();
			
			if(GameState.CURRENT_STATE == GameState.LEVEL_PLAY){ 
				GameState.CURRENT_STATE = GameState.LEVEL_SELECT;
			}
			
			cleanGame();
			
			if(b){
			
				if(GameState.CURRENT_LEVEL == LevelSelectScene.LEVEL_FIVE){
					GameState.CURRENT_STATE = GameState.GAME_FINISH;
				}
				
				if(!DEBUG){
					
					switch(GameState.CURRENT_LEVEL){
						
						case LevelSelectScene.LEVEL_ONE:
							
							_ns.play( _level1EndVideo );
							
							break;
						
						case LevelSelectScene.LEVEL_TWO:
							
							_ns.play( _level2EndVideo );
							break;
						
						case LevelSelectScene.LEVEL_THREE:
							
							_ns.play( _level3EndVideo );
							break;
						
						case LevelSelectScene.LEVEL_FOUR:
							
							_ns.play( _level4EndVideo );
							break;
						
						case LevelSelectScene.LEVEL_FIVE:
							
							_ns.play( _level5EndVideo );
							
							break;
						
					}
					
				}
				
				//increment to next level
				if(GameState.CURRENT_STATE == GameState.LEVEL_NEXT){
					
					switch(GameState.CURRENT_LEVEL){
						
						case LevelSelectScene.LEVEL_ONE:
							GameState.CURRENT_LEVEL = LevelSelectScene.LEVEL_TWO;
							break;
						case LevelSelectScene.LEVEL_TWO:
							GameState.CURRENT_LEVEL = LevelSelectScene.LEVEL_THREE;
							break;
						case LevelSelectScene.LEVEL_THREE:
							GameState.CURRENT_LEVEL = LevelSelectScene.LEVEL_FOUR;
							break;
						case LevelSelectScene.LEVEL_FOUR:
							GameState.CURRENT_LEVEL = LevelSelectScene.LEVEL_FIVE;
							break;
						
					}
						
				}
				
			}else{
				
				stopVideo();
				
			}
			
		}
		
		/**********************************************************************
		 * 
		 *		Finished Game Video
		 * 
		 **********************************************************************/
		private function playGameFinishedVideo():void
		{
			GameState.CURRENT_STATE = GameState.LEVEL_SELECT;
			
			cleanGame();
			
			if(!DEBUG){
				_ns.play( _gameEndVideo );
			}
			
		}		
		
		/**********************************************************************
		 * 
		 *		Intro Video
		 * 
		 **********************************************************************/
		private function loadIntroVideo():void
		{
			GameState.CURRENT_STATE = GameState.LEVEL_SELECT;
			
			cleanGame();
			
			if(!DEBUG){
				_ns.play( _introVideoPath );
			}
			
		}		
		
	}
}