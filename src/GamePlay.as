package
{
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import com.sajakfarki.biopets.components.*;
	import com.sajakfarki.biopets.game.*;
	import com.sajakfarki.biopets.game.base.*;
	import com.sajakfarki.biopets.game.levels.*;
	import com.sajakfarki.biopets.game.scenes.*;
	import com.sajakfarki.biopets.systems.*;
	import com.sajakfarki.biopets.utils.*;
	
	import de.nulldesign.nd2d.display.World2D;
	import de.nulldesign.nd2d.materials.BlendModePresets;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.events.StageVideoAvailabilityEvent;
	import flash.events.TouchEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.media.StageVideo;
	import flash.media.StageVideoAvailability;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.Dictionary;
	
	import net.hires.debug.Stats;
	
	import org.osflash.signals.Signal;
	
	public class GamePlay extends HoganWorld
	{

		private static const WIDTH:Number = 1024;
		private static const HEIGHT:Number = 768;
//		private static const WIDTH:Number = 960;
//		private static const HEIGHT:Number = 600;
		
		public static const LEVEL_1_PASS:Number = 100000;
		public static const LEVEL_2_PASS:Number = 200000;
		public static const LEVEL_3_PASS:Number = 500000;
		public static const LEVEL_4_PASS:Number = 1000000;
		public static const LEVEL_5_PASS:Number = 1500000;
		
		//DEBUG
//		public static var stats:Stats = new Stats();
				
		public var startDone:Signal = new Signal();
		public var levelSelected:Signal = new Signal();
		public var levelFinished:Signal = new Signal(Boolean);
		public var gameFinished:Signal = new Signal();
		public var saveGame:Signal = new Signal();
		
		
		private var _success:Boolean;
		
		
		//Main Scene
		private var _startScene : StartScene;
		private var _levelSelect : LevelSelectScene;
		private var _gameScene : GameScene;
		private var _achieveScene : AchieveScene;
		
		//Level Interface 
		private var _level : ILevel;
		
		/** Music */
		[Embed(source = "../sounds/bg.mp3")]
		private var _bgMP3:Class;
		private var _bgMusic:Sound = new _bgMP3() as Sound;
		
		/** Music */
		[Embed(source = "../sounds/InfoMusic.mp3")]
		private var _infoMP3:Class;
		private var _infoMusic:Sound = new _infoMP3() as Sound;
		
		/** Music */
		[Embed(source = "../sounds/LevelComplete.mp3")]
		private var _completeMP3:Class;
		private var _completeMusic:Sound = new _completeMP3() as Sound;
		
		/** Music */
		[Embed(source = "../sounds/LevelFail.mp3")]
		private var _failMP3:Class;
		private var _failMusic:Sound = new _failMP3() as Sound;
		
		/** Music */
		[Embed(source = "../sounds/MapSelect.mp3")]
		private var _levelSelectMP3:Class;
		private var _levelSelectMusic:Sound = new _levelSelectMP3() as Sound;
		
		private var _channel:SoundChannel;
		
		private var _unlock:Boolean;
		
		
		public function GamePlay()
		{
			// touch
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			enableErrorChecking = false;
			
			antialiasing = 1;
			
			super(Context3DRenderMode.AUTO, 60);
			
		}
	
		/************************************************
		 * 
		 *		Override Added To Stage , Main Loop
		 * 
		 ************************************************/
		
		override protected function mainLoop(e:Event):void
		{
			super.mainLoop(e);
//			stats.update(statsObject.totalDrawCalls, statsObject.totalTris);	
		}
		
		override protected function context3DCreated(e:Event):void
		{
			super.context3DCreated(e);
			
			if (context3D)
			{
//				stats.driverInfo = context3D.driverInfo;
			}
			
		}
		
		override protected function addedToStage(event:Event):void
		{
			super.addedToStage(event);
			
//			stats.y = HEIGHT - 100;
//			addChild(stats);
		}
		
		/************************************************
		 * 
		 *		Start Scene
		 * 
		 ************************************************/
		public function startScene():void
		{
			_startScene = new StartScene(WIDTH, HEIGHT);
			
			setActiveScene(_startScene);
			
			start();
			
			TweenMax.delayedCall(0.5, getGameState);
			
		}
		
		private function getGameState():void
		{
			//TODO: get game state, saved game domain
			
			_startScene.done.add(startScreenDone);
			_startScene.show();
			
		}
		
		private function startScreenDone():void
		{
			trace(">>>>>> startScreenDone");
			_startScene.done.removeAll();
			
			disposeGame();
			
			startDone.dispatch();
		}
				
		/************************************************
		 * 
		 *		Show Level Select Screen
		 * 
		 ************************************************/	
		public function showSelectScene():void
		{	
			if(_channel){
				_channel.stop();
			}
				
			_channel = _levelSelectMusic.play(0,99);
			
			if(GameState.globalBTUs < LEVEL_1_PASS){
				
				GameState.levelsOpen = 1;
				
			}else if(GameState.globalBTUs < LEVEL_2_PASS){
				
				GameState.levelsOpen = 2;
				
			}else if(GameState.globalBTUs < LEVEL_3_PASS){
				
				GameState.levelsOpen = 3;
				
			}else if(GameState.globalBTUs < LEVEL_4_PASS){
				
				GameState.levelsOpen = 4;
				
			}else{
				
				GameState.levelsOpen = 5;
			}
			
			//Level Select Scene
			_levelSelect = new LevelSelectScene(WIDTH, HEIGHT);
			_levelSelect.levelSelected.add(selectLevel);
			_levelSelect.doneHide.add(showLevelVideo);
			setActiveScene(_levelSelect);
			
			start();
			
			TweenMax.delayedCall(1, _levelSelect.show);
						
		}
				
		private function selectLevel(s:String):void
		{
			GameState.CURRENT_LEVEL = s;
		}
		
		private function showLevelVideo():void
		{
			_levelSelect.doneHide.removeAll();
			
			
			
			if(GameState.CURRENT_LEVEL == LevelSelectScene.ACHIEVEMENTS){
				
				disposeGame(false);
				
				showAchievements();
				
			}else{
				
				disposeGame();
								
				levelSelected.dispatch();
				
			}
		}
		
		/**********************************************************
		 * 
		 * 	Show Achievements
		 * 
		 **********************************************************/
		public function showAchievements(level:int = 0, reveal:Boolean = false):void
		{	
			//Level Select Scene
			_achieveScene = new AchieveScene(WIDTH, HEIGHT);
			_achieveScene.doneHide.add(showSelectScene);
			setActiveScene(_achieveScene);
			
			start();
			
			TweenMax.delayedCall(1, _achieveScene.show, [level, reveal]);
			
		}
		
		/**********************************************************
		 * 
		 * 	Start Level
		 * 
		 **********************************************************/
		public function startLevel():void
		{	
			//set level
			GameState.stage1Collected = 0.1;
			GameState.stage2Collected = 0.1;
			GameState.stage3Collected = 0.1;
						
			GameState.CURRENT_PHASE = 0;
						
			switch(GameState.CURRENT_LEVEL){
				
				case LevelSelectScene.LEVEL_ONE:
				
//					GameState.totalGemsPossible = 7 * Math.pow(3, 4);
					GameState.gemsCollected= GameState.totalGemsPossible = 100 * 1;
					GameState.btuMultiplier = 1000;
										
//					trace("GameState.totalGemsPossible = " + GameState.totalGemsPossible);
						
					break;
				
				case LevelSelectScene.LEVEL_TWO:
					 
					GameState.gemsCollected = GameState.totalGemsPossible = 100*2;
//					GameState.totalGemsPossible = 7 * Math.pow(3, 4);
					GameState.btuMultiplier = 1000;
					
					break;
				
				case LevelSelectScene.LEVEL_THREE:
					
					GameState.gemsCollected = GameState.totalGemsPossible = 100*2;
					//GameState.totalGemsPossible = 7 * Math.pow(3, 4);
					GameState.btuMultiplier = 1000;
					break;
				
				case LevelSelectScene.LEVEL_FOUR:
					
					GameState.gemsCollected = GameState.totalGemsPossible = 100*3;
					//GameState.totalGemsPossible = 7 * Math.pow(3, 4);
					GameState.btuMultiplier = 1000;
					break;
				
				case LevelSelectScene.LEVEL_FIVE:

					GameState.gemsCollected = GameState.totalGemsPossible = 100*4;
					//GameState.totalGemsPossible = 7 * Math.pow(3, 4);
					GameState.btuMultiplier = 1000;
					break;
				
				
			}
			
			loadStage();
			
		}
		
		private function quitGame():void
		{
			_level.pause();
			_level.remove();
			
			_gameScene.hide();
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(quitGameDone);
		}
		
		private function quitGameDone():void
		{
			trace(">>>>>>> quitGameDone");
			_gameScene.doneHide.removeAll();
			
			disposeGame();
			
			TweenMax.delayedCall(1, showSelectScene);
		}
		
		private function retryStage():void
		{
			GameState.gemsCollected = GameState.gemsRetry;
			
			_level.pause();
			_level.remove();
			
			_gameScene.hide();
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(retryDone);
			
		}
		
		private function retryDone():void
		{
			trace(">>>>>>> quitGameDone");
			
			//_gameScene.killTweens();
			disposeGame();
			
			TweenMax.delayedCall(1 , loadStage);
		}
		
		/**********************************************************
		 * 
		 *		Load Stage
		 * 
		 **********************************************************/
		private function loadStage():void
		{
			_gameScene = new GameScene(WIDTH, HEIGHT);
			_gameScene.paused.add(pauseGame);
			_gameScene.help.add(helpGame);
			_gameScene.start.add(startGame);
			_gameScene.retry.add(retryStage);
			_gameScene.quit.add(quitGame);
			setActiveScene(_gameScene);
			
			
			trace(">>>>> Load Stage");
			
			
			/** Reset gems */
			GameState.stageGems = 0;
			GameState.gemsRetry = GameState.gemsCollected;
			
			var stage:Class;									//Level_*_*
			
			switch(GameState.CURRENT_LEVEL){
				
				case LevelSelectScene.LEVEL_ONE:	
					
					switch(GameState.CURRENT_PHASE){
						case 0:
							stage = Level_1_1;
							break;
						case 1:
							stage = Level_1_2;
							break;
						case 2:
							stage = Level_1_3;
							break;
						case 3:
							stage = Level_1_4;
							break;
					}
					break;
				
				case LevelSelectScene.LEVEL_TWO:
					
					switch(GameState.CURRENT_PHASE){
						case 0:
							stage = Level_2_1;
							break;
						case 1:
							stage = Level_2_2;
							break;
						case 2:
							stage = Level_2_3;
							break;
						case 3:
							stage = Level_2_4;
							break;
					}
					break;
				
				case LevelSelectScene.LEVEL_THREE:
					
					switch(GameState.CURRENT_PHASE){
						case 0:
							stage = Level_3_1;
							break;
						case 1:
							stage = Level_3_2;
							break;
						case 2:
							stage = Level_3_3;
							break;
						case 3:
							stage = Level_3_4;
							break;
					}
					break;
				
				case LevelSelectScene.LEVEL_FOUR:
					
					switch(GameState.CURRENT_PHASE){
						case 0:
							stage = Level_4_1;
							break;
						case 1:
							stage = Level_4_2;
							break;
						case 2:
							stage = Level_4_3;
							break;
						case 3:
							stage = Level_4_4;
							break;
					}
					break;
				
				case LevelSelectScene.LEVEL_FIVE:
					
					switch(GameState.CURRENT_PHASE){
						
						case 0:
							stage = Level_5_1;
							break;
						case 1:
							stage = Level_5_2;
							break;
						case 2:
							stage = Level_5_3;
							break;
						case 3:
							stage = Level_5_4;
							break;
					}
					
					break;
				
			}
			
			start();
			
			
			/**  Set the Stage */
			_level = new stage(_gameScene, _gameScene.gameBatches, WIDTH, HEIGHT, GameState.gemsCollected);

			/**  Add Listeners */
			_level.initialized.add(showLevel);
			_level.stageFinished.add(finishedStage);
			_level.timesup.add(endLevel);
			
		}
		
		private function helpGame():void
		{
			_level.hidePanel();
		}
		
		private function showLevel():void
		{
			_level.initialized.remove(showLevel);
			
			_channel.stop();
			_channel = _infoMusic.play(0,99);
			
			//_level done initializing
			TweenMax.delayedCall(0.5, _gameScene.show);
		}
		
		private function endLevel():void
		{			
			
			if(_level.bacteriaCount <= 0){
				
				finishedStage(false, 1);
				
			}else{
				
				if(GameState.stageGems > GameState.totalGemsPossible/2){
					
					finishedStage(true);
					
				}else{
					
					finishedStage(false, 2);
					
				}
				
			}
			
			
		}
		
		private function startGame():void
		{
			//Start Game Button pressed
			_channel.stop();
			_channel = _bgMusic.play(0,99);
			
			
			//_level.resume();
			TweenMax.delayedCall(0.2, resumeLevel);
		}
		
		/************************************************
		 * 
		 *	  Game Pause
		 * 
		 ************************************************/
		private function pauseGame(value:Boolean):void
		{
			
			if(value){
				
				pauseLevel();
								
			}else{
				
				resumeLevel();
				
			}
		
		}
		
		/**********************************************************
		 * 
		 * 	Pause/Resume Level
		 * 
		 **********************************************************/
		private function resumeLevel():void
		{
			TweenMax.killDelayedCallsTo(_level.resume);
			
			_level.hidePanel();
			TweenMax.delayedCall(1.5, _level.resume);
		}
		
		private function pauseLevel():void
		{
			TweenMax.killDelayedCallsTo(_level.resume);
			_level.showPanel();
			_level.pause();
			
		}
		
		/**********************************************************
		 * 
		 * 	Level / Stage Finished
		 * 
		 **********************************************************/
		private function finishedStage(success:Boolean, type:int = -1):void
		{
			_success = success;
			
			_level.pauseTime();				
			_level.remove();					//tween out progress bar
			_gameScene.hidePause();
			
			/**  Remove Listeners */
			_level.stageFinished.remove(finishedStage);
			_level.initialized.remove(showLevel);
			_level.timesup.remove(endLevel);
			
			_unlock= false;
			
			/** Advance Stage Position if Success */
			if(success){
				
				_channel.stop();
				_channel = _completeMusic.play();
				
				//GameState.CURRENT_PHASE++;
				//GameState.CURRENT_PHASE++;
				GameState.gemsRetry = GameState.gemsCollected;
				GameState.gemsCollected = GameState.stageGems;
				
				if(GameState.CURRENT_PHASE == 3){
					
					trace("Level Complete!");
					
					_level.levelFinished.add(finish);
					_level.retry.add(retryLevel);
					_level.unlock.add(unlockAchieve);   //NOW NEXT LEVEL BUTTON
					
					var delay:Number = 2;
					var passBTU:Number;
					
					switch(GameState.CURRENT_LEVEL){
						
						case LevelSelectScene.LEVEL_ONE:	
							passBTU = GamePlay.LEVEL_1_PASS
							break;
						
						case LevelSelectScene.LEVEL_TWO:
							passBTU = GamePlay.LEVEL_2_PASS
							break;
						
						case LevelSelectScene.LEVEL_THREE:
							passBTU = GamePlay.LEVEL_3_PASS
							break;
						
						case LevelSelectScene.LEVEL_FOUR:
							passBTU = GamePlay.LEVEL_4_PASS
							break;
						
						case LevelSelectScene.LEVEL_FIVE:
							passBTU = GamePlay.LEVEL_5_PASS
							break;
						
					}
					
					//CHECK UNLOCK > GLOBAL BTUS
					if(GameState.globalBTUs < passBTU){
						
						if(GameState.globalBTUs + GameState.btuMultiplier * GameState.gemsCollected >= passBTU){
							
							_unlock = true;
							delay += 8;
							
							_level.showLevelComplete(1.2, delay, _unlock);
							_level.showTitle(1.1, delay + 0.1);
						
						}else{
							
							_unlock = false;
							_level.showLevelComplete(1.2, delay, _unlock);
							_level.showTitle(1.1, delay + 0.1);
							
						}
						
					}
					
					//IF THEY ALREADY PASSED THE LEVEL
					if(GameState.globalBTUs > passBTU){
						
						_unlock = false;
						_level.showLevelComplete(1.2, delay, _unlock);
						_level.showTitle(1.1, delay + 0.1);
						
					}
					
					GameState.globalBTUs += GameState.btuMultiplier * GameState.gemsCollected;
					
					saveGame.dispatch();
										
				}else{
										
					switch(GameState.CURRENT_PHASE){
						
						case 0:
							GameState.stage1Collected = GameState.gemsCollected;
							break;
						case 1:
							GameState.stage2Collected = GameState.gemsCollected;
							break;
						case 2:
							GameState.stage3Collected = GameState.gemsCollected;
							break;
					}
					
					
					/** Show Stage Complete */
					_level.nextStage.removeAll();
					_level.nextStage.add(nextStage);
					_level.showStageComplete(0.7, 1);
					
					//Auto complete stage
					TweenMax.delayedCall(3, nextStage);
					
				}
				
			}else{
				
				_channel.stop();
				_channel = _failMusic.play();
				
				_level.levelFinished.add(finish);
				_level.retry.add(retryStage);
				_level.showStageFail(type);
				
			}
						
		}
		
		private function retryLevel():void
		{
			_level.pause();
			
			_gameScene.hide();
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(retryLevelDone);
		}
		
		private function retryLevelDone():void
		{
			disposeGame();
			TweenMax.delayedCall(1, startLevel);
		}
		
		/**********************************************************
		 * 
		 * 	Unlocked Achievement
		 * 
		 **********************************************************/
		private function unlockAchieve():void
		{
//			GameState.globalBTUs += GameState.btuMultiplier * GameState.gemsCollected;
			
			_level.pause();
			
			_gameScene.hide();
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(unlockDone);
			
		}
		
		private function unlockDone():void
		{
			trace(">>>>>>> unlock Done");
			
			//_gameScene.killTweens();
			disposeGame();
			
			TweenMax.delayedCall(1 , showAchieve);
			
		}
		
		private function showAchieve():void
		{
			
			if(_channel){
				_channel.stop();
			}
			
			//_channel = _levelSelectMusic.play(0,99);
			
			GameState.CURRENT_STATE = GameState.LEVEL_NEXT;
			//GameState.CURRENT_LEVEL = LevelSelectScene.LEVEL_FIVE;
			TweenMax.delayedCall(1 , levelFinished.dispatch, [_unlock]);
			
			
		}
		
		/**********************************************************
		 * 
		 * 	Stage Complete
		 * 
		 **********************************************************/
		private function nextStage():void
		{
			TweenMax.killDelayedCallsTo(nextStage);
			
			trace("Next Stage - >");
			_level.pause();
			
			_gameScene.hide();
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(nextStageDone);
		}
		
		private function nextStageDone():void
		{
			trace(">>>>>>> nextStageDone");
			
			_gameScene.doneHide.removeAll();
			//sleep();
			GameState.CURRENT_PHASE++;
			
			//_gameScene.killTweens();
			disposeGame();
			
			TweenMax.delayedCall(1 , loadStage);
		}
		
		/**********************************************************
		 * 
		 * 	Level Complete
		 * 
		 **********************************************************/
		private function finish():void
		{
			
//			if(_success){
////				GameState.globalBTUs += GameState.btuMultiplier * GameState.gemsCollected;
//			}
			
			_level.pause();
			
			_gameScene.doneHide.removeAll();
			_gameScene.doneHide.add(finishDone);
			
			_gameScene.hide();
			
		}
		
		private function finishDone():void
		{
			trace(">>>>>>> finishDone");
			
			_gameScene.doneHide.removeAll();
			
			disposeGame();

			TweenMax.delayedCall(1 , levelFinished.dispatch, [_unlock]);
			
		}
		
		/**********************************************************
		 * 
		 *    Clean up _gameScene / _level , keep the contextView persistant  
		 * 
		 **********************************************************/
		private function disposeGame(stopSound:Boolean = true):void
		{
			
			if(stopSound){
				if(_channel){
					
					_channel.stop();
	//				_channel = null;
				}
			}
			
			if(_level){
				_level.disposeLevel();
				_level = null;
			}
			
			if(_gameScene){
				_gameScene.dispose();
				_gameScene = null;
			}
			
		}
		
	}
}
